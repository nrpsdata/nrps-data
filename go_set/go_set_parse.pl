#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

## usage:
##	perl go_set_parse.pl goset.tsv

my %ans = ();
while(<>){
	chomp;
	unless($_ =~ m/^#/){
		my ($acc, $num, $spec) = split(/\t/, $_);
		$ans{$acc}{$num} = $spec;
	}
}
unless(-e 'goset.faa'){
	foreach my $a (keys %ans){
		system("esearch -db protein -query \"$a\" |efetch -format fasta|grep . > tmp");
		chomp(my $seqs = `grep '>' tmp|wc -l`);
		die "Issue with $a:\t$seqs sequence(s) found.\n" unless($seqs == 1);
		system("perl -pi -e \'s\/>\.+\/>$a\/\' tmp");
		system("cat tmp >> goset.faa");
	}
}
system("rm tmp") if(-e 'tmp');
unless(-e 'goset.nrpsA.bp'){
	## Get A_Domains
	my $a_query = 'nnvaivceneqltyhelnvkanqlarifiekgigkdtlvgimmeksidlfigilavlkaggayvpidieypkeriqyilddsqarmlltqkhlvhlihniqfngqveifeedtikiregtnlhvpskstdlayviytsgttgnpkgtmlehkgisnlkvffenslnvtekdrigqfasisfdasvwemfmalltgaslyiilkdtindfvkfeqyinqkeitvitlpptyvvhldperilsiqtlitagsatspslvnkwkekvtyinaygptetticattcvatketighsvpigapiqntqiyivdenlqlksvgeagelciggeglargywkrpeltsqkfvdnpfvpgeklyktgdqarwlsdgnieylgridnqvkirghrveleevesillkhmyisetavsvhkdhqeqpylcayfvsekhipleqlrqfsseelptymipsyfiqldkmpltsngkidrkqlpepdltfgmrvdyeaprn';
	open my $q, '>', 'q.tmp' or die $!;
	print $q '>nrpsA' . "\n$a_query\n";
	close $q;
	my $evalue = '1e-20';
	system("makeblastdb -in goset.faa -out goset.db -dbtype prot &> /dev/null");
	system("blastp -query q.tmp -db goset.db -outfmt 6 -out goset.nrpsA.bp -max_target_seqs 10000000 -num_threads 16 -evalue $evalue");
	system("rm q.tmp");
}

open my $bp, '<', 'goset.nrpsA.bp' or die $!;
my %hit = ();
while(<$bp>){
	chomp;
	my ($query, $h, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
	$hit{$h}{'start2end'}{$sstart}{'end'} = $send;
	$hit{$h}{'count'} += 1;
}
## Get mod numbers
foreach my $h ( keys %hit){
	my $m = 1;
	foreach my $s (sort { $a <=> $b } keys %{$hit{$h}{'start2end'}}){
		$hit{$h}{'start2end'}{$s}{'mod'} = $m;
		$m+=1;
	}
}
close $bp;
foreach my $ha (keys %hit){
	my $largest = 0;
	foreach my $n (keys %{$ans{$ha}}){
		$largest = $n if($n > $largest);
	}
	if($hit{$ha}{'count'} != $largest){
		print STDERR "\tPossible issue with $ha:\t" . $hit{$ha}{'count'} . " in blast, $largest in list.\n";
	}#else{
		#print STDERR "$ha looks good.  Both $largest\n";
	#}
}
open my $aout, '>', 'goset_adom.faa' or die $!;
my $nrpsfa = new Bio::SeqIO(-file=>'goset.faa', -format=>'fasta');
while(my $seq = $nrpsfa->next_seq){
	foreach my $s (sort { $a <=> $b } keys %{$hit{$seq->id}{'start2end'}}){
		next unless(exists $ans{$seq->id}{ $hit{$seq->id}{'start2end'}{$s}{'mod'} });
		my $a_dom = $seq->trunc($s, $hit{$seq->id}{'start2end'}{$s}{'end'});
		print $aout '>' . join('_', $seq->id, $s, 'mod' . $hit{$seq->id}{'start2end'}{$s}{'mod'}, $ans{$seq->id}{ $hit{$seq->id}{'start2end'}{$s}{'mod'} } ) . "\n" . $a_dom->seq . "\n";
	}
}
close $aout;
