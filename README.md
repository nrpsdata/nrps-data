All algorithms developed and tested in a Linux environment.

## List of dependancies:
* hmmscan, hmmpress, hmmbuild (part of HMMER)
* perl
* clustalw
* python
* blastp (part of ncbi-blast+)
* mafft
	
## List of python modules:
* json
* glob
* datetime
* os
* re

## List of perl modules:
* Bio::SeqIO
* Bio::TreeIO
* Cwd 'abs_path'
* List::Util qw/shuffle/
