#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my $aamin = 360; ## typical nrpsA is 515AA, so this is about 30% trimming
foreach my $arg (@ARGV){
	my $stem = $arg;
	$stem =~ s/(.*)\.fa(a)?/$1/;
	system("mkdir $stem") unless(-d "$stem");
	## Initial alignment
	print "Aligning & Pruning";
	system("mafft --quiet --namelength 40 $arg > $stem.afa");
	my $totree = undef;
	my $lastafa = "$stem.afa";
	if( failgap("$stem.afa", $aamin) ){
		print '.';
		my $it = 1;
		my $tfi = $stem . '_trim' . "$it.faa";
		my $na = $tfi;
		$na =~ s/\.faa/\.afa/;
		## First trim
		trimmer("$stem.afa", $tfi);
		## Second alignment
		system("mafft --quiet --namelength 40 $tfi > $na");
		while( failgap($na, $aamin) ){
			print '.';
			my $la = $na;
			$it += 1;
			$tfi = $stem . '_trim' . "$it.faa";
			trimmer($la, $tfi);
			$na = $tfi;
			$na =~ s/\.faa/\.afa/;
			system("mafft --quiet --namelength 40 $tfi > $na");
		}
		$lastafa = $na;
		## Clustal format
		$na =~ s/\.afa/\.aln/;
		system("mafft --clustalout --quiet --namelength 40 $tfi > $na");
		$totree = $na;
	}else{
		## Clustal format
		system("mafft --clustalout --quiet --namelength 40 $arg > $stem.aln");
		$totree = "$stem.aln";
	}
	print "DONE!\n";
	system("clustalw -TREE -INFILE=$totree");
	my $mvpref = $lastafa;
	$mvpref =~ s/\.afa//;
	my @mv = ('*.ph', $mvpref . '*');
	system("mv $_ $stem") foreach(@mv);
	my @rm = ($stem . '.a*', $stem . '_*'); 
	system("rm $_") foreach(@rm);
}


sub failgap{ ## gaps/ext test
	my ($aln, $min) = ($_[0], $_[1]);
	my ($tot, $ngaps, $cgaps) = (0,0,0);
	my $afa = new Bio::SeqIO(-file=>$aln, -format=>'fasta');
	while(my $seq = $afa->next_seq){
		$tot += 1;
		if($tot == 1){ #for the first seq, check against min
			my $s = $seq->seq;
			$s =~ s/-//g;
			return 0 if(length($s) <= $min);
		}
		if($seq->seq =~ m/^-/){
			$ngaps += 1;
		}
		if($seq->seq =~ m/-$/){
			$cgaps += 1;
		}
	}
	if($ngaps == 0 && $cgaps == 0){
		return 0;
	}else{
		return 1;
	}
}

sub trimmer{
	my ($aln, $out) = @_;
	## Parse and trim
	my ($ftrim, $rtrim) = (0, 0);
	my %gaps = ();
	my ($last, $current, $alignment) = ('','','');
	my $afa = new Bio::SeqIO(-file=>$aln, -format=>'fasta');
	while(my $seq = $afa->next_seq){
		if($seq->seq =~ m/^-/){
			$ftrim = 1;
			$gaps{$seq->id}{'left'} = 1;
		}
		if($seq->seq =~ m/-$/){
			$rtrim = 1;
			$gaps{$seq->id}{'right'} = 1;
		}
	}
	if($ftrim == 0 && $rtrim == 0){ ## no trimming
		system("cp $aln $out");
		die "Died.  Issue: No trims found necessary in $aln.  Copied to $out\n";
	}else{
		$aln =~ s/\.afa/\.faa/;
		open my $tfh, '>', $out or die $!;	
		my $faa = new Bio::SeqIO(-file=>$aln, -format=>'fasta');
		if($ftrim == 0){ ## C trim only
			while(my $seq = $faa->next_seq){
				$seq = $seq->trunc(1, $seq->length - 1) unless(exists $gaps{$seq->id}{'right'});
				print $tfh '>' . $seq->id . "\n" . $seq->seq . "\n";
			}
		}elsif($rtrim == 0){ ## N trim only
			while(my $seq = $faa->next_seq){
				$seq = $seq->trunc(2, $seq->length) unless(exists $gaps{$seq->id}{'left'});
				print $tfh '>' . $seq->id . "\n" . $seq->seq . "\n";
			}
		}else{ ## Both trim
			while(my $seq = $faa->next_seq){
				$seq = $seq->trunc(1, $seq->length - 1) unless(exists $gaps{$seq->id}{'right'});
				$seq = $seq->trunc(2, $seq->length) unless(exists $gaps{$seq->id}{'left'});
				print $tfh '>' . $seq->id . "\n" . $seq->seq . "\n";
			}
		}
		close $tfh;
	}
}
