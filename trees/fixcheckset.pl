#!/bin/env perl

use strict;
use warnings;

my %ft = ();
while(<>){
	unless($_ =~ m/^#/){
		chomp;
		my ($f, $t) = split(/\t/, $_);
		$ft{$f} = $t;
	}
}
my $chk = 'khayatt_goset_mibig_check.faa';
foreach my $f (keys %ft){
	my $cmd = 'perl -pi -e ' . "'s" . '/' . $f . '/' . $ft{$f} . '/g' . "' $chk"; 
	system("$cmd");
}
