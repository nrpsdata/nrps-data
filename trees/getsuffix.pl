#!/bin/env perl

use strict;
use warnings;

my %seen = ();
while(<>){
	if($_ =~ m/^>/){
		chomp;
		my @head = split(/_/, $_);
		$seen{$head[-1]} += 1		
	}
}

foreach my $s (sort { $seen{$b} <=> $seen{$a} } keys %seen){
	print join("\t", $s, $seen{$s}) . "\n";
}
