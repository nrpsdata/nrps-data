CLUSTAL W multiple sequence alignment


Q9FB27_A1_asn                            VALDADRRRIA---------AHPAGPTGIATTPDAPAYVVYTSGTTGKPNGVRVPHRGLT
Q9FB23_A2_asn                            AGLDTPLRACGLPVVAPDDLGAPIAP--VSVHPEQLAAVMATSGSTGTPKTIGVPQRALA
                                         ..**:  *  .         . * .*  ::. *:  * *: ***:**.*: : **:*.*:

Q9FB27_A1_asn                            NYLTWCTGAYGLDGGTGTLVHTSISFDLTLTTLFGPLLAGGQVVMLSETAGVTGLIAALR
Q9FB23_A2_asn                            GYLRWAIGHYRLDEETVSPVHSSLGFDLTVTALLAPLAAGGQ-ARLTDSGDPGALGAALA
                                         .** *. * * **  * : **:*:.****:*:*:.** **** . *:::..  .* *** 

Q9FB27_A1_asn                            SRRDLTLVKLTPTHL-DVVNQLLTPDELRGAVRTLVVGGEAVRAESLEPFR--ASGTRVV
Q9FB23_A2_asn                            AGHH-TLLKITPAHLAALAHQLGAP----TALRTVVAGGEPLHAGHVRALRAFAPGARLV
                                         : :. **:*:**:**  :.:** :*     *:**:*.***.::*  :..:*  *.*:*:*

Q9FB27_A1_asn                            NEYGPSETVVGSVAHVVDAATPRTGPVPIGRPIANTTVHLLDQRRRPVPDGVVGELWIGG
Q9FB23_A2_asn                            NEYGPTETTVGCCAHDV-APDPGEAPIPVGTPIAGLSACVVDD-ALPAPPGVRGELYIGG
                                         *****:**.**. ** * *. *  .*:*:* ***. :. ::*:   *.* ** ***:***

Q9FB27_A1_asn                            AGVADGYLGRPELTGERFLPSDYPPDGGRVYRTGDLARRRADGTLEYLGRTDAQVKIRGV
Q9FB23_A2_asn                            TGVTRGYLGRPAATAAAYVPDPAAP-GARRYRTGDLARRLPDGTLLLAGRADRQVKIRGH
                                         :**: ******  *.  ::*.  .* *.* ********* .****   **:* ****** 

Q9FB27_A1_asn                            RVEPAETEAVLASHPGVGQAVVVARLDEDPGRSSPLAGELTLTGYVVPARGAQAPPHE
Q9FB23_A2_asn                            RVEPGEVEQVLGGHPGVREAAVVAHPAPGGGRR--------LVAYWVPAEPARPPSAD
                                         ****.*.* **..**** :*.***:   . **         *..* ***. *:.*. :
