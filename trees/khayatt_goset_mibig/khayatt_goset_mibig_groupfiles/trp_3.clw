CLUSTAL W multiple sequence alignment


Q93N89_A2_2__trp                         TLDEDLPEGDFDDGRVLTAPDADHIAYVIHTSGSTGTPKGAAVTWGGLRNLVADRIERYG
Q93N89_A2_trp                            TLDEDLPEGDFDDGRVLTAPDADHIAYVIHTSGSTGTPKGAAVTWGGLRNLVADRIERYG
                                         ************************************************************

Q93N89_A2_2__trp                         IGTDTRLVQLVSPSFDVSMADIWPTLCAGGRLVLAPPGGHTTGDELGDLLADHRITHAVM
Q93N89_A2_trp                            IGTDTRLVQLVSPSFDVSMADIWPTLCAGGRLVLAPPGGHTTGDELGDLLADHRITHAVM
                                         ************************************************************

Q93N89_A2_2__trp                         PAVQLTHLPDRELPALRVLVSGGDALPADTRRRWVARCDLHNEYGVTEATVVSTVTAPLD
Q93N89_A2_trp                            PAVQLTHLPDRELPALRVLVSGGDALPADTRRRWVARCDLHNEYGVTEATVVSTVTAPLD
                                         ************************************************************

Q93N89_A2_2__trp                         DAGPLTIGGPIARAGVHVLDGFLRPVPPGVTGELYVTGTGVARGYLNRPTLTAHRFIADP
Q93N89_A2_trp                            DAGPLTIGGPIARAGVHVLDGFLRPVPPGVTGELYVTGTGVARGYLNRPTLTAHRFIADP
                                         ************************************************************

Q93N89_A2_2__trp                         RTRGGRMYRTGDLVRHTHGGELVFVGRADEQVKLRGHRIELGEVEAALADHPDVEQAVAA
Q93N89_A2_trp                            RTRGGRMYRTGDLVRHTHGGELVFVGRADEQVKLRGHRIELGEVEAALADHPDVEQAVAA
                                         ************************************************************

Q93N89_A2_2__trp                         IHDARLIGYVVPADGRVPDPSA
Q93N89_A2_trp                            IHDARLIGYVVPADGRVPDPSA
                                         **********************
