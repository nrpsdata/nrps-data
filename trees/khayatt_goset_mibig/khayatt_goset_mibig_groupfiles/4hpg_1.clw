CLUSTAL W multiple sequence alignment


BGC0000419_AIG79241.1_462_mod1_4hpg      VCTEAHRSAVLDGGLAPIVLDEPETRRAVAESARRSIGVTADDVAYVMYTSGSTGTPKGV
BGC0001178_AGS77309.1_471_mod1_4hpg      VCAEAFRAAVLDVGLDPIVLDDPRTRQSVAESAPRTVATGADDLAYVMYTSGSTGTPKGV
                                         **:**.*:**** ** *****:*.**::***** *::.. ***:****************

BGC0000419_AIG79241.1_462_mod1_4hpg      AVSHGNVAALAGEPGWDVGPEDAVLMHASHAFDISLFEMWVPLLTGARLVLAGSGVVDGE
BGC0001178_AGS77309.1_471_mod1_4hpg      AVSHGNVAALAGEPGWGVGPGDTVLMHASHAFDISLFELWVPLLSGARVMIAGPGAVDGE
                                         ****************.*** *:***************:*****:***:::**.*.****

BGC0000419_AIG79241.1_462_mod1_4hpg      ALAGYVASGVTAAHLTAGSFRVLAEEAPESVAGLREVLTGGDAVPPAAVERVRRTCPDVR
BGC0001178_AGS77309.1_471_mod1_4hpg      ALAGHVAGGVTAAHLTAGAFRVLAEESPESVAGLREVLTGGDAVPPAAVERVRRACPDVR
                                         ****:**.**********:*******:***************************:*****

BGC0000419_AIG79241.1_462_mod1_4hpg      VRHLYGPTEATLCATWWLLEPGDEMGAVLPIGRPLAGRRVYVLDAFLRPVPQGLPGELYV
BGC0001178_AGS77309.1_471_mod1_4hpg      VRHLYGPTEATLCATWWLLEPGDETGSALPIGRPLAGRRAYVLDAFLRPLPPGVKGELYV
                                         ************************ *:.***********.*********:* *: *****

BGC0000419_AIG79241.1_462_mod1_4hpg      AGAGVAQGYLGRSMLTAERFVADPFVAGGRMYRTGDLAYWTENGTLAFAGRADDQVKIRG
BGC0001178_AGS77309.1_471_mod1_4hpg      AGAGVARGYLGRPALTAERFVADPFAPGGRMYRTGDLAYWTDQGALVFAGRADDQVKIRG
                                         ******:*****. ***********..**************::*:*.*************

BGC0000419_AIG79241.1_462_mod1_4hpg      YRVEPGEIEVALARLPGVAQAVVVARGEHLIGYAVPEAGQALDPAH
BGC0001178_AGS77309.1_471_mod1_4hpg      YRVEPGEVEAVLGRLPGVGQAVVSVREEHLIGYVVAEAGQDVDPAR
                                         *******:*..*.*****.**** .* ******.*.**** :***:
