CLUSTAL W multiple sequence alignment


O52821_A1_7__dpg|dhpg                    LCTQDTSGVVPENAIVLDAPDTRASIEDCADTAPEIRLYAGDLAYVMYTSGSTGLPKGVA
Q939Y9_A1_dpg|dhpg                       LCTLATSAVAPGNAIVLDAPETRVAVRDCA--APEIRPHADDLAYVMYTSGSTGLPKGVA
Q939Y9_A1_7__dpg|dhpg                    LCTLATSAVAPGNAIVLDAPETRVAVRDCA--APEIRPHADDLAYVMYTSGSTGLPKGVA
                                         ***  **.*.* ********:**.::.***  ***** :*.*******************

O52821_A1_7__dpg|dhpg                    IPHGAVAGLAGDSGWQIGPDDGVLMHATHVFDPSLYAMWVPLVAGGRVLLTEPGVLDAAG
Q939Y9_A1_dpg|dhpg                       IPHGAVAGLAGDAGWQIGPGDGVLMHATHVFDPSLYAMWVPLVSGARVLLTEPGVLDAAG
Q939Y9_A1_7__dpg|dhpg                    IPHGAVAGLAGDAGWQIGPGDGVLMHATHVFDPSLYAMWVPLVSGARVLLTEPGVLDAAG
                                         ************:******.***********************:*.**************

O52821_A1_7__dpg|dhpg                    VRQAVERGATAVHLTAGSFRALAETSPECFAGLVEIGTGGDVVPAQSVANLRRAQPGLRV
Q939Y9_A1_dpg|dhpg                       VRQAVHRGATFVHLTAGTFRALAETAPECFEGLVEIGTGGDVVPLQSVENLRRAQPGLRV
Q939Y9_A1_7__dpg|dhpg                    VRQAVHRGATFVHLTAGTFRALAETAPECFEGLVEIGTGGDVVPLQSVENLRRAQPGLRV
                                         *****.**** ******:*******:**** ************* *** ***********

O52821_A1_7__dpg|dhpg                    RNTYGPTETTLCATWLPIEPGDVIDRELPIGHPMTNRKIYILDAFLRPVPPGVAGELYIA
Q939Y9_A1_dpg|dhpg                       RNTYGPTETTLCATWLPIEPGEVLGRELPIGHPMTNRRIYLLDAFLRPVPPGVAGELYIA
Q939Y9_A1_7__dpg|dhpg                    RNTYGPTETTLCATWLPIEPGEVLGRELPIGHPMTNRRIYLLDAFLRPVPPGVAGELYIA
                                         *********************:*:.************:**:*******************

O52821_A1_7__dpg|dhpg                    GTGLARGYLDSPGLTADRFVACPFLAGERMYRTGDVARWTRDGEVVFLGRADDQVKIRGY
Q939Y9_A1_dpg|dhpg                       GTGLAHGYLKSPGLTAGRFVACPFAAGERMYRTGDRARWTRDGEVVFLGRADDQVKIRGY
Q939Y9_A1_7__dpg|dhpg                    GTGLAHGYLKSPGLTAGRFVACPFAAGERMYRTGDRARWTRDGEVVFLGRADDQVKIRGY
                                         *****:***.******.******* ********** ************************

O52821_A1_7__dpg|dhpg                    RVELGEVEAVLAAQPGVVEAVVMAREDQPGEKRLVGYFVSDGSDAGPAE
Q939Y9_A1_dpg|dhpg                       RVELGEVEAALAAQPGVVEAVVTAREDQPGEKRLVGYFVSDGGDAGPVE
Q939Y9_A1_7__dpg|dhpg                    RVELGEVEAALAAQPGVVEAVVTAREDQPGEKRLVGYFVSDGGDAGPVE
                                         *********.************ *******************.****.*
