CLUSTAL W multiple sequence alignment


P39845_A2_orn                            LLQRELKHLISNSPESEMSHIFLDDEGSFEESNCNLNLSPAPEEPVYIIYTSGTTGAPKG
O87606_A2_orn                            LLQNEVKHLIAGSDIGDISRVCLDDESFYESKKMHLSSSPAPEDSAYIIYTSGTTGAPKG
Q70JX1_A2_orn                            MVQSEYKELASQLTDHNLFLIQLDLEDQYDICAKNIQPSASPDDTAYIIYTSGTTGTPKG
                                         ::* * *.* :     ::  : ** *. ::    ::. *.:*::..**********:***

P39845_A2_orn                            VIVTYQNFTHAALAWRQIYELDRKPVRLLQIASFSFDVFSGDLARTLTNGGTLIVCPDET
O87606_A2_orn                            VIVTHRNFAHAVLAWRRIYQLDQMPVRLLQMASFSFDVFSGDLARTLANGGTLVICPDET
Q70JX1_A2_orn                            VEVRNRSFTHAALAWRRIYELDLIPVRVLQMASFSFDVFSGDLARALLNGGTLVICPDDV
                                         * *  :.*:**.****:**:**  ***:**:**************:* *****::***:.

P39845_A2_orn                            RLEPAEIYKIIKSQRITVMESTPALIIPVMEYVYRNQFKLPDLDILILGSDMVKAQDFKT
O87606_A2_orn                            RLEPAELYALMNRQRITIMESTPALIVPFMEYVYRNQLSLPDLDILILGSDMVKAHDFKT
Q70JX1_A2_orn                            RLEPQQLYRLIDQHRITFMESTPALVVPFMEYIYRRKLALQSVKILVLGSDMIKSQDFYT
                                         **** ::* ::. :***.*******::*.***:**.:: * .:.**:*****:*::** *

P39845_A2_orn                            LTDRFGQSMRIINSYGVTEATIDSSFYETSMGGECTGDNVPIGSPLPNVHMYVLSQTDQI
O87606_A2_orn                            LADRFGNKMRLINSYGVTEATIDSSYYEMNMGEEYSGDSVPIGIPLPNVKLCVLSQTDQI
Q70JX1_A2_orn                            LHERFGKEMRIINSYGVTEATIDSSYYEAEMSEEPREDDVPIGVPLPNVQMYVLNKDKQV
                                         * :***:.**:**************:** .*. *   *.**** *****:: **.: .*:

P39845_A2_orn                            QPIGVAGELCIGGAGVAKGYHHKPDLTQMKFTENPFVSGERLYRTGDRACWLPNGTIRLL
O87606_A2_orn                            QPIGIAGELCIAGAGVAKGYHGKFELTEKKFTENPFVPGERLYRTGDLACWLPNGTLRLL
Q70JX1_A2_orn                            QPIGVFGELYIGGAGVAKGYWGQPELTEGAFS-DPLQMGETLYRTGDQACWLPDGTLRFQ
                                         ****: *** *.********  : :**:  *: :*:  ** ****** *****:**:*: 

P39845_A2_orn                            GRMDYQVKINGYRIETEEIESVLLQTGLVREAAVAVQHDKNGQAGLAAYIVPSDVNTNA
O87606_A2_orn                            GRIDHQVKINGYRIETEEIESVLLQTGLVNEAVVAVQNDTNGQARLAAYILPSDADTTA
Q70JX1_A2_orn                            GRIDKQVKIRGYRIETGEIESVLLKHDQVKEAAVTVIKDAEGQARLAAYIVPKEADTSS
                                         **:* ****.****** *******: . *.**.*:* :* :*** *****:*.:.:*.:
