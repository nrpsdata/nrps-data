CLUSTAL W multiple sequence alignment


Q65NK5_A1_gln                            ITDSGLTFETAETVQFSEALSESRENGYPSSAAGAGHLAYIIYTSGTTGRPKGVMIEHRQ
O66069_A1_gln                            ITDSGLTFETAETVRFSEALSESLENGHPSSEAGAGHLAYIIYTSGTTGRPKGVMIEHRQ
Q45295_A1_gln                            ITDSGLTFETTETVRFSEALSESLENGHPSSEAGAGHLAYIIYTSGTTGRPKGVMIEHRQ
                                         **********:***:******** ***:*** ****************************

Q65NK5_A1_gln                            VHHLVRGLQQAVGTYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVSDGRA
O66069_A1_gln                            VHHLVRGLQQAVGAYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVSDGRA
Q45295_A1_gln                            VHHLVRGLQQAVGAYDQDDLKLALLAPFHFDASVQQIFTSLLLGQTLYIVPKKTVSDGRA
                                         *************:**********************************************

Q65NK5_A1_gln                            LSDYYRRHQIDVTDGTPAHLQLLAAADDLSGVKLRHMLVGGEALSRVATERLLQLFAETA
O66069_A1_gln                            LSDYYRRHQIDVTDGTPAHLQLLSAADDLSGVKLRHMLVGGEALSRVATERLLQLFAETA
Q45295_A1_gln                            LSDYYRRHQIDVTDGTPAHLQLLSAADDLSGVKLRHMLVGGEALSRVATERLLQLFAETA
                                         ***********************:************************************

Q65NK5_A1_gln                            ESVPAVTNVYGPTETCVDASSFTITNRTDLQYDTAYVPIGRPIGNNRFYILDENGALLPD
O66069_A1_gln                            ESVPDVTNVYGPTETCVDASSFTMTNHADLQGDTAYVPIGRPIGNNRFYILDENGALLPD
Q45295_A1_gln                            ESVPDVTNVYGPTETCVDASSFTMTNHADLQADTAYVPIGRPIGNNRFYILDEGGALLPD
                                         **** ******************:**::*** *********************.******

Q65NK5_A1_gln                            GVEGELYIAGDGVGRGYLNLPDMTRDRFLKDPFVSGGLMYRTGDTARWLPDGTVDFIGRR
O66069_A1_gln                            GVEGELYIAGDGVGRGYLNLPDMTADKFLEDPFVPGGFMYRTGDAVRWLPDGTVDFIGRK
Q45295_A1_gln                            GVEGELYIAGDGVGRGYLNLPDMTADKFLEDPFVPGGFMYRTGDAVRWLPDGTVDFIGRK
                                         ************************ *:**:****.**:******:.*************:

Q65NK5_A1_gln                            DDQVKIRGFRIELGEIESVLQGAPAVEKAVVLARHETGGSLEVCAYVVPKQGGKIHI
O66069_A1_gln                            DDQVKIRGYRIELGEIESVLQGAPAVGKAVVLARPETGGSLEVCAYVVPKQSGEIHL
Q45295_A1_gln                            DDQVKIRGYRIELGEIESVLQGAPAVGKAVVLARPETGGSLEVCAYVVPKQSGEIHL
                                         ********:***************** ******* ****************.*:**:
