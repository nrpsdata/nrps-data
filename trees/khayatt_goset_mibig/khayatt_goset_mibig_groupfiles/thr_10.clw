CLUSTAL W multiple sequence alignment


BGC0000369_AHZ20773.1_478_mod1_thr       LTVENLLSTIPVNIAEILVI------------DKI--EF--APNVILTSQ-----ILSHQ
AHZ20781.1_481_mod1_thr                  ITEETLLSELTDEKITAFTL------------KSIESELIQQPQ---TS--PQITVKPEF
AHZ20781.1_1540_mod2_thr                 ITEKALISQLPECEKEILIL------------EDIVTQL--KPE---NSQPPIVRILPDC
BGC0000369_AHZ20781.1_1540_mod2_thr      ITEKALISQLPECEKEILIL------------EDIVTQL--KPE---NSQPPIVRILPDC
BGC0000369_AHZ20781.1_481_mod1_thr       ITEETLLSELTDEKITAFTL------------KSIESELIQQPQ---TS--PQITVKPEF
AHZ20773.1_478_mod1_thr                  LTVENLLSTIPVNIAEILVI------------DKI--EF--APNVILTSQ-----ILSHQ
Q847C7_A1_6__thr                         LTQKSLLPSLPENQAIVMCLDRDWGVIAACSQENIVSHA--QPQNL--------------
                                         :* : *:. :.      : :            ..*  .    *:                

BGC0000369_AHZ20773.1_478_mod1_thr       TAYIIYTSGSTGQPKGCLVSHNNVVRLMENTQAWFEFNSKDVWTMFHSFAFDFSVWEMWG
AHZ20781.1_481_mod1_thr                  PAYIIYTSGSTGTPKGCIVTHSNAIRLMRSTEPWFGFNENDIWTLFHSFAFDFSVWELWG
AHZ20781.1_1540_mod2_thr                 PAYVIYTSGSTGTPKGCIVTHSNVIRLLKATASWFEFNSQDVWTLFHSYAFDFSVWEIWG
BGC0000369_AHZ20781.1_1540_mod2_thr      PAYVIYTSGSTGTPKGCIVTHSNVIRLLKATASWFEFNSQDVWTLFHSYAFDFSVWEIWG
BGC0000369_AHZ20781.1_481_mod1_thr       PAYIIYTSGSTGTPKGCIVTHSNAIRLMRSTEPWFGFNENDIWTLFHSFAFDFSVWELWG
AHZ20773.1_478_mod1_thr                  TAYIIYTSGSTGQPKGCLVSHNNVVRLMENTQAWFEFNSKDVWTMFHSFAFDFSVWEMWG
Q847C7_A1_6__thr                         -AYVIYTSGSTGKPKGVLINHQNVIRLFAATQAWYHFGASDVFTLFHSIAFDFSVWELWG
                                          **:******** *** ::.*.*.:**:  * .*: *. .*::*:*** ********:**

BGC0000369_AHZ20773.1_478_mod1_thr       ALLYGGKLVIVPYLESRSPTAFRTLLQQEGVTILNQTPSAFRQLIRADE---EFADKLNS
AHZ20781.1_481_mod1_thr                  ALLYGGKVIIVPFWLSRTPETFREFLTTEGVTVLNQTPSAFYQLIRTDE---ASVGEL-A
AHZ20781.1_1540_mod2_thr                 ALLYGGQVVVVPYWTSRSPADFFQLLQTHKVTVLNQTPSAFKQLIPIAK---EKQEKL-P
BGC0000369_AHZ20781.1_1540_mod2_thr      ALLYGGQVVVVPYWTSRSPADFFQLLQTHKVTVLNQTPSAFKQLIPIAK---EKQEKL-P
BGC0000369_AHZ20781.1_481_mod1_thr       ALLYGGKVIIVPFWLSRTPETFREFLTTEGVTVLNQTPSAFYQLIRTDE---ASVGEL-A
AHZ20773.1_478_mod1_thr                  ALLYGGKLVIVPYLESRSPTAFRTLLQQEGVTILNQTPSAFRQLIRADE---EFADKLNS
Q847C7_A1_6__thr                         ALLYGGSLVIVPYWVSRDPSAFHTLLRQEQVTVLNQTPSAFRQLIRVEELAKTGESQL-S
                                         ******.:::**:  ** *  *  :*  . **:******** ***   :       :* .

BGC0000369_AHZ20773.1_478_mod1_thr       LRVIILGGEALELQSLKPWIKRYGDSHPRLINMYGITETTVHVTYRPILEEDILENRGSV
AHZ20781.1_481_mod1_thr                  LRYVIFGGEALDLQSLQPWLEKYGDKKPRLVNMYGITETTVHVTYRPISRQD-LKVKASV
AHZ20781.1_1540_mod2_thr                 LRYVIFGGEALELPSLQPWTELYGDEQPKLINMYGITETTVHVTYRPITQADIISNRGSL
BGC0000369_AHZ20781.1_1540_mod2_thr      LRYVIFGGEALELPSLQPWTELYGDEQPKLINMYGITETTVHVTYRPITQADIISNRGSL
BGC0000369_AHZ20781.1_481_mod1_thr       LRYVIFGGEALDLQSLQPWLEKYGDKKPRLVNMYGITETTVHVTYRPISRQD-LKVKASV
AHZ20773.1_478_mod1_thr                  LRVIILGGEALELQSLKPWIKRYGDSHPRLINMYGITETTVHVTYRPILEEDILENRGSV
Q847C7_A1_6__thr                         LRLVIFGGEALEPQSLQPWFEGYKDQSPQLVNMYGITETTVHVTYRPLSIAD-VNNSKSL
                                         ** :*:*****:  **:** : * *. *:*:****************:   * :.   *:

BGC0000369_AHZ20773.1_478_mod1_thr       IGVPIADLSLYILDSGFEPSPYGVPGEIYVGGMGVSRGYLNRSALTAERFIPDPFSQQLG
AHZ20781.1_481_mod1_thr                  IGREIPDLSIYLLDEKLEPVADGIPGEIYVAGAGVTGGYLNRPGLTAERFLPNPFG---S
AHZ20781.1_1540_mod2_thr                 IGQPIPDLQLYILDENLEPSPLCVPGEIYVGGAGVTRGYLHQPRLSAERFIPNPYSQIPG
BGC0000369_AHZ20781.1_1540_mod2_thr      IGQPIPDLQLYILDENLEPSPLCVPGEIYVGGAGVTRGYLHQPRLSAERFIPNPYSQIPG
BGC0000369_AHZ20781.1_481_mod1_thr       IGREIPDLSIYLLDEKLEPVADGIPGEIYVAGAGVTGGYLNRPGLTAERFLPNPFG---S
AHZ20773.1_478_mod1_thr                  IGVPIADLSLYILDSGFEPSPYGVPGEIYVGGMGVSRGYLNRSALTAERFIPDPFSQQLG
Q847C7_A1_6__thr                         IGVPIPDLQLYILDEQLKPLPIGIKGEMYIGGAGLARGYLNRPELTAERFIPNPFSDAPE
                                         **  *.**.:*:**. ::* .  : **:*:.* *:: ***::. *:****:*:*:.    

BGC0000369_AHZ20773.1_478_mod1_thr       ARLYRTGDLARRLRNGDIEYLGRCDLQVKVRGFRIELGEIEAALTALPEVLEAVVIVYSE
AHZ20781.1_481_mod1_thr                  GRIYRSGDLAQRLSNGDLEYLGRIDQQVKIRGFRIELGEIQAALTSHFQVREAVVIT-DE
AHZ20781.1_1540_mod2_thr                 SRLYRSGDLARRLPEGEIEYLGRADQQIKIRGFRIELGEITGVINSHPQVKQALVMVQKA
BGC0000369_AHZ20781.1_1540_mod2_thr      SRLYRSGDLARRLPEGEIEYLGRADQQIKIRGFRIELGEITGVINSHPQVKQALVMVQKA
BGC0000369_AHZ20781.1_481_mod1_thr       GRIYRSGDLAQRLSNGDLEYLGRIDQQVKIRGFRIELGEIQAALTSHFQVREAVVIT-DE
AHZ20773.1_478_mod1_thr                  ARLYRTGDLARRLRNGDIEYLGRCDLQVKVRGFRIELGEIEAALTALPEVLEAVVIVYSE
Q847C7_A1_6__thr                         ARLYKTGDLARYLENGDIEYLDRIDNQVKIRGFRIELGEIEAALLKYPEVQEAVVMARTD
                                         .*:*::****: * :*::***.* * *:*:********** ..:    :* :*:*:.   

BGC0000369_AHZ20773.1_478_mod1_thr       AEDDQRLVAYIV-SNSEIDYN
AHZ20781.1_481_mod1_thr                  WEEEKRLVAYYVPGESQLHGN
AHZ20781.1_1540_mod2_thr                 TTGENRIVAYFT-SDTAPHLK
BGC0000369_AHZ20781.1_1540_mod2_thr      TTGENRIVAYFT-SDTAPHLK
BGC0000369_AHZ20781.1_481_mod1_thr       WEEEKRLVAYYVPGESQLHGN
AHZ20773.1_478_mod1_thr                  AEDDQRLVAYIV-SNSEIDYN
Q847C7_A1_6__thr                         QPGDKRLVAYIVAKSSNPASE
                                            ::*:*** .  .:    :
