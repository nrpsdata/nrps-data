CLUSTAL W multiple sequence alignment


Q93N89_A1_hpg|hpg2Cl                     VLKNSTPAVTIDAVVEGAGDASSVALDGDDLAYVMYTSGSTGTPKGVAVPHGSVAALVGE
Q93N89_A1_1__hpg|hpg2Cl                  VLKNSTPAVTIDAVVEGAGDASSVALDGDDLAYVMYTSGSTGTPKGVAVPHGSVAALVGE
                                         ************************************************************

Q93N89_A1_hpg|hpg2Cl                     SGWGLGPGDSVLFHAPHAFDISLFEVWVPLASGARVVIAEPGVAVDAAAVRRHIAAGVTH
Q93N89_A1_1__hpg|hpg2Cl                  SGWGLGPGDSVLFHAPHAFDISLFEVWVPLASGARVVIAEPGVAVDAAAVRRHIAAGVTH
                                         ************************************************************

Q93N89_A1_hpg|hpg2Cl                     VHVTAGLFRVLAEEAPDCFTGAREVLTGGDVVPLEAVERVRAACPDVRVRHLYGPTESTL
Q93N89_A1_1__hpg|hpg2Cl                  VHVTAGLFRVLAEEAPDCFTGAREVLTGGDVVPLEAVERVRAACPDVRVRHLYGPTESTL
                                         ************************************************************

Q93N89_A1_hpg|hpg2Cl                     CATWHLVEPGDDTARVLPIGHPLTNRHIHVLDDSLRRVAPGVTGELYIAGAGLARGYLKR
Q93N89_A1_1__hpg|hpg2Cl                  CATWHLVEPGDDTARVLPIGHPLTNRHIHVLDDSLRRVAPGVTGELYIAGAGLARGYLKR
                                         ************************************************************

Q93N89_A1_hpg|hpg2Cl                     AGLSAERFVACPFADGERMYRTGDLARWTDDGELAFAGRADAQVKIRGFRVELGEVEAAL
Q93N89_A1_1__hpg|hpg2Cl                  AGLSAERFVACPFADGERMYRTGDLARWTDDGELAFAGRADAQVKIRGFRVELGEVEAAL
                                         ************************************************************

Q93N89_A1_hpg|hpg2Cl                     AAQPAVAQAVVVAREDRPGEKRLVGYLVPDGHQADSDV
Q93N89_A1_1__hpg|hpg2Cl                  AAQPAVAQAVVVAREDRPGEKRLVGYLVPDGHQADSDV
                                         **************************************
