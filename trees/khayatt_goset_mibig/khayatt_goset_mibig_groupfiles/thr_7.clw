CLUSTAL W multiple sequence alignment


AEF33078.1_463_mod1_thr                  -----LTDTDGADRVRGAVHPLLLDDPALAAELTGLPGTDPTAAERPAAAGPDSLAYVIY
AHH53506.1_4093_mod4_thr                 ETVTPLTGSLPERKV------LLVDDPDVRARIAATPGHPLGEHERPAPPSPAHPAYAIY
                                              **.:    :*      **:*** : *.::. **      ****...*   **.**

AEF33078.1_463_mod1_thr                  TSGSTGRPKGVAVTHHNVLRLFRQTDHWFGFGADDVWTLFHSYAFDFSVWEIWGPLLHGG
AHH53506.1_4093_mod4_thr                 TSGSTGMPKGVVVTHDNVTRLFAAAAELFDFGPADVWSLFHSSAFDFSVWELWGALLHGG
                                         ****** ****.***.** ***  : . *.**. ***:**** ********:**.*****

AEF33078.1_463_mod1_thr                  RLVVVPFDTSRSPADFLRLLVRERVTVLNQTPSAFYQLIAAERERPDLAGRLTLRHVVFG
AHH53506.1_4093_mod4_thr                 RAVVVPFEVSRSPEDLLRLLTEHRVTVLNQTPSAFYQLAQALAER---QAEFAPRYVIFG
                                         * *****:.**** *:****...***************  *  **    ..:: *:*:**

AEF33078.1_463_mod1_thr                  GEALETHRLEDWYARHGDDAPRLVNMYGITETTVHVTYAALDRETVRAARGSVIGEPIPD
AHH53506.1_4093_mod4_thr                 GEALDPSRLAAWF-DHGSAGTELVNMYGITETTVHVTFRKLDERSTRRA-NSVIGAPLAD
                                         ****:. **  *:  **. ...***************:  **..:.* * .**** *:.*

AEF33078.1_463_mod1_thr                  LDVYVLDGELRPVPEGATGELYVAGPGLARGYLGRPGLTAERFVADPFGAPGGRMYRTGD
AHH53506.1_4093_mod4_thr                 LGGYVLDELLRPVPTGVPGELYVSGAGLARGYLGRAGLTATRFVANPFSPSGQRLYRTGD
                                         *. ****  ***** *..*****:*.*********.**** ****:**...* *:*****

AEF33078.1_463_mod1_thr                  LACVLDDGSLGYLGRADSQVKIRGFRIELGEIEAALERHPGVERAAVVVREDRPGDQRLV
AHH53506.1_4093_mod4_thr                 LVRSTVDGELEYLGRADRQVQLRGFRVELGEIEARLVDHTAVSQAAVVAREDTPADVRLV
                                         *.    **.* ****** **::****:******* *  *..*.:****.*** *.* ***

AEF33078.1_463_mod1_thr                  AYAVPAAGTAPDTT
AHH53506.1_4093_mod4_thr                 AYLVPATGARPSAA
                                         ** ***:*: *.::
