CLUSTAL W multiple sequence alignment


Q8G983_A1_6__ser                         LTSQSLTSLLPANLAKIICLDSDWDLIAKENPDNFESGVTVENLAYVIYTSGSTGQPKGA
Q9S1A9_A1_6__ser                         ITAESLRGLLGEYRGIVVALDTDWPAISQESQNNCDSAVTGENLAYVIYTSGSTGKPKGV
                                         :*::** .**    . ::.**:**  *::*. :* :*.** **************:***.

Q8G983_A1_6__ser                         MNCHRGVVNRLLWMQDTYPLTQGDRILQKTPFSFDVSVWEFFCPLLTGARLVVAKPEGHK
Q9S1A9_A1_6__ser                         MNNHKGIRNRLLWMQDTYQLTKSDGILQKTPFSFDVSVWEFFWPLLAGATLVVAKPEGHK
                                         ** *:*: ********** **:.* ***************** ***:** **********

Q8G983_A1_6__ser                         DSVYLIKLIQDQQITTLHFVPSMLRVLLQEADLEKCQSLKRVICSGEALPNDLSQRFFER
Q9S1A9_A1_6__ser                         DSTYLIQLIQKQQITTLHFVPSMLRVFLQEPELKECSSLKRVFCSGEALSLDLTQRFFEH
                                         **.***:***.***************:***.:*::*.*****:******. **:*****:

Q8G983_A1_6__ser                         FNCELHNLYGPTEAAIDVTYWACSPNWKREVVPIGRPVANTQIYLLNADLQPVPIGVVGE
Q9S1A9_A1_6__ser                         FDCELHNLYGPTEAAIDVTYWPCLPENQKAIVSIGQPIANTQIYILNPDLQPVPIGIVGE
                                         *:*******************.* *: :: :*.**:*:******:**.********:***

Q8G983_A1_6__ser                         LHIGGVQLARGYFNRPELTAEKFIANPFYPSLQKEQGSTISPRLYKTGDLARYLPDGNIE
Q9S1A9_A1_6__ser                         LHIGGIGLARGYLNRPELTAEKFIPNPF-AKVEGEIGGEIRAKLYKTGDLARYLPDGNIE
                                         *****: *****:***********.*** ..:: * *. * .:*****************

Q8G983_A1_6__ser                         FLGRLDHQVKIRGFRIELGEIETILGQHPDVCQSVVLAHQTETGSQTLIAYVVSQSQTRQ
Q9S1A9_A1_6__ser                         FLGRIDHQVKIRGFRIELGEIETILSQHPAVEQAVVIASETETGSKTLIAYVVGQSQEGE
                                         ****:********************.*** * *:**:* :*****:*******.***  :

Q8G983_A1_6__ser                         HLSP
Q9S1A9_A1_6__ser                         LLAT
                                          *:.
