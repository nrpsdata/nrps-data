CLUSTAL W multiple sequence alignment


NP_216899.1_570_mod1_thr                 VCGPPCQVRVPVPTLLLADVLAAAPAEFVPGPSDPTA-LAYVLFTSGSTGEPKGVEVAHD
YP_116977.1_584_mod1_thr                 LTVPGADLGVPIPTLSI-DAARAYPAPLDAPDIPPTSEIAYVIFTSGSTGVPKGVDVPHS
                                         :  * .:: **:*** : *.  * ** : .    **: :***:******* ****:*.*.

NP_216899.1_570_mod1_thr                 AAMNTVETFIRHFELGAADRWLALATLECDMSVLDIFAALRSGGAIVVVDEAQRRDPDAW
YP_116977.1_584_mod1_thr                 GAMNTIDAVNDWFEVGSADRVLALSALEFDASVYDIFGMFAVGGSIVAVDAERRAEATTW
                                         .****:::.   **:*:*** ***::** * ** ***. :  **:**.**  :* :. :*

NP_216899.1_570_mod1_thr                 ARLIDTYEVTALNFMPGWLDMLLEVGGGRL-SSLRAVAVGGDWVRPDLARRLQVQAPSAR
YP_116977.1_584_mod1_thr                 VDLLRHHRVTILNCVPSMLDMILEIGGDELGDSLRAVTLGGDWVGADLARRLARQVPGCR
                                         . *:  :.** ** :*. ***:**:**..* .*****::***** .******  *.*..*

NP_216899.1_570_mod1_thr                 FAGLGGATETAVHATIFEVQDAANLPPDWASVPYGVPFPNNACRVVADSGDDCPDWVAGE
YP_116977.1_584_mod1_thr                 FSGLGGATETSIHNTICEV--VGEPPAHWATVPFGVPLRNVRCRVVSQAGRDCPDWVPGE
                                         *:********::* ** **  ..: *..**:**:***: *  ****:::* ******.**

NP_216899.1_570_mod1_thr                 LWVSGRGIARGYRGRPELTAERFVEHDGRTWYRTGDLARYWHDGTLEFVGRADHRVKISG
YP_116977.1_584_mod1_thr                 FWVGGANVAAGYRNDPERTAERFVEHDGMRWYKTGDMARYWPDGTIEFLGRADHQVQIRG
                                         :**.* .:* ***. ** **********  **:***:**** ***:**:*****:*:* *

NP_216899.1_570_mod1_thr                 YRVELGEIEAALQRLPGVHAAAATVLPGGSDVLAAAVCVDDAGVTA
YP_116977.1_584_mod1_thr                 YRVELGEVESALRTVPGVRHAVAAVVGAGAPKLVAAVAGERG----
                                         *******:*:**: :***: *.*:*: .*:  *.***. : .    
