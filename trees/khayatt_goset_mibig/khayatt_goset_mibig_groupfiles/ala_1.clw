CLUSTAL W multiple sequence alignment


Q01886v2_A2_ala                          LCSPATSRMGALQNISTQMGTEFKIVELEPEFIRSLPLPPKPNHQPMVGLNDDLYVVFTS
BGC0001166_AGQ43600.1_1838_mod2_ala      LCSPVTRSIGALQNISTEMGAGLKIIRIEPEFIQGLPPVPRLAHQDMAGLDDDLYVVFTS
Q01886v1_A2_ala                          LCSPATSRMGALQNISTQMGTEFKIVELEPEFIRSLPLPPKPNHQPMVGLNDDLYVVFTS
                                         ****.*  :********:**: :**:.:*****:.**  *:  ** *.**:*********

Q01886v2_A2_ala                          GSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASYSFDASIGDIFTTLAVG
BGC0001166_AGQ43600.1_1838_mod2_ala      GSTGDPKGAVATHRAFATGIYEHAMACGMTSVGAPPRSLQFASYSFDASIGDIFTTFVAS
Q01886v1_A2_ala                          GSTGVPKGAVATHQAYATGIYEHAVACGMTSLGAPPRSLQFASYSFDASIGDIFTTLAVG
                                         **** ********:*:********:******:************************:...

Q01886v2_A2_ala                          GCLCIPREEDRNPAGITTFINRYGVTWAGITPSLALHLDPDAVPTLKALCVAGEPLSMSV
BGC0001166_AGQ43600.1_1838_mod2_ala      GCLCIPREEDRNPVGITAFINRCGVTWAGITPSFVSHLEPDAVPTLKALCVAGEPLSMSV
Q01886v1_A2_ala                          GCLCIPREEDRNPAGITTFINRYGVTWAGITPSLALHLDPDAVPTLKALCVAGEPLSMSV
                                         *************.***:**** **********:. **:*********************

Q01886v2_A2_ala                          VTVWSKRLNLINMYGPTEATVACIANQVTCTTTTVSDIGRGYRATTWVVQPDNHNSLVPI
BGC0001166_AGQ43600.1_1838_mod2_ala      VTAWSNRLNLINMYGPTEATVACIANRVLCTTTTVGDIGRGYRAATWVVQPDNHNSLVPI
Q01886v1_A2_ala                          VTVWSKRLNLINMYGPTEATVACIANQVTCTTTTVSDIGRGYRATTWVVQPDNHNSLVPI
                                         **.**:********************:* ******.********:***************

Q01886v2_A2_ala                          GAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPNSTLYKTGDLVRYSADGKII
BGC0001166_AGQ43600.1_1838_mod2_ala      GAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPNSTLYKTGDLVRYSADGKII
Q01886v1_A2_ala                          GAVGELIIEGSILCRGYLNDPERTAEVFIRSPSWLHDLRPNSTLYKTGDLVRYSADGKII
                                         ************************************************************

Q01886v2_A2_ala                          FIGRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLLKRTQSGEPDLLIAFLFVGR
BGC0001166_AGQ43600.1_1838_mod2_ala      FIGRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLLKRTQSGEPDLLIAFLFVGR
Q01886v1_A2_ala                          FIGRKDTQVKMNGQRFELGEVEHALQLQLDPSDGPIIVDLLKRTQSGEPDLLIAFLFVGR
                                         ************************************************************

Q01886v2_A2_ala                          ANTGTGNSDE
BGC0001166_AGQ43600.1_1838_mod2_ala      ANTGTGNSDE
Q01886v1_A2_ala                          ANTGTGNSDE
                                         **********
