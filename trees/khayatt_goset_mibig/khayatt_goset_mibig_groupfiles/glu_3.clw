CLUSTAL W multiple sequence alignment


P27206_A1_glu                            LTESKVAAPEADAELIDLDQAIE-EGAEESLNADVNARNLAYIIYTSGTTGRPKGVMIEH
Q70KJ7_A1_glu                            LTEAGIQAPAADAERIDFDEAVQYETAADGVS--TQSDRLAYIIYTSGTTGRPKGVMIEH
                                         ***: : ** **** **:*:*:: * * :.:.  .:: .*********************

P27206_A1_glu                            RQVHHLVESLQQTIYQSPTQTLPMAFLPPFHFDASVKQIFASLLLGQTLYIVPKKTVTNG
Q70KJ7_A1_glu                            RQVHHLVQSLQQEIYQCGEQTLRMALLAPFHFDASVKQIFASLLLGQTLYIVPKTTVTNG
                                         *******:**** ***.  *** **:*.**************************.*****

P27206_A1_glu                            AALTAYYRKNSIEATDGTPAHLQMLAAAGDFEGLKLKHMLIGGEGLSSVVADKLLKLFKE
Q70KJ7_A1_glu                            SALLDYYRQNRIEATDGTPAHLQMMVAAGDVSGIELRHMLIGGEGLSAAVAEQLMALFHQ
                                         :**  ***:* *************:.****..*::*:**********:.**::*: **::

P27206_A1_glu                            AGTAPRLTNVYGPTETCVDASVHPVIPENAV--QSAYVPIGKALGNNRLYILDQKGRLQP
Q70KJ7_A1_glu                            SGRTPRLTNVYGPTETCVDASVHQVSADNGMNQQAAYVPIGKPLGNARLYILDKHQRLQP
                                         :* :******************* * .:*.:  *:*******.*** ******:: ****

P27206_A1_glu                            EGVAGELYIAGDGVGRGYLHLPELTEEKFLQDPFVPGDRMYRTGDVVRWLPDGTIEYLGR
Q70KJ7_A1_glu                            DGTAGELYIAGDGVGRGYLNLPDLTAEKFLQDPFNGSGRMYRTGDMARWLPDGTIEYIGR
                                         :*.****************:**:** ********  ..*******:.**********:**

P27206_A1_glu                            EDDQVKVRGYRIELGEIEAVIQQAPDVAKAVVLARPDEQGNLEVCAYVVQKPGSEFAP
Q70KJ7_A1_glu                            EDDQVKVRGYRIELGEIETVLRKAPGAAQAVVLARPDQQGSLDVCAYIVQEKGTEFHP
                                         ******************:*:::**..*:********:**.*:****:**: *:** *
