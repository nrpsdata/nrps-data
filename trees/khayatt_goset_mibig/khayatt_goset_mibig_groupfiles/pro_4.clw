CLUSTAL W multiple sequence alignment


O07944_A1_pro                            LTGEDTGQDLSGYDDTDLTDADRTAPLLPAHPAYVIYTSGSTGTPKAVVMPGAALVNLLA
O05647_A1_pro                            LTEDDVDEDLSGIPDGNLTDAERTAPLTPAHPAYVIYTSGSTGRPKAVVMPGAAVVNLLA
                                         ** :*..:****  * :****:***** *************** **********:*****

O07944_A1_pro                            WHRREIPGEAGAPVAQFTTIGFDVAAQEILATWLHGKTLAVPSQEVRRSAEQLAAWLDEQ
O05647_A1_pro                            WHRREIPAGAGTTVAQFASLSFDVAAQEILSTLLYGATLAVPTDAVRRDADAFAAWLEEY
                                         *******. **:.****:::.*********:* *:* *****:: ***.*: :****:* 

O07944_A1_pro                            HVSELYAPNLVIEALAEAAAEAGRTLPALRHIAQAGEALTLTRTVREFAAAVPGRQLHNH
O05647_A1_pro                            RVNELYAPNLVVEALAEAAAEQGRTLPDLRHIAQAGEALTAGPRVRDFCAALPGRRLHNH
                                         :*.********:********* ***** ************    **:*.**:***:****

O07944_A1_pro                            YGPAETHVMTGTALPEDPAAWSEHAPLGRPVSGARVYVLDSALRPVAPGVTGELYLAGAG
O05647_A1_pro                            YGPAETHVMTGIELPVDPGGWPERVPIGGPVDNARLYVLDGFLRPVPPGVVGELYLAGAG
                                         ***********  ** **..*.*:.*:* **..**:****. ****.***.*********

O07944_A1_pro                            VSRGYLNRPVLTAERFVADPYAPSPGARMYRTGDLGRWNTRGELEFAGRADHQVKIRGFR
O05647_A1_pro                            VARGYLNRPGLTAERFVADPFG-GPGTRMYRTGDLARWAGSGVLEFAGRADHQVKVRGFR
                                         *:******* **********:. .**:********.**   * ************:****

O07944_A1_pro                            IEPGEIEAALTDLPAVARAAVVVREDRPGDKRLVAY--AVPAGEGLDAAA
O05647_A1_pro                            IEPGEVESVLAAQPGVARAVVLAREDRPGERRLVAYLVAVP-GSVPDPGV
                                         *****:*:.*:  *.****.*:.******::*****  *** *.  *...
