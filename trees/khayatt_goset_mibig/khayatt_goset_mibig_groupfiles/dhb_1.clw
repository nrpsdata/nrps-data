CLUSTAL W multiple sequence alignment


Q9FDB2_A_dhb                             LAQELLACCPTLQTVIVRGQTRVTDPKFIELASCYSASSCQANADPNQIAFFQLSGGTTD
Q9F638/11127897_A_dhb                    LAEKVRSAVPGLRHVVVVGEA----GPFTSLSQLYASPVDLPGPIPSDVAFFQLSGGSTG
                                         **::: :. * *: *:* *::      * .*:. *::.   ... *.::********:*.

Q9FDB2_A_dhb                             TPKLIPRTHNDYAYSVTASVEICRFDQHTRYLCVLPAAHNFPLSSPGALGVFWAGGCVVL
Q9F638/11127897_A_dhb                    VPKLIPRTHDDYIYSLRGSVEICQLDETTVYLCALPAAHNFPLSSPGVLGTLYAGGTAVM
                                         .********:** **: .*****::*: * ***.*************.**.::*** .*:

Q9FDB2_A_dhb                             SQDASPQHAFKLIEQHKITVTALVPPLALLWMDHAEKSTYDLSSLHFVQVGGAKFSEAAA
Q9F638/11127897_A_dhb                    ALHPSPDQAFPLIERERITFTALVPPLAMIWMDAAKARRHDLSSLKVLQVGGARLSTEAA
                                         : ..**::** ***:.:**.********::*** *:   :*****:.:*****::*  **

Q9FDB2_A_dhb                             RRLPKALGCQLQQVFGMAEGLVNYTRLDDSAELIATTQGRPISAHDQLLVVDEQGQPVAS
Q9F638/11127897_A_dhb                    QRVRPTLGCALQQVYGMAEGLVNYTRLDDPDELVIATQGRPISPDDEIRIIDEDGRDVAP
                                         :*:  :*** ****:**************. **: :*******..*:: ::**:*: **.

Q9FDB2_A_dhb                             GEEGYLLTQGPYTIRGYYRADQHNQRAFNAQGFYITGDKVKLSSEGYVIVTGRAKDQINR
Q9F638/11127897_A_dhb                    GETGQLLTRGPYTIRGYYNAEAHNARAFTSDGFYCTGDLVRVTPEGYLVVEGRAKDQINR
                                         ** * ***:*********.*: ** ***.::*** *** *:::.***::* *********

Q9FDB2_A_dhb                             GGEKIAAEEVENQLLHHPAVHDAALIAISDEYLGERSCAVIVLKPEQSVNTI--
Q9F638/11127897_A_dhb                    GGDKIAAEEVENHLLAHPSVHDAAVISIPDPFLGERTCAFVI--PREAPPTAAT
                                         **:*********:** **:*****:*:*.* :****:**.::  *.::  *   
