CLUSTAL W multiple sequence alignment


O05819_A1_lys                            FAASVG--ADILEEDRAI---------TVPV-----DQAAYVIFTSGTTGTPKGVIGTHR
NP_216895.1_486_mod1_lys                 FAASVG--ADILEEDRAI---------TVPV-----DQAAYVIFTSGTTGTPKGVIGTHR
YP_116976.1_488_mod1_lys                 TAASAGDVPTLVLDDPAVRQRIAAREPVAPVVARHPEHCAYIIFTSGSTGEPKGVADTNA
                                          ***.*  . :: :* *:         ..**     ::.**:*****:** **** .*: 

O05819_A1_lys                            ALSAYADDHIERVLRPAAQRLGRPLRIAHAWSFTFDAAWQPLVALLDGHAVHIVDDHRQR
NP_216895.1_486_mod1_lys                 ALSAYADDHIERVLRPAAQRLGRPLRIAHAWSFTFDAAWQPLVALLDGHAVHIVDDHRQR
YP_116976.1_488_mod1_lys                 AVAAYFADHRARCYRPATARLGRPLRIAHAWSLSFDASWQPMVGLLDGQALHLFDAEEMR
                                         *::**  **  *  ***: *************::***:***:*.****:*:*:.* .. *

O05819_A1_lys                            DAGALVEAIDRFGLDMIDTTPSMFAQLHNAGLLDRAPLAVLALGGEALGAATWRMIQQNC
NP_216895.1_486_mod1_lys                 DAGALVEAIDRFGLDMIDTTPSMFAQLHNAGLLDRAPLAVLALGGEALGAATWRMIQQNC
YP_116976.1_488_mod1_lys                 DAGRIVAGMAEFGVDMIDTTPSMLAQLDAAGLLERR-LPVLALGGEAIDTALWNRLR---
                                         *** :* .: .**:*********:***. ****:*  *.********:.:* *. ::   

O05819_A1_lys                            ARTAMTAFNCYGPTETTVEAVVAAVAEHARPVIGRPTCTTRAYVMDSWLRPVPDGVAGEL
NP_216895.1_486_mod1_lys                 ARTAMTAFNCYGPTETTVEAVVAAVAEHARPVIGRPTCTTRAYVMDSWLRPVPDGVAGEL
YP_116976.1_488_mod1_lys                 ALPDTAVYNCYGPTETTVEAVVAPVGRYETPTIGTPNAGMAGYVLDSMLRPVPRGAVGEL
                                         * .  :.:***************.*..:  *.** *..   .**:** ***** *..***

O05819_A1_lys                            YLAGAQLTRGYLGRPAETAARFVAEPNGRGSRMYRTGDVVRRLPDGGLEFLGRSDDQVKI
NP_216895.1_486_mod1_lys                 YLAGAQLTRGYLGRPAETAARFVAEPNGRGSRMYRTGDVVRRLPDGGLEFLGRSDDQVKI
YP_116976.1_488_mod1_lys                 YLAGPQLARGYVGKPGVTADRFVADPLRPGARMYRTGDLVRRLPHGGFAYLGRADDQVKI
                                         ****.**:***:*:*. ** ****:*   *:*******:*****.**: :***:******

O05819_A1_lys                            RGFRVEPGEIAAVLNGHHAVHGCHVTARGHASGPRLTAYVAG-----GPQPP
NP_216895.1_486_mod1_lys                 RGFRVEPGEIAAVLNGHHAVHGCHVTARGHASGPRLTAYVAG-----GPQPP
YP_116976.1_488_mod1_lys                 RGYRIEIGEIETALRRLPGVRTAAVTVVRRAGGASLVGFVVGDTASTGEAPR
                                         **:*:* *** :.*.   .*: . **.  :*.*. *..:*.*     *  * 
