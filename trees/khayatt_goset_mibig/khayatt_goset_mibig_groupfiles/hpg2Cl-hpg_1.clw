CLUSTAL W multiple sequence alignment


Q93N88_A1_3__hpg2Cl|hpg                  MVEDSAPVLTIDDPSVVTAEGEPEVVETAGGDIAYVMYTSGSTGTPKGVAVPHASVAALV
Q93N87_A2_5__hpg2Cl|hpg                  VLEDSSPAVTID--AVVKGGGEAPSVAVTEGDLAYVMYTSGSTGTPKGVAVPHGSVAALV
                                         ::***:*.:***  :**.. **.  * .: **:********************.******

Q93N88_A1_3__hpg2Cl|hpg                  GEPGWGVGPGDAVLFHAPHAFDISLFEVWVPLASGGRIVVAEPSMAVDGAAVRRHIADGV
Q93N87_A2_5__hpg2Cl|hpg                  GETGWGLGPGDSVLFHAPHAFDISLFEVWVPLASGARVVIAEPGVAVDAAAVRRHIAAGV
                                         **.***:****:***********************.*:*:***.:***.******** **

Q93N88_A1_3__hpg2Cl|hpg                  THVHVTAGLFRVLAEEASDCFDGVHEVLTGGDVVPLEAVERVRAACPDVRVRHLYGPTEV
Q93N87_A2_5__hpg2Cl|hpg                  THVHVTAGLFRVLADEEPECFNGAHEVLTGGDVVPLEAVARVRAACPEVRVRHLYGPTEV
                                         **************:* .:**:*.*************** *******:************

Q93N88_A1_3__hpg2Cl|hpg                  SLCATWHLFEPGEEQGEVLPLGRPLNNRQVYVLDPFLQPVPPGVTGELYVAGAGLARGYL
Q93N87_A2_5__hpg2Cl|hpg                  SLCATWHLLEPGEGTDRVLPVGSPLANRQVYVLDAFLQPVPPGVAGELYIAGAGLARGYL
                                         ********:****  ..***:* ** ********.*********:****:**********

Q93N88_A1_3__hpg2Cl|hpg                  GRAGLSAERFVASPFADGERMYRTGDLVRWTTGVELVFVGRADAQVKIRGFRVELGEVEA
Q93N87_A2_5__hpg2Cl|hpg                  KRAGLSAERFVACPFADGERMYRTGDLARWTDDGELAFAGRADAQVKIRGFRVELGEVEA
                                          ***********.**************.*** . **.*.*********************

Q93N88_A1_3__hpg2Cl|hpg                  ALAAQPAVAQAVVVAREDRPGEKRLVGYLVPSGEEPDTEA
Q93N87_A2_5__hpg2Cl|hpg                  ALAAQPAVAQAVVVAREGRPGEKRLVGYLVPHGGQPDSDV
                                         *****************.************* * :**::.
