CLUSTAL W multiple sequence alignment


Q01886_A3_ala                            LTLPESANALATLSGLTKVIPVSLSELVQQITDNTTKKDEYCKSGDTDPSSPAYLLYTSG
Q01886/62867029_A3_ala                   LTLPESANALATLSGLTKVIPVSLSELVQQITDNTTKKDEYCKSGDTDPSSPAYLLYTSG
                                         ************************************************************

Q01886_A3_ala                            TSGKPKGVVMEHRAWSLGFTCHAEYMGFNSCTRILQFSSLMFDLSILEIWAVLYAGGCLF
Q01886/62867029_A3_ala                   TSGKPKGVVMEHRAWSLGFTCHAEYMGFNSCTRILQFSSLMFDLSILEIWAVLYAGGCLF
                                         ************************************************************

Q01886_A3_ala                            IPSDKERVNNLQDFTRINDINTVFLTPSIGKLLNPKDLPNISFAGFIGEPMTRSLIDAWT
Q01886/62867029_A3_ala                   IPSDKERVNNLQDFTRINDINTVFLTPSIGKLLNPKDLPNISFAGFIGEPMTRSLIDAWT
                                         ************************************************************

Q01886_A3_ala                            LPGRRLVNSYGPTEACVLVTAREISPTAPHDKPSSNIGHALGANIWVVEPQRTALVPIGA
Q01886/62867029_A3_ala                   LPGRRLVNSYGPTEACVLVTAREISPTAPHDKPSSNIGHALGANIWVVEPQRTALVPIGA
                                         ************************************************************

Q01886_A3_ala                            VGELCIEAPSLARCYLANPERTEYSFPSTVLDNWQTKKGTRVYRTGDLVRYASDGTLDFL
Q01886/62867029_A3_ala                   VGELCIEAPSLARCYLANPERTEYSFPSTVLDNWQTKKGTRVYRTGDLVRYASDGTLDFL
                                         ************************************************************

Q01886_A3_ala                            GRKDGQIKLRGQRIELGEIEHHIRRLMSDDPRFHEASVQLYNPATDPDRDATVDVQ
Q01886/62867029_A3_ala                   GRKDGQIKLRGQRIELGEIEHHIRRLMSDDPRFHEASVQLYNPATDPDRDATVDVQ
                                         ********************************************************
