CLUSTAL W multiple sequence alignment


Q93N87_A1_4__hpg|hpg2Cl                  VCSERTRVVVPEDVPAPLVPLDEASILDGEPLSLPVAGGDIAYVMYTSGSTGVPKGVAVP
Q93N87_A1_hpg|hpg2Cl                     VCSERTRVVVPEDVPAPLVPLDEASILDGEPLSLPVAGGDIAYVMYTSGSTGVPKGVAVP
                                         ************************************************************

Q93N87_A1_4__hpg|hpg2Cl                  HGSVAALVGEPGWAVGPGDSVLFHAPHAFDISLFEVWVPLASGARVEVAEPSVVVDAAAV
Q93N87_A1_hpg|hpg2Cl                     HGSVAALVGEPGWAVGPGDSVLFHAPHAFDISLFEVWVPLASGARVEVAEPSVVVDAAAV
                                         ************************************************************

Q93N87_A1_4__hpg|hpg2Cl                  REYIAGGVTHVHVTAGLFRVLAEESPECFTGAREVLTGGDVVPARAVQRVRSACPEVRVR
Q93N87_A1_hpg|hpg2Cl                     REYIAGGVTHVHVTAGLFRVLAEESPECFTGAREVLTGGDVVPARAVQRVRSACPEVRVR
                                         ************************************************************

Q93N87_A1_4__hpg|hpg2Cl                  HLYGPTEVSLCATWHLLEPGEESPQVLPVGRPLGNRQVYVLDAYLHPVAPGVTGELYIAG
Q93N87_A1_hpg|hpg2Cl                     HLYGPTEVSLCATWHLLEPGEESPQVLPVGRPLGNRQVYVLDAYLHPVAPGVTGELYIAG
                                         ************************************************************

Q93N87_A1_4__hpg|hpg2Cl                  AGLARGYLKRAGLSSERFVACPFADGERMYRTGDLVRWTSDGELVFVGRADAQVKVRGFR
Q93N87_A1_hpg|hpg2Cl                     AGLARGYLKRAGLSSERFVACPFADGERMYRTGDLVRWTSDGELVFVGRADAQVKVRGFR
                                         ************************************************************

Q93N87_A1_4__hpg|hpg2Cl                  VELGEVEAALAAEAGVGHAVVTAREDRPGERRLIGYVLPDGEDVDTEL
Q93N87_A1_hpg|hpg2Cl                     VELGEVEAALAAEAGVGHAVVTAREDRPGERRLIGYVLPDGEDVDTEL
                                         ************************************************
