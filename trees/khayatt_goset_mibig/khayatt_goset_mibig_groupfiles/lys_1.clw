CLUSTAL W multiple sequence alignment


AGM16413.1_1312_mod2_lys                 LAQRHLQARAAFSGRRITLDEEAFYDEDGSNLERVNQPEHLSYVIYTSGTTGKPKGVMIE
AGM16414.1_1316_mod2_lys                 LAQRHLQARAAFSGRRIMLDEEAFYGADGSNLERVNQPEHLSYVIYTSGTTGKPKGVMIE
                                         ***************** *******. *********************************

AGM16413.1_1312_mod2_lys                 HRQMAVLSAAWESEYGLREESMRWMQWASFSFDVFSGDLIRALLHGGELILCPEESRANP
AGM16414.1_1316_mod2_lys                 HRQMAVLSAAWEREYGLQEESMRWMQWASFSFDVFSGDLIRALLHGGELILCPEDARANP
                                         ************ ****:************************************::****

AGM16413.1_1312_mod2_lys                 AEIYELIRKHRIQMFDVTPSLAIPLMEYVYENKLDISSMKLAVVGADHCPKEEFQKLLER
AGM16414.1_1316_mod2_lys                 AEIYELIRKHRIQMFDVTPSLVIPLMEYVYENKLDISSMKLAVVGADHCPKEEFQKLLER
                                         *********************.**************************************

AGM16413.1_1312_mod2_lys                 FGSQMRIVNSYGVTETTIDSCYFEQASTEGLRTVPIGKPLPGVTMYILDDHHSLLPVGIT
AGM16414.1_1316_mod2_lys                 FGSQMRIVNSYGVTETTIDSCYFEQASTEGLRTVPIGKPLPGVTMYILDDQHSLLPVGIT
                                         **************************************************:*********

AGM16413.1_1312_mod2_lys                 GELYIGGPCVGRGYWKRPDLTAEKFVDNPFAPGERMYRTGDLARWLPDGNVEYLGRIDHQ
AGM16414.1_1316_mod2_lys                 GELYIGGPCVGRGYWKRPDLTAEKFVDNPFAPGERMYRTGDLARWLPDGNVEYLGRIDHQ
                                         ************************************************************

AGM16413.1_1312_mod2_lys                 VKIRGYRIEIGEVESQLLKTPFIREAVVVAREDAGGQKSLCAYFVAERELTVSE
AGM16414.1_1316_mod2_lys                 VKIRGYRIEIGEVETQLLRTPFIREAVVVAREDVSGQKSLCAYFVAERELTVSE
                                         **************:***:**************..*******************
