CLUSTAL W multiple sequence alignment


AGZ15459.1_694_mod1_pip                  LTDRGTASTLPECRSPVLHLD-DAELDGPAVDMPDVG----LRPDHLAYVMFTSGSTGTP
AGZ15458.1_478_mod1_pip                  VTDTATWRDMPDNDIEIVDLDRRAEWASPEGAPDDAGRVSPLAPDNLAYVMYTSGSTGTP
                                         :** .*   :*:    ::.**  **  .*     *.*    * **:*****:********

AGZ15459.1_694_mod1_pip                  KGVAITHAAVVNGVLDLRRVVQVEAGSRMLAATSVNFDVSVFEIFTALTSGASVEIVRDV
AGZ15458.1_478_mod1_pip                  KGAAITHRSVVNGVRELVRVLDAPPGWRMLAGTSVNFDVSVFELLSALSTGGTAEVVANA
                                         **.**** :***** :* **::. .* ****.***********:::**::*.:.*:* :.

AGZ15459.1_694_mod1_pip                  LELAERDRWSGSTISAVPTVFSALLDDIAGKGPGALELDVRTVILAGEALSADLVHKVRE
AGZ15458.1_478_mod1_pip                  LVLGERDGWAGQVISGVPSVLGELVEHL-GATSG-----VRTVVFAGDVLPARLVRQVRA
                                         * *.*** *:*..**.**:*:. *::.: *  .*     ****::**:.*.* **::** 

AGZ15459.1_694_mod1_pip                  VLPEVRVVNAYGQTESFYAATFTL--PQRWTGAGPIPIGRPLANMRAHVLGPELAPVAPG
AGZ15458.1_478_mod1_pip                  ALPDARIVNSYGQSESFYATTFSLAASEEWAAGEVAPIGTPLGNMRAYVLGPGLAPVPQG
                                         .**:.*:**:***:*****:**:*  .:.*:..   *** **.****:**** ****. *

AGZ15459.1_694_mod1_pip                  VVGELYVAG-LVARGYHRAAGATAERFVACPFGPAGARMYRTGDLARWDEDGHLRYAGRA
AGZ15458.1_478_mod1_pip                  VVGELYVVGTCLGRGYHGRPGMTAERFVADPFGPAGERMYRTGDLARWNPHGELECLGRG
                                         *******.*  :.****  .* ******* ****** ***********: .*.*.  **.

AGZ15459.1_694_mod1_pip                  DGQTKINGIRVDPTEIETVLGRHAAVGQAAVTVREDRTGSRRLVGYVAPAAA------GA
AGZ15458.1_478_mod1_pip                  DGQVKVRGFRIETAEVEAALARHPGISEVVVVGREVPSGGRRLVAYVVHTGEGGVGDDGA
                                         ***.*:.*:*::.:*:*:.*.**..:.:..*. **  :*.****.**. :.       **

AGZ15459.1_694_mod1_pip                  GG-GDLDADA
AGZ15458.1_478_mod1_pip                  GGIGDVDVQS
                                         ** **:*.::
