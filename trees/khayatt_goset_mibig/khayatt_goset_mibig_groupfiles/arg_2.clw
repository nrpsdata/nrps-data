CLUSTAL W multiple sequence alignment


BGC0001168_509516289_4_mod1_arg          VTHSDLE------HRFSGFEVRTVSVDGSSAMTHDSRSASRPPEVAVGGSDTAYIVYTSG
Q6VT93_A1_16__arg                        --LSELELCNSFLSDLGTIECVCLAID-APGWEPEEGELPVPP--VIEGRQPAYVIYTSG
                                            *:**        :. :*   :::* :..   :. . . **  .: * :.**::****

BGC0001168_509516289_4_mod1_arg          SSGTPKGVQVSHSSLNYLCRHINEVYGIGPDDRVLQYAALSFDTSIEQILVALLNGATLV
Q6VT93_A1_16__arg                        STGQPKGVIISHDSISHHCQVIRDYYRITAQDVILQFAPMNVDAALEQLLPGLISGATVV
                                         *:* **** :**.*:.: *: *.: * * .:* :**:*.:..*:::**:* .*:.***:*

BGC0001168_509516289_4_mod1_arg          L-PEELWAPSELSARIASLGISVMDLTPPYWRAFLSELE-----HSPTELPIRLTIVGGS
Q6VT93_A1_16__arg                        IRSEPLWSPDILCRKVVELGISVLDLPPSYLYELLLEIRDVAGWSRPPSL--RLVISGGE
                                         : .* **:*. *. ::..*****:**.*.*   :* *:.       *..*  **.* **.

BGC0001168_509516289_4_mod1_arg          AVHAAD----CRTALRLMPYSRLVNAYGLTETTITSCTMEV-----TPELLPSEGAAPIG
Q6VT93_A1_16__arg                        ALSPETLSLWCGCAL---SECRLVNAYGPTETTITSTVYEIESRARTFTRLPE--SVPIG
                                         *: .      *  **   . .******* ******* . *:     *   **.  :.***

BGC0001168_509516289_4_mod1_arg          RPLPGTTVLILDQDMRPVPPQQVGELYIGGPGVARGYLAEEASNKDRFVSLATDVGGATR
Q6VT93_A1_16__arg                        RPLPGESAYILDTQRRPLPVGVPGELYIGGAGVAIGYLNRPELTASTFVENPFMAG--TR
                                         ***** :. *** : **:*    *******.*** *** .   . . **. .  .*  **

BGC0001168_509516289_4_mod1_arg          FYRTGDLGRWTAEGNLHITGRADRQVKVRGYRVEPAEIEATLSAHPLIDDVAVKPYSVRD
Q6VT93_A1_16__arg                        LYKTGDAARWLADGNIALLGRLDQQVKIRGFRVECGEIEAALQALDVVKHVAVLAQPTQG
                                         :*:*** .** *:**: : ** *:***:**:*** .****:*.*  ::..*** . ..:.

BGC0001168_509516289_4_mod1_arg          ELQLAAYYTASR--
Q6VT93_A1_16__arg                        SHRLVAFLELVQPA
                                         . :*.*:    :  
