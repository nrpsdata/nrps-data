CLUSTAL W multiple sequence alignment


AEU11006.1_2653_mod1_gln                 LTQQRLVESLGKHQARVVHLDSD----WLSISEFSQGNPIAQVQASNLAYVIYTSGSTGQ
Q5DIS9_A1_5__gln                         LTQTFLQDEL-PFNAQLTNLCLDDPSLFQNVPAQWDGNPEQRACPEHLAYVIYTSGSTGR
                                         ***  * :.*  .:*::.:*  *    : .:.   :***  :. ..:************:

AEU11006.1_2653_mod1_gln                 PKGVMLSHSNLCNHTFWMQATFPITAKDKVLQKTPFGFDASVWEFYAPLLAGGQLLMAQP
Q5DIS9_A1_5__gln                         PKGVGITHGALVNHMRWMQERFQLAAHERVLQRTSSSFDASVWEFWLPLMSGARLHLAPA
                                         **** ::*. * **  ***  * ::*:::***:*. .********: **::*.:* :* .

AEU11006.1_2653_mod1_gln                 GGHTDSAYLLRVIAQEQVTTVQLVPSLLQMLLEQGGIETCHSLKHVFCGGEALPVTLQEG
Q5DIS9_A1_5__gln                         ELGTSLESLWGLVEAQRINVLQMPPSLLQALLPFAGDDQLDSLRLLCCGGEALSGALLEQ
                                            *.   *  ::  :::..:*: ***** **  .* :  .**: : ******. :* * 

AEU11006.1_2653_mod1_gln                 LLSKLNVKLHNLYGPTEACIDATFWNCQQHKYVQVVPIGRPISNTQVYILDQYLQPVPIG
Q5DIS9_A1_5__gln                         LGRRWNGELVNLYGPTEATIDACCFSAPVKEVGAEIPIGAPIAGVRARILDAAGGVCPVG
                                         *  : * :* ******** ***  :..  ::    :*** **:..:. ***      *:*

AEU11006.1_2653_mod1_gln                 VPGELHIGGAGLARGYLNRPELTQEKFIPNPLSGSKGERLYKTGDLARYLPDGNIEYLGR
Q5DIS9_A1_5__gln                         CRGELLIAGAGLARGYLGRPGLTAERFVPDPY--GDGERIYRTGDLARLRRDGQIDYLGR
                                           *** *.*********.** ** *:*:*:*   ..***:*:******   **:*:****

AEU11006.1_2653_mod1_gln                 IDNQVKIRGFRIELGEIEAVLRQHEGVQTSCVIAREDIPGNKRLVAYVVPQPQLTPTVSE
Q5DIS9_A1_5__gln                         LDHQVKIRGFRIELGEIEARLLEQECVREAVVLAADGASG-QQLLGYVVPQ-DVGALEDE
                                         :*:**************** * ::* *: : *:* :. .* ::*:.***** :: .  .*

AEU11006.1_2653_mod1_gln                 -
Q5DIS9_A1_5__gln                         K
                                          
