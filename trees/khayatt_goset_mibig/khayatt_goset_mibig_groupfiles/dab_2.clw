CLUSTAL W multiple sequence alignment


O85168_A4_dab                            LTLPGLQDRLPALSMPLVLLDDEQYQGLAECDDNPVVPTLGVRNLAYVIYTSGSTGNPKG
Q83VS0_A11_21__dab                       LTQRDLQKRLPTLTLPLVLLDDDQRNTFTERDDNPVVEALGVRNLAYVIYTSGSTGNPKG
O85168_A3_dab                            MTQRHLQEYLPTLTLPLVLLDDDQRKTFTERDDNPVVEALGVRNLAYVIYTSGSTGNPKG
Q83VS0_A10_20__dab                       LTQRGLQERLPALSMPLVLLDDEHCQGFTECDDNPVVPTLGVRNLAYVIYTSGSTGNPKG
                                         :*   **. **:*::*******:: : ::* ****** :*********************

O85168_A4_dab                            VMIEHRGLVNYSVDAARLFDLSQSDTVLQQNTLNFDLSVEEIFPALLAGATLAPSREIFG
Q83VS0_A11_21__dab                       VMIEHRGLVNYSVDAARLFALSQSDTVLQQNTLNFDLSVEEIFPALLAGATLAPSREIFG
O85168_A3_dab                            VMIEHRGLVNYSVDAARLFDLSPTDTVLQQNTLNFDLSVEEIFPALLAGATLTPSREIFG
Q83VS0_A10_20__dab                       VMIEHRGLVNYSVDAARLFGLSQSDTVLQQNTLNFDLSVEEIFPALLAGATLAPSREIFG
                                         ******************* ** :****************************:*******

O85168_A4_dab                            SEGTETHGIQPTVLHLTTAHWHTLVAEWHNQPQAAEQRLQHVRLINVTGDALSAQKLKLW
Q83VS0_A11_21__dab                       SEGTENHGIYPTVLHLTAAHWHTLVAEWHNQPQAAAQRLAEVRLINVTGDALSAQKLKLW
O85168_A3_dab                            SEGTENHGINPTVLHLTAAHWHTLVAEWHKQPQVAEQRLQHVRLINVTGDALSAQKLKLW
Q83VS0_A10_20__dab                       SEGTENHGIYPTVLHLTAAHWHTLVAEWHNQPQAAEQRLAEVRLINVTGDALSAQKLKLW
                                         *****.*** *******:***********:***.* *** .*******************

O85168_A4_dab                            DEVRPAHTLLINTYGPTEATVSCTAAYVSYDAAAGSEGSGNATIGKPMANTRIYLLDAHQ
Q83VS0_A11_21__dab                       DEVRPAHTRLINTYGPTEATVSCTAAYVSHDAIAGSEGSGNATIGKPMANTRIYLLDAHQ
O85168_A3_dab                            DEVRPAHTRLINTYGPTEATVSCTAAYVSHDAAAGSEGSGNATIGKPMANTRIYLLDAHQ
Q83VS0_A10_20__dab                       DEVRPAHTRLINTYGPTEATVSCTAAYVSHDAAAGSEGSGNATIGKPMANTRIYLLDAHQ
                                         ******** ********************:** ***************************

O85168_A4_dab                            QPVPYGVAGEIYIGGDGVARGYLNLEEVNAERFLADPFSESPDARMYKTGDLARYMADGR
Q83VS0_A11_21__dab                       QPVPYGVAGEIFIGGDGVARGYLNLEEVNAERFLADPFSNSPEARMYKTGDLARYMADGR
O85168_A3_dab                            QPVPYGVAGEIYIGGDGVARGYLNLEEVNAERFLADPFSESPDARMYKTGDLARYMADGR
Q83VS0_A10_20__dab                       QPVPYGVAGEIFIGGDGVARGYLNLEEVNAERFLADPFSNSPDARMYKTGDLARYMADGR
                                         ***********:***************************:**:*****************

O85168_A4_dab                            IEYLGRNDFQVKVRGFRIELGEIEARLGNCKGVKEAVVIAREDNPGEKRFVAYVVAQPQT
Q83VS0_A11_21__dab                       IEYLGRNDFQVKVRGFRIELGEIEARFGNCAGVKEAVVIAREDTPGDKRLVAYVVGQPQA
O85168_A3_dab                            IEYLGRNDFQVKVRGFRIELGEIEARLGNCTGVKEAVVIAREDNPGDKRLVAYVVAQPQS
Q83VS0_A10_20__dab                       IEYLGRNDFQVKVRGFRIELGEIEARLGNCKGVKEAVVIAREDNPGEKRLVAYVIAQPQA
                                         **************************:*** ************.**:**:****:.***:

O85168_A4_dab                            QITAAE
Q83VS0_A11_21__dab                       SLDAAS
O85168_A3_dab                            QLTAAD
Q83VS0_A10_20__dab                       NLDAAS
                                         .: **.
