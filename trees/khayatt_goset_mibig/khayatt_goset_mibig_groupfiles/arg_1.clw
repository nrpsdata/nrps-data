CLUSTAL W multiple sequence alignment


Q8RTG4_A1_arg                            LTQESLGDFLPQTGAELLCLDRDWEKIATYSPENPFNLTTP---ENLAYVIYTSGSTGKP
Q9S1A7_A1_10__arg                        LTQQSLVQFLPENQAEILCLDTDWSRIANYSQE---NLTSPVKPENLAYVIYTSGSTGKP
Q9RNA9_A1_arg                            LTQQSLVQFLPENQAEILCLDTDWSRIANYSQE---NLTSPVKTENLAYVIYTSGSTGKP
                                         ***:** :***:. **:**** **.:**.** *   ***:*   ****************

Q8RTG4_A1_arg                            KGVMNIHRGICNTLTYAIGHYNINSEDRILQITSLSFDVSVWEVFLSLISGASLVVAKPD
Q9S1A7_A1_10__arg                        KGVMNIHQGICNTLKYNIDNYNLNSEERILQITPFSFDVSVWEIFLSLTSGATLVVAKPD
Q9RNA9_A1_arg                            KGVMNIHQGICNTLKYNIDNYNLNSEDRILQITPFSFDVSVWEVFSSLTSGATLVVTKPD
                                         *******:******.* *.:**:***:******.:********:* ** ***:***:***

Q8RTG4_A1_arg                            GYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHPKSKDCHCLKRVIVGGEALSYELNQRF
Q9S1A7_A1_10__arg                        GYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHSKSKDCHCLKRVIVGGEALSYELNQRF
Q9RNA9_A1_arg                            GYKDIDYLIDLIVQEQVTCFTCVPSILRVFLQHPKSKDCHCLKRVIVGGEALSYELNQRF
                                         *********************************.**************************

Q8RTG4_A1_arg                            FQQLNCELYNAYGPTEVAVETTIWCCQPNSQ-ISIGTPIANAQVYILDSYLQPVPIGVAG
Q9S1A7_A1_10__arg                        FQQLNCELYNAYGPTEVAVETTIWCCQPNSQ-ISIGTPIANAQVYILDSYLQPVPIGVAG
Q9RNA9_A1_arg                            FQQLNCELYNAYGPTEVAVDATVWCCQPNSQLISIGRPIANVQVYILDSYLQPVPIGVAG
                                         *******************::*:******** **** ****.******************

Q8RTG4_A1_arg                            ELHIGGMGLARGYLNQPELTAEKFIPHPFAQGKLYKTGDLARYLPEGNIEYLGRIDNQVK
Q9S1A7_A1_10__arg                        ELHIGGMGLARGYLNQPELTAEKFIPHPFAQGKLYKTGDLARYLPDGNIEYLGRIDNQVK
Q9RNA9_A1_arg                            ELHIGGMGLARGYLNQAELTAEKFIPHPFAEGKLYKTGDLARYLPDGNIEYLGRIDNQVK
                                         ****************.*************:**************:**************

Q8RTG4_A1_arg                            LRGLRIELGEIQTVLETHPNVEQTVVIPRGSA------------GNSLLSPQD
Q9S1A7_A1_10__arg                        LRGLRIELGEIQTVLETHPNVEQTVVIMREDTLYNQRLVAYVIRKDTLLTSQD
Q9RNA9_A1_arg                            LRGFRIELGEIQTVLETHPNVEQTVVIMREDTLYNQRLVAYVMRKEPLLTSQD
                                         ***:*********************** * .:             :.**:.**
