CLUSTAL W multiple sequence alignment


Q70AZ7_A2_hpg|hpg2Cl                     VCVQATSGAVPDGLAPVVMDSPAIAAAPSEAPPITVGAHDLAYVMYTSGSTGVPKGVAVP
Q70AZ7_A2_5__hpg|hpg2Cl                  VCVQATSGAVPDGLAPVVMDSPAIAAAPSEAPPITVGAHDLAYVMYTSGSTGVPKGVAVP
                                         ************************************************************

Q70AZ7_A2_hpg|hpg2Cl                     HGSVAALAGDPGWSVGPGDGVLMHAPHAFDASLLEIWVPLLSGAHVVVADPGAVDAQRLR
Q70AZ7_A2_5__hpg|hpg2Cl                  HGSVAALAGDPGWSVGPGDGVLMHAPHAFDASLLEIWVPLLSGAHVVVADPGAVDAQRLR
                                         ************************************************************

Q70AZ7_A2_hpg|hpg2Cl                     EAIDRGVTTVHLTAGSFRVLAEESPDAFRGLREVLTGGDAVPLASVVRLRETCPEIRVRH
Q70AZ7_A2_5__hpg|hpg2Cl                  EAIDRGVTTVHLTAGSFRVLAEESPDAFRGLREVLTGGDAVPLASVVRLRETCPEIRVRH
                                         ************************************************************

Q70AZ7_A2_hpg|hpg2Cl                     LYGPTETTLCATWHLIEPGVATGDTLPIGRPLAGRRAYVLDAFLQPVAPNVTGELYLAGA
Q70AZ7_A2_5__hpg|hpg2Cl                  LYGPTETTLCATWHLIEPGVATGDTLPIGRPLAGRRAYVLDAFLQPVAPNVTGELYLAGA
                                         ************************************************************

Q70AZ7_A2_hpg|hpg2Cl                     GLARGYLGAAAATAERFVADPFAAGERMYRTGDLARWTEQGELLFAGRADAQVKIRGYRV
Q70AZ7_A2_5__hpg|hpg2Cl                  GLARGYLGAAAATAERFVADPFAAGERMYRTGDLARWTEQGELLFAGRADAQVKIRGYRV
                                         ************************************************************

Q70AZ7_A2_hpg|hpg2Cl                     EPAEIEAALTAIPEVAQAVVVAREDGPGEKRLIAYVTAAGQPGPDPAA
Q70AZ7_A2_5__hpg|hpg2Cl                  EPAEIEAALTAIPEVAQAVVVAREDGPGEKRLIAYVTAAGQPGPDPAA
                                         ************************************************
