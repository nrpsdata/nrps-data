CLUSTAL W multiple sequence alignment


Q939Z1_A3_asn                            AVLSASPIGDVLAARSRTVVLDEPAAAGQLAGRDRAPVTDTDRARALDPRHPAYLIYTSG
O52819_A3_asn                            AALSASPVGEVLAARSTTMVIDDPSAAGEVAGRDRAPVTDTDRTRRLDPRHPAYLIYTSG
                                         *.*****:*:****** *:*:*:*:***::*************:* **************

Q939Z1_A3_asn                            STGRPKAVVVTHRNLTNYLLHCGRMYPGLRGRSVLHSSIAFDLTVTATFTPLIVGGEIHV
O52819_A3_asn                            STGRPKAVVITHRNLTNYLFHCGRMYPGLRGRSVMHSSIAFDLTITAMFTPLTVGGTVHV
                                         *********:*********:**************:*********:** **** *** :**

Q939Z1_A3_asn                            GALEDLIGVVEAAPSIFLKATPSHLLTLDTASRGSAGSGDLLLGGEQLPADTVVQWRRKY
O52819_A3_asn                            GALEAVIGAVDSAPSIFLKATPSHLRTLDTGSRESAVSGDLLLGGEQLPVDTIVQWRRTY
                                         **** :**.*::************* ****.** ** ************.**:*****.*

Q939Z1_A3_asn                            PNIVVVNEYGPTEATVGCVEYRLEPGQECPPGGVVPIGTPLANMRAFVLDSWLRLVPPGA
O52819_A3_asn                            PNTVVVNEYGPTEATVGCVEYRLEPGQECPPGGVVPIGTPLTNMRAFVLDSWLRLVPPGA
                                         ** **************************************:******************

Q939Z1_A3_asn                            VGELYVAGAGLARGYLGRAGLTATRFVADPFGSGERMYRTGDLVQWNPDGQLVFAGRVDD
O52819_A3_asn                            VGELYVSGVGVARGYLGRAGLTASRFVADPFGSGERMYRTGDLVRWNPDGQLVFAGRVDD
                                         ******:*.*:************:********************:***************

Q939Z1_A3_asn                            QVKVRGFRIEPGEIEAALVAQESVGQAVVVARDSEIGTRLIGYVTAAGESGVDEA
O52819_A3_asn                            QVKVRGFRIEPGEIEAALVAQESVGQAVVVAHDSDVGKRLIAYVTAAGQTGVDTA
                                         *******************************:**::*.***.******::*** *
