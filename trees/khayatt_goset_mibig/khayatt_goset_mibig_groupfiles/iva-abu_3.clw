CLUSTAL W multiple sequence alignment


Q8NJX1_A16_iva|abu                       LTSNNTRNLLSGIIDRVIEVSSALYR-HINVSLGAKNPQVSVSSHNAAYVLFTSGSTGVP
Q8NJX1_A5_iva|abu                        LTSPLYHTLGDELSDTVLEISQALDDILLKVEPSTLAPDVALSPRNAAYVLFTSGSTGTP
                                         ***   :.* . : * *:*:*.**    ::*. .:  *:*::*.:*************.*

Q8NJX1_A16_iva|abu                       KGLVMTHGGISTSQMAIKRKWGINSSNRTLQFVPNVFDLCLGESILQLISGACIFIPSEY
Q8NJX1_A5_iva|abu                        KGFVIEHKAVCTSQTAITNRLGLHRGVRMLQFASYVFDLSVGEICCTLICGACLVIPSDE
                                         **:*: * .:.*** **..: *:: . * ***.. ****.:**    **.***:.***: 

Q8NJX1_A16_iva|abu                       TRMNGLKDFITEHKINTLFLTPSFVRTLSPDQLPSVTLLLLAGEAVPRDILTTWFGKVRL
Q8NJX1_A5_iva|abu                        VRLNNLAKFMREKDINWAWFTPSFIRTLEPADVPKLELVLLCGEASDKKILEIWVGKVQL
                                         .*:*.* .*: *:.**  ::****:***.* ::*.: *:**.***  :.**  *.***:*

Q8NJX1_A16_iva|abu                       WNGWGPAETCLFSSLHQFQSVDESPLTIGRPVGGFCWVVDPTNANKLAPIGTLGEVVIQG
Q8NJX1_A5_iva|abu                        FNGWGPAETCVFSSLHEWKSVTESPLTVGKPVASFIWIVDPKNPHRLAPVGTVGEAVVQG
                                         :*********:*****:::** *****:*:**..* *:***.*.::***:**:**.*:**

Q8NJX1_A16_iva|abu                       PTVLREYLADVERTKATTMYKLPAWAPYQDQPSWSRFFKSGDLASYNPDGTLEFSNRKDT
Q8NJX1_A5_iva|abu                        PTLLREYLDDPIRTKASILESSPRWAPRCESQHWNRFYLTGDFCCYNPDGTIEYHGRKDT
                                         **:***** *  ****: : . * ***  :.  *.**: :**:..******:*: .****

Q8NJX1_A16_iva|abu                       QVKIRGLRVELGEVEYHVRVALKVLVKLLLTYSRPIPVQGSSHISVSRMKHARQAFLNQM
Q8NJX1_A5_iva|abu                        QIKIRGLRVELGEVEHQIRNFLGESVHVTVDVHK----FESGSVLVAYIGYSEETMDNPD
                                         *:*************:::*  *   *:: :   :      *. : *: : ::.::: *  

Q8NJX1_A16_iva|abu                       ESI
Q8NJX1_A5_iva|abu                        E--
                                         *  
