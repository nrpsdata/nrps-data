CLUSTAL W multiple sequence alignment


Q9Z4X5_A3_9__asn                         LTTSAVPAGVFPAELPRLLLDDPDVTARLAAQPAHDLTDEDRTQPLSPWNAAYIIYTSGS
Q9Z4X5_A3_asn                            LTTSAVPAGVFPAELPRLLLDDPDVTARLAAQPAHDLTDEDRTQPLSPWNAAYIIYTSGS
                                         ************************************************************

Q9Z4X5_A3_9__asn                         TGRPKGVLVEHQPVLNYLAVSAELYPGVAGNALLHSPLSFDLTVTGLFAPLLNGGCVHLA
Q9Z4X5_A3_asn                            TGRPKGVLVEHQPVLNYLAVSAELYPGVAGNALLHSPLSFDLTVTGLFAPLLNGGCVHLA
                                         ************************************************************

Q9Z4X5_A3_9__asn                         DLEELHARALDGEVPDLPQTTFLKATPSHLPLITGLPGVCVPDGELVLGGESLTGRAVRT
Q9Z4X5_A3_asn                            DLEELHARALDGEVPDLPQTTFLKATPSHLPLITGLPGVCVPDGELVLGGESLTGRAVRT
                                         ************************************************************

Q9Z4X5_A3_9__asn                         LLAAHPGARVLNEYGPTETIVGCTTWRVEAPDDLADGVLTIGRPFPNTRMLVLDPYLQPV
Q9Z4X5_A3_asn                            LLAAHPGARVLNEYGPTETIVGCTTWRVEAPDDLADGVLTIGRPFPNTRMLVLDPYLQPV
                                         ************************************************************

Q9Z4X5_A3_9__asn                         PAGVPGELYVSGVQLARGYLNRPGQSASRFVANPFEGPGERMYRTGDIVRWNRRGDLEFI
Q9Z4X5_A3_asn                            PAGVPGELYVSGVQLARGYLNRPGQSASRFVANPFEGPGERMYRTGDIVRWNRRGDLEFI
                                         ************************************************************

Q9Z4X5_A3_9__asn                         SRVDDQVKIRGFRVELGEVESALSRQPGVPEAVAVVREDRPGDRRLVAYLVTGAGPVPVP
Q9Z4X5_A3_asn                            SRVDDQVKIRGFRVELGEVESALSRQPGVPEAVAVVREDRPGDRRLVAYLVTGAGPVPVP
                                         ************************************************************

Q9Z4X5_A3_9__asn                         S
Q9Z4X5_A3_asn                            S
                                         *
