CLUSTAL W multiple sequence alignment


Q8KLL5_A1_hpg|hpg2Cl                     VCRAANRAAVLDGGLDPIVLDDPEIRRAVAECARPSVQVSADDLAYVMYTSGSTGTPKGV
Q8KLL5_A1_4__hpg|hpg2Cl                  VCRAANRAAVLDGGLDPIVLDDPEIRRAVAECARPSVQVSADDLAYVMYTSGSTGTPKGV
                                         ************************************************************

Q8KLL5_A1_hpg|hpg2Cl                     AVSHGNVAALVGEPRWEIGPGDTVLTHASHAFDISLFEIWVPLLSGARMMIAEPGAVDGE
Q8KLL5_A1_4__hpg|hpg2Cl                  AVSHGNVAALVGEPRWEIGPGDTVLTHASHAFDISLFEIWVPLLSGARMMIAEPGAVDGE
                                         ************************************************************

Q8KLL5_A1_hpg|hpg2Cl                     ALARYVAAGVTAAHLTAGAFRVLAEESPESVAGLREVLTGGDEVPPAAVRRVRRACPDIR
Q8KLL5_A1_4__hpg|hpg2Cl                  ALARYVAAGVTAAHLTAGAFRVLAEESPESVAGLREVLTGGDEVPPAAVRRVRRACPDIR
                                         ************************************************************

Q8KLL5_A1_hpg|hpg2Cl                     VRHLYGPTETTLCATWWLLEPGDEAGGVLPIGRPLAGRRVHVLDAFLRPVPPGVKGELYV
Q8KLL5_A1_4__hpg|hpg2Cl                  VRHLYGPTETTLCATWWLLEPGDEAGGVLPIGRPLAGRRVHVLDAFLRPVPPGVKGELYV
                                         ************************************************************

Q8KLL5_A1_hpg|hpg2Cl                     AGAGVARGYLGRSALTAERFVADPFVPGERMYRTGDLAHWTDEGTLVFGGRADDQVKIRG
Q8KLL5_A1_4__hpg|hpg2Cl                  AGAGVARGYLGRSALTAERFVADPFVPGERMYRTGDLAHWTDEGTLVFGGRADDQVKIRG
                                         ************************************************************

Q8KLL5_A1_hpg|hpg2Cl                     YRVEPGEIEAVLAGLPGVGQAVVLARDERLIGYVVAEEGRDLDPAG
Q8KLL5_A1_4__hpg|hpg2Cl                  YRVEPGEIEAVLAGLPGVGQAVVLARDERLIGYVVAEEGRDLDPAG
                                         **********************************************
