CLUSTAL W multiple sequence alignment


O52820_A2_5__hpg|hpg2Cl                  VCLEATRKAVPDGVEPVVMDVPAIDGVRHEAPQVTVGAHDLAYVMYTSGSTGVPKGVAVP
Q939Z0_A2_5__hpg|hpg2Cl                  VCVEATRKAVPDGVEPVVVDLPVIGGVRPEAPPVTVGAHDVAYVMYTSGSTGVPKAVAVP
                                         **:***************:*:*.*.*** *** *******:**************.****

O52820_A2_5__hpg|hpg2Cl                  HGSVAALASDPGWSQGPDDCVLLHASHAFDASLVEIWVPLVNGSRVMVAEPGAVDAERLR
Q939Z0_A2_5__hpg|hpg2Cl                  HGSVAALASDPGWSQGPGDCVLLHASHAFDASLVEIWVPLVSGARVLVAEPGTVDAERLR
                                         *****************.***********************.*:**:*****:*******

O52820_A2_5__hpg|hpg2Cl                  EAISRGVTTVHLTAGAFRAVAEESPDSFTGLREILTGGDAVPLASVVRMRRACPDVRVRQ
Q939Z0_A2_5__hpg|hpg2Cl                  EAVSRGVTTVHLTAGAFRAVAEESPDSFIGLREILTGGDAVPLASVVRMRQACPDVRVRQ
                                         **:************************* *********************:*********

O52820_A2_5__hpg|hpg2Cl                  LYGPTEITLCATWHVIEPGAETGDTLPIGRPLAGRQAYVLDAFLQPVAPNVTGELYIAGA
Q939Z0_A2_5__hpg|hpg2Cl                  LYGPTEITLCATWLVLEPGAATGDVLPIGRPLAGRQAYVLDAFLQPVAPNVTGELYLAGA
                                         ************* *:**** ***.*******************************:***

O52820_A2_5__hpg|hpg2Cl                  GLAHGYLGNNGSTSERFIANPFASGERMYRTGDLARWTDQGELLFAGRADSQVKIRGYRV
Q939Z0_A2_5__hpg|hpg2Cl                  GLAHGYLGNTAATSERFVANPFSGGGRMYRTGDLARWTDQGELVFAGRADSQVKIRGYRV
                                         *********..:*****:****:.* *****************:****************

O52820_A2_5__hpg|hpg2Cl                  EPGEIEVALTEVPHVAQAVVVAREDHPGDKRLIAYVTAEEGPALAADA
Q939Z0_A2_5__hpg|hpg2Cl                  EPGEVEVALTEVPHVAQAVVVAREGQPGEKRLIAYVTAEAGSALESAA
                                         ****:*******************.:**:********** *.** : *
