CLUSTAL W multiple sequence alignment


Q884E5_A1_asp                            LCAEGDRRETSLNVAGCQGLAWTPALWQSLPTSRPDIELPADSAAYVIHTSGSTGQPKGV
Q884E3_A1_asp                            IHASGDDKAAQLGV--CPVLAFDAALWSEVDGGELSVRIIAEQPAYIIYTSGSTGQPKGV
                                         : *.** : :.*.*  *  **: .***..:  .. .:.: *:..**:*:***********

Q884E5_A1_asp                            VVSQGALASYVRGLLEQLQLAPEVSMALVSTIAADLGHTVLFGALCSGRTLHVLTESLGF
Q884E3_A1_asp                            VISHGALANYVQGVLARLSLNDGASMAMVSTVAADLGHTLLFGALASGRPLHLLSHEQAF
                                         *:*:****.**:*:* :*.*   .***:***:*******:*****.***.**:*:.. .*

Q884E5_A1_asp                            DPDAFATYMAEHQIGVLKIVPGHLAALLQAAQPADVLPQHALIVGGEACSPALVEQVRQL
Q884E3_A1_asp                            DPDGFARYMAEHQVEVLKIVPSHLQGLLQAAHPADVLPSQLLMLGGEASSWALIEQVRAL
                                         ***.** ******: ******.** .*****:******.: *::****.* **:**** *

Q884E5_A1_asp                            KPGCRVINHYGPSETTVGVLTHEVPALSELNAIPQVSRFPQCSAHSGDPLRSVPVGAPLP
Q884E3_A1_asp                            KPGCRIVNHYGPTETTVGILTHEV-----------AERLNAC--------RSVPVGQPLA
                                         *****::*****:*****:*****           ..*:  *        ****** **.

Q884E5_A1_asp                            GASAYVLDDVLNPVATQVAGELYIGGDSVALGYLGQPALTAERFVPDPFAQDGARVYRSG
Q884E3_A1_asp                            NCKARVLDAYLNPVAERVSGELYLGGQGLAQGYLGRAAMTAERFVPDPDA-DGQRLYRAG
                                         ...* ***  ***** :*:****:**:.:* ****:.*:********* * ** *:**:*

Q884E5_A1_asp                            DRMRHNHQGLLEFIGRADDQVKVRGYRVEPAEVAQVLLSLPSVAQVSVLALPVDEDESRL
Q884E3_A1_asp                            DRARWV-DGVLEYLGRADDQVKIRGYRVEPGEVGQVLQTLENVAEAVVLAQPLESDETRL
                                         ** *   :*:**::********:*******.**.*** :* .**:. *** *::.**:**

Q884E5_A1_asp                            QLVAYCVAAAAASLTVDS
Q884E3_A1_asp                            QLVAYCVAAVGVRLNVES
                                         *********... *.*:*
