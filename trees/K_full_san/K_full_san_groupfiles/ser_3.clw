CLUSTAL W multiple sequence alignment


P11454_A1_2__ser                         PSLLITTDD---QLPRFSDVPNL--------TSLCYNAPLTPQGSAPLQLSQPHHTAYII
Q9Z4X6_A1_ser                            AMLVLTTRDTAERLPG-DGTPRLLLDEPAAAGTTAAGAP-APPGTLPRALPAPGHPAYVI
P29698_A1_2__ser                         PSLLITTDD---QLPRFSDVPNL--------TNLCYNAPLTPQGSAPLQLSQPHHTAYII
Q50E73_A6_11__ser                        PVAVLTRGDV--ELPG--SVPRIGLDDTEIRATLATA-----PGTNPGTPVTEAHPAYMI
                                         .  ::*  *   .**   ..*.:         . .        *: *       *.**:*

P11454_A1_2__ser                         FTSGSTGRPKGVMVGQTAIVNRLLWMQNHYPLTGEDVVAQKTPCSFDVSVWEFFWPFIAG
Q9Z4X6_A1_ser                            YTSGSTGRPKGVVISHRAIVNRLAWMQDTYGLEPSDRVLQKTPSGFDVSVWEFFWPLVQG
P29698_A1_2__ser                         FTSGSTGRPKGVMVGQTAIVNRLLWMQNHYPLTGEDVVAQKTPCSFDVSVWEFFWPFIAG
Q50E73_A6_11__ser                        YTSGSTGRPKGVVVSHGAIVNRLAWMQAEYRLDATDRVLQKTPAGFDVSVWEFFWPLLEG
                                         :***********::.: ****** ***  * *   * * ****..***********:: *

P11454_A1_2__ser                         AKLVMAEPEAHRDPLAMQQFFAEYGVTTTHFVPSMLAAFVASLTPQTARQSCAT-LKQVF
Q9Z4X6_A1_ser                            ATLVVARPGGHTDPAYLAGTVRREGVTTLHFVPSMLDVFLRE--PAAAALGGATPVRRVF
P29698_A1_2__ser                         AKLVMAEPEAHRDPLAMQQFFAEYGVTTTHFVPSMLAAFVASLTPQTARQSCVT-LKQVF
Q50E73_A6_11__ser                        AVLVFARPGGHRDAAYLAGLIERERITTAHFVPSMLRVFLEE--PGAALCTG---LRRVI
                                         * **.*.* .* *.  :   . .  :** ******* .*: .  * :*       :::*:

P11454_A1_2__ser                         CSGEALPADLCREWQQLTGAPLHNLYGPTEAAVDVSWYPAFGEELAQVRGSS-VPIGYPV
Q9Z4X6_A1_ser                            CSGEALPAELRARFRAVSDVPLHNLYGPTEAAVDVTYWPC-----AEDTGDGPVPIGRPV
P29698_A1_2__ser                         CSGEALPADLCREWQQLTGAPLHNLYGPTEAAVDVSWYPAFGEELAQVRGSS-VPIGYPV
Q50E73_A6_11__ser                        CSGEALGTDLAVDFRAKLPVPLHNLYGPTEAAVDVTHHAY-----EPATGTATVPIGRPI
                                         ****** ::*   ::    .***************:  .          * . **** *:

P11454_A1_2__ser                         WNTGLRILDAMMHPVPPGVAGDLYLTGIQLAQGYLGRPDLTASRFIADPF-APGERMYRT
Q9Z4X6_A1_ser                            WNTRMYVLDAALRPVPAGVPGELYIAGVQLARGYLGRPALSAERFTADPHGAPGSRMYRT
P29698_A1_2__ser                         WNTGLRILDAMMHPVPPGVAGDLYLTGIQLAQGYLGRPDLTASRFIADPF-APGERMYRT
Q50E73_A6_11__ser                        WNIRTYVLDAALRPVPPGVPGELYLAGAGLARGYHGRPALTAERFVACPFGVPGERMYRT
                                         **    :*** ::***.**.*:**::*  **:** *** *:*.** * *. .**.*****

P11454_A1_2__ser                         GDVARWLDNGAVEYLGRSDDQLKIRGQRIELGEIDRVMQALPDVEQAVTHACVINQAAAT
Q9Z4X6_A1_ser                            GDLARWNHDGSLDYLGRADHQVKLRGFRIELGEIEAALVRQPEIAQAAV---VLREDRP-
P29698_A1_2__ser                         GDVARWLDNGAVEYLGRSDDQLKIRGQRIELGEIDRVMQALPDVKQAVTHACVINQAAAT
Q50E73_A6_11__ser                        GDLVRWRVDGTLEFVGRADDQVKVRGFRVELGEVEGAVAAHPDVVRAVV---VVREDRP-
                                         **:.**  :*:::::**:*.*:*:** *:****:: .:   *:: :*..   *:.:  . 

P11454_A1_2__ser                         GGDARQLVGYLV------------SQSG----------
Q9Z4X6_A1_ser                            -GDQR-LVAYTVPARDADTLTGPPAEAGTHPGPGAAPD
P29698_A1_2__ser                         GGDARQLVGYLV------------SQSG----------
Q50E73_A6_11__ser                        -GDHR-LVAYVT--------------------------
                                          ** * **.* .                          
