CLUSTAL W multiple sequence alignment


Q70AZ7_A2_hpg|hpg2Cl                     VSVAVCVQATSGAVPDGLAPVVMDSPAIAAAPSEAPPITVGAHDLAYVMYTSGSTGVPKG
Q939Z0_A2_5__hpg|hpg2Cl                  ASVAVCVEATRKAVPDGVEPVVVDLPVIGGVRPEAPPVTVGAHDVAYVMYTSGSTGVPKA
Q70AZ7_A2_5__hpg|hpg2Cl                  VSVAVCVQATSGAVPDGLAPVVMDSPAIAAAPSEAPPITVGAHDLAYVMYTSGSTGVPKG
O52820_A2_5__hpg|hpg2Cl                  ASVAVCLEATRKAVPDGVEPVVMDVPAIDGVRHEAPQVTVGAHDLAYVMYTSGSTGVPKG
Q8KLL5_A2_5__hpg|hpg2Cl                  VTVAVCAEGTRNAVPDGLEPVPVDAPWAGETRHETP--TVTARDAAYVMYTSGSTGEPKG
Q8KLL5_A2_hpg|hpg2Cl                     VTVAVCAEGTRNAVPDGLEPVPVDAPWAGETRHETP--TVTARDAAYVMYTSGSTGEPKG
                                         .:**** :.*  *****: ** :* *    .  *:*  ** *:* *********** **.

Q70AZ7_A2_hpg|hpg2Cl                     VAVPHGSVAALAGDPGWSVGPGDGVLMHAPHAFDASLLEIWVPLLSGAHVVVADPGAVDA
Q939Z0_A2_5__hpg|hpg2Cl                  VAVPHGSVAALASDPGWSQGPGDCVLLHASHAFDASLVEIWVPLVSGARVLVAEPGTVDA
Q70AZ7_A2_5__hpg|hpg2Cl                  VAVPHGSVAALAGDPGWSVGPGDGVLMHAPHAFDASLLEIWVPLLSGAHVVVADPGAVDA
O52820_A2_5__hpg|hpg2Cl                  VAVPHGSVAALASDPGWSQGPDDCVLLHASHAFDASLVEIWVPLVNGSRVMVAEPGAVDA
Q8KLL5_A2_5__hpg|hpg2Cl                  IVVPHGSVAALAGDPGWALDADDCVLMHASHAFDASLFEIWAPLVRGARVMVAEPGAVDT
Q8KLL5_A2_hpg|hpg2Cl                     IVVPHGSVAALAGDPGWALDADDCVLMHASHAFDASLFEIWAPLVRGARVMVAEPGAVDT
                                         :.**********.****: ...* **:**.*******.***.**: *::*:**:**:**:

Q70AZ7_A2_hpg|hpg2Cl                     QRLREAIDRGVTTVHLTAGSFRVLAEESPDAFRGLREVLTGGDAVPLASVVRLRETCPEI
Q939Z0_A2_5__hpg|hpg2Cl                  ERLREAVSRGVTTVHLTAGAFRAVAEESPDSFIGLREILTGGDAVPLASVVRMRQACPDV
Q70AZ7_A2_5__hpg|hpg2Cl                  QRLREAIDRGVTTVHLTAGSFRVLAEESPDAFRGLREVLTGGDAVPLASVVRLRETCPEI
O52820_A2_5__hpg|hpg2Cl                  ERLREAISRGVTTVHLTAGAFRAVAEESPDSFTGLREILTGGDAVPLASVVRMRRACPDV
Q8KLL5_A2_5__hpg|hpg2Cl                  QRLREAVARGVTTVHLTAGSFRVLAEESPGSFDGLREILTGGDVVPLASVAQLRRACPDV
Q8KLL5_A2_hpg|hpg2Cl                     QRLREAVARGVTTVHLTAGSFRVLAEESPGSFDGLREILTGGDVVPLASVAQLRRACPDV
                                         :*****: ***********:**.:*****.:* ****:*****.******.::*.:**::

Q70AZ7_A2_hpg|hpg2Cl                     RVRHLYGPTETTLCATWHLIEPGVATGDTLPIGRPLAGRRAYVLDAFLQPVAPNVTGELY
Q939Z0_A2_5__hpg|hpg2Cl                  RVRQLYGPTEITLCATWLVLEPGAATGDVLPIGRPLAGRQAYVLDAFLQPVAPNVTGELY
Q70AZ7_A2_5__hpg|hpg2Cl                  RVRHLYGPTETTLCATWHLIEPGVATGDTLPIGRPLAGRRAYVLDAFLQPVAPNVTGELY
O52820_A2_5__hpg|hpg2Cl                  RVRQLYGPTEITLCATWHVIEPGAETGDTLPIGRPLAGRQAYVLDAFLQPVAPNVTGELY
Q8KLL5_A2_5__hpg|hpg2Cl                  RVRHLYGPTETTLCGTWHLLEPGDEPGDVLPIGRPLAGRRAYVLDAFLQPVAPNVTGELY
Q8KLL5_A2_hpg|hpg2Cl                     RVRHLYGPTETTLCGTWHLLEPGDEPGDVLPIGRPLAGRRAYVLDAFLQPVAPNVTGELY
                                         ***:****** ***.** ::***  .**.**********:********************

Q70AZ7_A2_hpg|hpg2Cl                     LAGAGLARGYLGAAAATAERFVADPFAAGERMYRTGDLARWTEQGELLFAGRADAQVKIR
Q939Z0_A2_5__hpg|hpg2Cl                  LAGAGLAHGYLGNTAATSERFVANPFSGGGRMYRTGDLARWTDQGELVFAGRADSQVKIR
Q70AZ7_A2_5__hpg|hpg2Cl                  LAGAGLARGYLGAAAATAERFVADPFAAGERMYRTGDLARWTEQGELLFAGRADAQVKIR
O52820_A2_5__hpg|hpg2Cl                  IAGAGLAHGYLGNNGSTSERFIANPFASGERMYRTGDLARWTDQGELLFAGRADSQVKIR
Q8KLL5_A2_5__hpg|hpg2Cl                  LAGVGLALGYLGARGATSERFVADPFVPGERMYRTGDLARRNDRGELLFAGRADAQVKIR
Q8KLL5_A2_hpg|hpg2Cl                     LAGVGLALGYLGARGATSERFVADPFVPGERMYRTGDLARRNDRGELLFAGRADAQVKIR
                                         :**.*** ****  .:*:***:*:**  * ********** .::***:******:*****

Q70AZ7_A2_hpg|hpg2Cl                     GYRVEPAEIEAALTAIPEVAQAVVVAREDGPGEKRLIAYVTAAGQPGPDPA
Q939Z0_A2_5__hpg|hpg2Cl                  GYRVEPGEVEVALTEVPHVAQAVVVAREGQPGEKRLIAYVTAEAGSALESA
Q70AZ7_A2_5__hpg|hpg2Cl                  GYRVEPAEIEAALTAIPEVAQAVVVAREDGPGEKRLIAYVTAAGQPGPDPA
O52820_A2_5__hpg|hpg2Cl                  GYRVEPGEIEVALTEVPHVAQAVVVAREDHPGDKRLIAYVTAEEGPALAAD
Q8KLL5_A2_5__hpg|hpg2Cl                  GYRVEPTEIETVLAEAPQVAQTVVVAREDGPGEKRLIAYAIAEPDQVLDPE
Q8KLL5_A2_hpg|hpg2Cl                     GYRVEPTEIETVLAEAPQVAQTVVVAREDGPGEKRLIAYAIAEPDQVLDPE
                                         ****** *:*..*:  *.***:******. **:******. *       . 
