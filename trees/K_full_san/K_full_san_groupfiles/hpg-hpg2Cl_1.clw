CLUSTAL W multiple sequence alignment


Q8KLL3_A1_hpg|hpg2Cl                     ASLMACSAATAGRVPEGVEPVVVTDEGRGDASAVPVSPGDLAYVMYTSGSTGTPKGVAVP
Q70AZ9a_A1_hpg|hpg2Cl                    ASRMVCSAATRDGVPEGIEAIVVTDEEAFEASAAGARPGDLAYVMYTSGSTGIPKGVAVP
Q70AZ9b_A1_hpg|hpg2Cl                    ASRMVCSAATRDGVPEGIEAIVVTDEEAFEASAAGARPGDLAYVMYTSGSTGIPKGVAVP
                                         ** *.***** . ****:*.:*****   :***. . *************** *******

Q8KLL3_A1_hpg|hpg2Cl                     HRSVAELAGNPGWAVKPGDAILMHAPHAFDASLFEIWVPLVSGARVVIAEPGAVDARRLR
Q70AZ9a_A1_hpg|hpg2Cl                    HRSVAELAGNPGWAVEPGDAVLMHAPYAFDASLFEIWVPLVSGGRVVIAEPGPVDARRLR
Q70AZ9b_A1_hpg|hpg2Cl                    HRSVAELAGNPGWAVEPGDAVLMHAPYAFDASLFEIWVPLVSGGRVVIAEPGPVDARRLR
                                         ***************:****:*****:****************.********.*******

Q8KLL3_A1_hpg|hpg2Cl                     EAIAAGVTKVHLTAGSFRALAEESSESFAGLQEVLTGGDVVPAHAVEKVRKAVPQARIRH
Q70AZ9a_A1_hpg|hpg2Cl                    EAISSGVTRAHLTAGSFRAVAEESPESFAGLREVLTGGDVVPAHAVARVRSACPRVRIRH
Q70AZ9b_A1_hpg|hpg2Cl                    EAISSGVTRAHLTAGSFRAVAEESPESFAGLREVLTGGDVVPAHAVARVRSACPRVRIRH
                                         ***::***:.*********:****.******:************** :**.* *:.****

Q8KLL3_A1_hpg|hpg2Cl                     LYGPTETTLCATWHLLQPSEALGPVLPIGRPLPGRRAQVLDASLRPLPPGVVGDLYLSGA
Q70AZ9a_A1_hpg|hpg2Cl                    LYGPTETTLCATWHLLEPGDEIGPVLPIGRPLPGRRAQVLDASLRAVAPGVIGDLYLSGA
Q70AZ9b_A1_hpg|hpg2Cl                    LYGPTETTLCATWHLLEPGDEIGPVLPIGRPLPGRRAQVLDASLRAVAPGVIGDLYLSGA
                                         ****************:*.: :***********************.:.***:********

Q8KLL3_A1_hpg|hpg2Cl                     GLADGYLDRAALTAERFVADPSVPGGRMYRTGDLVQWTADGELLFVGRADDQVKIRGFRI
Q70AZ9a_A1_hpg|hpg2Cl                    GLADGYLRRAGLTAERFVADPSAPGARMYRTGDLAQWTADGALLFAGRADDQVKVRGFRI
Q70AZ9b_A1_hpg|hpg2Cl                    GLADGYLRRAGLTAERFVADPSAPGARMYRTGDLAQWTADGALLFAGRADDQVKVRGFRI
                                         ******* **.***********.**.********.****** ***.********:*****

Q8KLL3_A1_hpg|hpg2Cl                     EPGEIEAALTAQPDVHEAVVVAIDGRLIGYAVT--DVDPV
Q70AZ9a_A1_hpg|hpg2Cl                    EPAEVEAALTAQPGVHEAVVRAVDGRLVGYVVAEGDAEPA
Q70AZ9b_A1_hpg|hpg2Cl                    EPAEVEAALTAQPGVHEAVVRAVDGRLVGYVVAEGDAEPA
                                         **.*:********.****** *:****:**.*:  *.:*.
