CLUSTAL W multiple sequence alignment


Q54959_A1_thr|athr                       -------------------APVLVLDPQAMTEDLAG-------YPDTAPRTA-----VDG
removed_A1_2__thr|athr                   PTLILTTTETEAKLPDRHTAPALRLDDPETLAALAG-------QPANSPAVG-----LRP
P45745_A2_3__thr|athr                    PAFIMTNTKAANHIPPVENVPKIVLDDPELAEKLNT-------YPAGNPKNKDRTQPLSP
Q9Z4X6_A2_thr|athr                       PAALLTDRATAGRLPAHE-VPRIVLDAPAPADGGTTGGDPADAHPATDLAQGERVRPLDP
                                                            .* : **                  *            :  

Q54959_A1_thr|athr                       AHPAYVIYTSGSTGRPKGVVIPHSNVVRLFTSTDHWFGFGPDDVWTLFHSYAFDFSVWEI
removed_A1_2__thr|athr                   DHPAYVIYTSGSTGVPKGVVNTHRNVVRLFDATRPWFDFGPDDVWTLFHSYAFDFSVWEL
P45745_A2_3__thr|athr                    LNTAYVIYTSGSTGVPKGVMIPHQNVTRLFAATEHWFRFSSGDIWTMFHSYAFDFSVWEI
Q9Z4X6_A2_thr|athr                       RDTAYVIYTSGSTGRPKGVAVPHGNVVRLFSATAPWFGFDEHDVWTLFHSYAFDFSVWEL
                                          ..*********** ****  .* **.*** :*  ** *.  *:**:************:

Q54959_A1_thr|athr                       WGALLHGGRLVVVPYHVSRSPGDFLDLLAREKVTVLNQTPTAFHQL--DAADRARTA--A
removed_A1_2__thr|athr                   WGALLHGGRLVVVPYDVSRSPHAFLDLLADQGVTVLNQTPSAFHQLAQAAADPGR-P--P
P45745_A2_3__thr|athr                    WGPLLHGGRLVIVPHHVSRSPEAFLRLLVKEGVTVLNQTPSAFYQF--MQAEREQ-PDLG
Q9Z4X6_A2_thr|athr                       WGPLLHGGRLVVVPHDVTRDPAAFLALLARERVTVLNQTPSAFHQL--AAADREN-P---
                                         **.********:**:.*:*.*  ** **. : ********:**:*:    *:  . .   

Q54959_A1_thr|athr                       PELALRYVVFGGEALDVARLADWYARRGT-AARLVNMYGITETTVHVTHAPLGPGHAVPG
removed_A1_2__thr|athr                   RRLALRTVVFGGEALQPARLAEWYRRHPEDTPQLVNMYGITETTVHVTHQPLTRDRAAAG
P45745_A2_3__thr|athr                    QALSLRYVIFGGEALELSRLEDWYNRHPENRPQLINMYGITETTVHVSYIELDRSMAALR
Q9Z4X6_A2_thr|athr                       TELALRTVVFGGEALDLSRLADWYERHAEDAPALVNMYGITETTVHVSHFALDRATAAAS
                                           *:** *:******: :** :** *:    . *:************::  *    *.  

Q54959_A1_thr|athr                       TPSLLGGPIPDLTPRVLDAALRPVPPGFTGELYVAGAGLARGYLNRPALTAQRFPADP--
removed_A1_2__thr|athr                   AASVIGAGISDLRTHVLDGGLQLVPPGAVGELYVAGPGLARGYLGRPALTAERFVADP--
P45745_A2_3__thr|athr                    ANSLIGCGIPDLGVYVLDERLQPVPPGVAGELYVSGAGLARGYLGRPGLTSERFIADP--
Q9Z4X6_A2_thr|athr                       SASTIGVNIPDLRVYVLDDRLRPTAPGVTGEMYVAGAGLARGYLGRPALTADRFPADPYA
                                         : * :*  *.**   ***  *: ..** .**:**:*.*******.**.**::** ***  

Q54959_A1_thr|athr                       --YGAPGTRMYRTGDLVRHLDDGTYAYLGRGDDQVKIRGFRIELGEIENVLATHPGVAQA
removed_A1_2__thr|athr                   --YGAPGARMYRTGDLVRRNPDGELEFVGRADHQVKVRGFRIELGEVEAALLAHPDVEQA
P45745_A2_3__thr|athr                    --FGPPGTRMYRTGDVARLRADGSLDYVGRADHQVKIRGFRIELGEIEAALVQHPQLEDA
Q9Z4X6_A2_thr|athr                       ALFGERGTRMYRTGDLARRRTDGGLDYLGRADQQVKIRGFRIEPGEIEAVLAAHPAVDDV
                                           :*  *:*******:.*   **   ::**.*.***:****** **:* .*  ** : :.

Q54959_A1_thr|athr                       AAVVREDRHGDLRLAAYAVPTPGTEPDVA
removed_A1_2__thr|athr                   TVIVREDRPGDTRLVAYVVGREALRPEQV
P45745_A2_3__thr|athr                    AVIVREDQPGDKRLAAYVIPSEETF-DTA
Q9Z4X6_A2_thr|athr                       AVVAREDVQGDPRLVAYVVTGSGA---TA
                                         :.:.***  ** **.**.:         .
