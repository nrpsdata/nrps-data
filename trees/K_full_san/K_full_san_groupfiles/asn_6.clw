CLUSTAL W multiple sequence alignment


Q9Z4X5_A3_9__asn                         PVCALTTSAVPAGVFPAELPRLLLDDPDVTARLAAQPAHDLTDEDRTQPLSPWNAAYIIY
Q9Z4X5_A3_asn                            PVCALTTSAVPAGVFPAELPRLLLDDPDVTARLAAQPAHDLTDEDRTQPLSPWNAAYIIY
                                         ************************************************************

Q9Z4X5_A3_9__asn                         TSGSTGRPKGVLVEHQPVLNYLAVSAELYPGVAGNALLHSPLSFDLTVTGLFAPLLNGGC
Q9Z4X5_A3_asn                            TSGSTGRPKGVLVEHQPVLNYLAVSAELYPGVAGNALLHSPLSFDLTVTGLFAPLLNGGC
                                         ************************************************************

Q9Z4X5_A3_9__asn                         VHLADLEELHARALDGEVPDLPQTTFLKATPSHLPLITGLPGVCVPDGELVLGGESLTGR
Q9Z4X5_A3_asn                            VHLADLEELHARALDGEVPDLPQTTFLKATPSHLPLITGLPGVCVPDGELVLGGESLTGR
                                         ************************************************************

Q9Z4X5_A3_9__asn                         AVRTLLAAHPGARVLNEYGPTETIVGCTTWRVEAPDDLADGVLTIGRPFPNTRMLVLDPY
Q9Z4X5_A3_asn                            AVRTLLAAHPGARVLNEYGPTETIVGCTTWRVEAPDDLADGVLTIGRPFPNTRMLVLDPY
                                         ************************************************************

Q9Z4X5_A3_9__asn                         LQPVPAGVPGELYVSGVQLARGYLNRPGQSASRFVANPFEGPGERMYRTGDIVRWNRRGD
Q9Z4X5_A3_asn                            LQPVPAGVPGELYVSGVQLARGYLNRPGQSASRFVANPFEGPGERMYRTGDIVRWNRRGD
                                         ************************************************************

Q9Z4X5_A3_9__asn                         LEFISRVDDQVKIRGFRVELGEVESALSRQPGVPEAVAVVREDRPGDRRLVAYLVTGAGP
Q9Z4X5_A3_asn                            LEFISRVDDQVKIRGFRVELGEVESALSRQPGVPEAVAVVREDRPGDRRLVAYLVTGAGP
                                         ************************************************************

Q9Z4X5_A3_9__asn                         VPVPS
Q9Z4X5_A3_asn                            VPVPS
                                         *****
