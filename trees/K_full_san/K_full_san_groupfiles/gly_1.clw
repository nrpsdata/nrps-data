CLUSTAL W multiple sequence alignment


Q45R83_A3_10__gly                        PGCVITTTDLARRLPPVPAPLLVLDDPATAARLAATTATALAEDPREQNGE-WGEELAYT
Q9Z4X5_A2_8__gly                         ------------------GPALTVTEPVAEAGLSGYGDADLGAD--ELRGPVHGAHPAYT
Q9Z4X5_A2_gly                            ------------------GPALTVTEPVAEAGLSGYGDADLGAD--ELRGPVHGAHPAYT
                                                           .* *.: :*.: * *:.   : *. *  * .*   * . ***

Q45R83_A3_10__gly                        IYTSGSTGRPKGVMVTRSAVANFLADMNERLELGPGDRLLAVTTVSFDIAVLELLAPLLT
Q9Z4X5_A2_8__gly                         IYTSGSTGRPKGVVVPRGALDNFLADMGRRFTPGSGDRLLAVTTVGFDIAGLEIFLPLLH
Q9Z4X5_A2_gly                            IYTSGSTGRPKGVVVPRGALDNFLADMGRRFTPGSGDRLLAVTTVGFDIAGLEIFLPLLH
                                         *************:*.*.*: ******..*:  *.**********.**** **:: *** 

Q45R83_A3_10__gly                        GGTVVLADATTQRDPAAVRSLCAREGVTVIQATPSWWHAMAVDGGLDLTALRVLVGGEAL
Q9Z4X5_A2_8__gly                         GAVLVLADEETARDPHALLHRVSASGITMVQATPSLWQGVAAVAGDELAGVRVLVGGEAL
Q9Z4X5_A2_gly                            GAVLVLADEETARDPHALLHRVSASGITMVQATPSLWQGVAAVAGDELAGVRVLVGGEAL
                                         *..:****  * *** *:    : .*:*::***** *:.:*. .* :*:.:*********

Q45R83_A3_10__gly                        PPALARTLLEPGRAPLGDYLLNLYGPTETTVWSTVARITADSLEAHGGAVPTGTPIARTA
Q9Z4X5_A2_8__gly                         PSELARALTDRARS-----VTNLYGPTEATIWATAADV------AESGPV-IGRPLANTS
Q9Z4X5_A2_gly                            PSELARALTDRARS-----VTNLYGPTEATIWATAADV------AESGPV-IGRPLANTS
                                         *. ***:* : .*:     : *******:*:*:*.* :      *..*.*  * *:*.*:

Q45R83_A3_10__gly                        AYVLDAALRPVPDGVPGELYLAGAGLARGYLGRPGMTAERFVACPFGEPGERMYRTGDLA
Q9Z4X5_A2_8__gly                         AYVLDSALRPVPVGVPGELYLAGEQLAQGYHLRPALTSERFTADPYGPAGTRMYRTGDLV
Q9Z4X5_A2_gly                            AYVLDSALRPVPVGVPGELYLAGEQLAQGYHLRPALTSERFTADPYGPAGTRMYRTGDLV
                                         *****:****** **********  **:**  **.:*:***.* *:* .* ********.

Q45R83_A3_10__gly                        RWRADGNLEHLGRTDDQVKVRGFRIELGEVERALTQAHGVGRAAAAVHPDAAGSARLVGY
Q9Z4X5_A2_8__gly                         CRRRDGALRYLSRVDQQVKLRGFRIELGEIEAELSRHPAVAESAVTVREDRPGDRRLVGY
Q9Z4X5_A2_gly                            CRRRDGALRYLSRVDQQVKLRGFRIELGEIEAELSRHPAVAESAVTVREDRPGDRRLVGY
                                           * ** *.:*.*.*:***:*********:*  *::  .*..:*.:*: * .*. *****

Q45R83_A3_10__gly                        LVPAGGSGALD
Q9Z4X5_A2_8__gly                         VVPKGPEGPAG
Q9Z4X5_A2_gly                            VVPKGPEGPAG
                                         :** * .*. .
