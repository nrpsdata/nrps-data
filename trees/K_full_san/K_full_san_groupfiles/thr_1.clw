CLUSTAL W multiple sequence alignment


P39846_A2_thr                            AKLLLTERGLNKPADYTGHILYIDECENNSIPADVNIEEIVTDQPAYVIYTSGTTGQPKG
O87704_A2_4__thr                         ARILLTEHGHNKPADYHGQILYLNDAENELISPDLKAQETLADQPAYVIYTSGTTGQPKG
                                         *::****:* ****** *:***:::.**: *..*:: :* ::******************

P39846_A2_thr                            VIVEHRNVISLLKHQNLPFEFNHEDVWTLFHSYCFDFSVWEMFGALLNGSTLVVVSKETA
O87704_A2_4__thr                         VVVEHRNVISLLKHQDLPFDFGSEDVWTLFHSYCFDFSVWEMFGALLNGSTLVVVSRETA
                                         *:*************:***:*. *********************************:***

P39846_A2_thr                            RDPQAFRLLLKKERVTVLNQTPTAFYGLMLEDQNHTDHLNIRYVIFGGEALQPGLLQSWN
O87704_A2_4__thr                         RDPNAFRLLLKNEGVTVLNQTPTAFYGLIHEEENHTDRLHVRYVIFGGEALQPGMLVTWN
                                         ***:*******:* **************: *::****:*::*************:* :**

P39846_A2_thr                            EKYPHTDLINMYGITETTVHVTFKKLSAADIAKNKSNIGRPLSTLQAHVMDAHMNLQPTG
O87704_A2_4__thr                         EKYPDTDLINMYGITETTVHVTYKKLSSADIEKNKSNIGKPLATLQAYVMDAHMNLQPTG
                                         ****.*****************:****:*** *******:**:****:************

P39846_A2_thr                            VPGELYIGGEGVARGYLNRDELTADRFVSNPYLPGDRLYRTGDLAKRLSNGELEYLGRID
O87704_A2_4__thr                         VPGELYIGGEGVARGYLNRDDLTAARFVPNPYLPGDRLYRTGDLAKRLASGDLEYMGRID
                                         ********************:*** ***.*******************:.*:***:****

P39846_A2_thr                            EQVKVRGHRIELGEIQAALLQYPMIKEAAVITRADEQGQTAIYAYMVIKDQQAANIS
O87704_A2_4__thr                         DQVKVRGHRIELGEIQASLLQLPIIKEAAVITRDDEQGQSAVYAYLVAEDGQVVNEA
                                         :****************:*** *:********* *****:*:***:* :* *..* :
