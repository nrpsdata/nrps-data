CLUSTAL W multiple sequence alignment


Q9K5L9_A1_7__ile                         VSVLLTQQHLVEKL--------PEHQVPVVCLDTDWLVICESSPESPITEVQPGNLAYVI
Q84BC7_A1_4__ile                         LSVILTQEKLVNKLGERLRRGFAERNASVICLDSNWDIINQQTQNNPTTSVTADNLAYVM
                                         :**:***::**:**        .*::..*:***::* :* :.: :.* *.* ..*****:

Q9K5L9_A1_7__ile                         YTSGSTGTPKGVVVNHQAVNRLVKNTNYVQLTPDDRVAQAANIAFDAATFEIWGALLNGA
Q84BC7_A1_4__ile                         YTSGSTGQPKGVSIVHRSVVRLVKETNYISISADDVIAQASNHAFDAATFEIWGALLNGA
                                         ******* **** : *::* ****:***:.::.** :***:* *****************

Q9K5L9_A1_7__ile                         KLVMITKSVLLSPQEFAANIRDREVSVLFLTTALFNQLASFVPQAFSSLRYLLFGGEAVD
Q84BC7_A1_4__ile                         RLVGVSKDLALSPRDFAVFMRSQSISVLFLTTALFNQIAQEVPSAFNSLRHLLFGGEAVD
                                         :** ::*.: ***::**. :*.:.:************:*. **.**.***:*********

Q9K5L9_A1_7__ile                         PQWVQEVLEKGAPKQLLHVYGPTENTTFSSWYLVEELTTIATTIPIGRAISNTQIYLLDQ
Q84BC7_A1_4__ile                         PKWVKEVLNNGAPQRLLHVYGPTENTTFSSWYLVQDVPEGATTIPIGQPISNTQIYLLDS
                                         *:**:***::***::*******************:::.  *******:.**********.

Q9K5L9_A1_7__ile                         NLQPVPVGVPGELHVGGAGLARGYLNRPELTQEKFIPNPFDNS-----------------
Q84BC7_A1_4__ile                         QLQPVGIGVPGELYIGGDGLAREYLNRTELTQEKLIQNPFGGSRGAGEQGSKGAEEQSFP
                                         :**** :******::** **** ****.******:* ***..*                 

Q9K5L9_A1_7__ile                         -----KLYKTGDLARYLPDGNIEYLGRIDHQVKIRGFRIELGEIEAVLSQHEDVQISCVI
Q84BC7_A1_4__ile                         SASSERLYKTGDKARYLSDGNIEYLGRIDDQVKIRGLRIELGEIEAVLSQHSDVQVSCVI
                                              :****** ****.***********.******:**************.***:****

Q9K5L9_A1_7__ile                         VREDTPGETCTERSRSKQLVAYIVPQKDVTLTT
Q84BC7_A1_4__ile                         VREDTPGD--------KRLVAYIVTHQDCQPTM
                                         *******:        *:******.::*   * 
