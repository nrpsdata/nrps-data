CLUSTAL W multiple sequence alignment


O87314_A1_orn                            -----------------------------------------EHPAYLIYTSGSTGRPKGV
O87314_A3_orn                            SPRCRCHRARRRGNRCHTRRYARAPADRRRTRGFRVRQHAPEHPAYLIYTSGSTGRPKGV
                                                                                  *******************

O87314_A1_orn                            LTGYAGLTNMYFNHREAIFAPTVARAGSAEQLRIAHTVSFSFDMSWEELFWLVEGHQVHV
O87314_A3_orn                            LTGYAGLTNMYFNHREAIFAPTVARAGSAEQLRIAHTVSFSFDMSWEELFWLVEGHQVHV
                                         ************************************************************

O87314_A1_orn                            CDEELRRDAPALVAYCHRHRIDVINVTPTYAHHLFDAGLLDDGAHTPPLVLLGGEAVGDG
O87314_A3_orn                            CDEELRRDAPALVAYCHRHRIDVINVTPTYAHHLFDAGLLDDGAHTPPLVLLGGEAVGDG
                                         ************************************************************

O87314_A1_orn                            VWSALRDHPDSAGYNLYGPTEYTINTLGGGTDDSDTPTVGQPIWNTRGYILDAALRPVPD
O87314_A3_orn                            VWSALRDHPDSAGYNLYGPTEYTINTLGGGTDDSDTPTVGQPIWNTRGYILDAALRPVPD
                                         ************************************************************

O87314_A1_orn                            GAVGELYIAGTGLALGYHRRAGLTAATMVADPYVPGGRMYRTGDLVRRRPGSAELLDYLG
O87314_A3_orn                            GAVGELYIAGTGLALGYHRRAGLTAATMVADPYVPGGRMYRTGDLVRRRPGSAELLDYLG
                                         ************************************************************

O87314_A1_orn                            RVDDQVKIRGYRVELGEIESVLTRADGVARCAVVARATGANPPVKTLAAYVIPDRWPAED
O87314_A3_orn                            RVDDQVKIRGYRVELGEIESVLTRADGVARCAVVARATGANPPVKTLAAYVIPDRWPAED
                                         ************************************************************
