CLUSTAL W multiple sequence alignment


Q51338_A2_thr                            VRLLLTQGHLLERLPRQAGVEVLAIDGLVLDGYAESDPLPTLSADNLAYVIYTSGSTGKP
Q51338_A1_thr                            VRLLLTQGHLLERLPRQAGVEVLAIDGLVLDGYAESDPLPTLSADNLAYVIYTSGSTGKP
                                         ************************************************************

Q51338_A2_thr                            KGTLLTHRNALRLFSATEAWFGFDERDVWTLFHSYAFDFSVWEIFGALLYGGRLVIVPQW
Q51338_A1_thr                            KGTLLTHRNALRLFSATEAWFGFDERDVWTLFHSYAFDFSVWEIFGALLYGGCLVIVPQW
                                         **************************************************** *******

Q51338_A2_thr                            VSRSPEDFYRLLCREGVTVLNQTPSAFKQLMAVACSADMATQQPALRYVIFGGEALDLQS
Q51338_A1_thr                            VSRSPEDFYRLLCREGVTVLNQTPSAFKQLMAVACSADMATQQPALRYVIFGGEALDLQS
                                         ************************************************************

Q51338_A2_thr                            LRPWFQRFGDRQPQLVNMYGITETTVHVTYRPVSEADLKGGLVSPIGGTIPDLSWYILDR
Q51338_A1_thr                            LRPWFQRFGDRQPQLVNMYGITETTVHVTYRPVSEADLEGGLVSPIGGTIPDLSWYILDR
                                         **************************************:*********************

Q51338_A2_thr                            DLNPVPRGAVGELYIGRAGLARGYLRRPGLSATRFVPNPFPGGAGERLYRTGDLARFQAD
Q51338_A1_thr                            DLNPVPRGAVGELYIGRAGLARGYLRRPGLSATRFVPNPFPGGAGERLYRTGDLARFQAD
                                         ************************************************************

Q51338_A2_thr                            GNIEYIGRIDHQVKVRGFRIELGEIEAALAGLAGVRDAVVLAHDGVGGTQLVGYVVADSA
Q51338_A1_thr                            GNIEYIGRIDHQVKVRGFRIELGEIEAALAGLAGVRDAVVLAHDGVGGTQLVGYVVADSA
                                         ************************************************************

Q51338_A2_thr                            EDAER
Q51338_A1_thr                            EDAER
                                         *****
