CLUSTAL W multiple sequence alignment


Q7WRQ4_A2_9__asp-me                      PTMIVVDSENLDIIKPRLALLPKPPQILVINAHEIQQYYQWDGTNYQCLLVEANHDKKVL
Q847C7_A2_7__asp-me                      PSIVVTAAEYLDVVKSRLAALSKPPHILVITANEITQRYEWNGTNYQSFSVVESENPKTL
Q8G982_A2_9__asp-me                      PAIILIAAENLEMIKPRLLVLAKPPNLLVVNHQEIQQYYEWNGTNYQEFSIVENNNRKDL
Q9S1A8_A2_9__asp-me                      PEIILVAAENLEGIKPQLSALEKPPHILVVKAHKIQQYHQWNGMDYQEFPCQLSKLQPLL
                                         * :::  :* *: :*.:*  * ***::**:. ::* * ::*:* :** :    ..    *

Q7WRQ4_A2_9__asp-me                      IMPDADDSNYIMFTSGSTGEPKAILGSHGSLRHFINWEKLEFGINSNWRCLQIAQINFDA
Q847C7_A2_7__asp-me                      LLPDADDSNYIMFTSGSTGEPKAILGSHGSLRHFINWEKIEFGINQNWRCLQIAQINFDA
Q8G982_A2_9__asp-me                      LMPDADDANYIIFTSGSTGEPKAILGSHGSLRHFINWEKIEFGINHNWRCLQIAQINFDP
Q9S1A8_A2_9__asp-me                      AMPDADDSNYIMFTSGSTGEPKAILGSHGSLRHFIDWEKREFGINESWHCLQIAQINFDA
                                          :*****:***:***********************:*** ***** .*:**********.

Q7WRQ4_A2_9__asp-me                      YLRETLVTLCSGGTLYIPDSTDREDLEKLLLRLGEWQINLLHTVPSVMRLFLKIGRNLTN
Q847C7_A2_7__asp-me                      YLRETLVTLCSGGTLYIPDSTDREDLEGLLLRLGEWQINLLHTVPSVMRLFLKIGENLAH
Q8G982_A2_9__asp-me                      YLRETLVTLCSGGTLYIPDSIDREDLERLLLRLGEWQINLLHTVPSVMRLFLNIGRNLPN
Q9S1A8_A2_9__asp-me                      YLRETCVTLCSGGTLYIPESTEREDLELLLLRIGEWEINLLHTVPSVMRLFLKIGRGLVN
                                         ***** ************:* :***** ****:***:***************:**..* :

Q7WRQ4_A2_9__asp-me                      ADQLLKNLRVLVLGGEPLFVKELREWHQVFGSQTEFVNIYGASETTFVKHFHRIPDPNKI
Q847C7_A2_7__asp-me                      ADQLLKNLQVLVLGGEPLFVKELCEWHKVFGEQTEFVNIYGASETTFVKHFYRIPDPSKI
Q8G982_A2_9__asp-me                      ANQLLKNLQVLVLGGEPLFVKELCEWHEVFGNQTEFVNIYGASETTFIKHFHRIPKPNNI
Q9S1A8_A2_9__asp-me                      AHNLLKSLRIFVLGGEPLFVKELAEWHQIFGSQTEFVNIYGASETTFVKHFYRIPNPNNI
                                         *.:***.*:::************ ***::**.***************:***:***.*.:*

Q7WRQ4_A2_9__asp-me                      TYARVPGGKTLPEAAFAVIDGTRPCAVGEVGEIFVKSPYLTKGYYQDQRLTNSVFIPNPL
Q847C7_A2_7__asp-me                      TYARVPAGKTLSDAAFAVINGTRPCAVGEVGEIFVKSPYLTKGYYQDQKLTNLVFVPNPL
Q8G982_A2_9__asp-me                      SYARVPGGKTLPDAAFAVIDENRPCAIGEVGEIFVKSPYLTKGYYQDEILTNSVFVPNPL
Q9S1A8_A2_9__asp-me                      PYERVPGGQTLPDAAYAVVDGNRARAIGEVGEVFVKSPYLTKGYYQDESLTHSVFVPNPL
                                         .* ***.*:**.:**:**:: .*. *:*****:**************: **: **:****

Q7WRQ4_A2_9__asp-me                      NHGADIVYCTGDLGRLLPDLTVEVIG-RSDKQVKLNGVRIELGEIEDALAAIDGVEKALV
Q847C7_A2_7__asp-me                      NNGSDLVYRTGDLGRLLPDMSLEVVG-RSDNQIKLNGVRIELGEIENAIYAIDGVEKTLV
Q8G982_A2_9__asp-me                      NNGNDLVYRTGDLGRLLPDLTLEVIGRRSDNQIKLNGVRIELGEIEDAVAAIDGVQKALV
Q9S1A8_A2_9__asp-me                      NGGRDIVYRTGDLGRLLPDLTLEVIG-RSDNQIKLNGVRIELGEIEDVLSGIEGVEKALV
                                         * * *:** **********:::**:* ***:*:*************:.: .*:**:*:**

Q7WRQ4_A2_9__asp-me                      IAEEKEELVTVIAYYQAKNAINHQ
Q847C7_A2_7__asp-me                      VAEKKEELVTVIAYYQGNDTANQE
Q8G982_A2_9__asp-me                      IADKKEELVTVIAYYQGNNTVNRE
Q9S1A8_A2_9__asp-me                      MANKKEELVTVIAYYQAEDTVHQE
                                         :*::************.::: :::
