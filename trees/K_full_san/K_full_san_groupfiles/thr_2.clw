CLUSTAL W multiple sequence alignment


Q70K00_A2_thr                            VSILLLQHHLLEGTDYQSHTVFLDDPSSYGAEASNLKLNVMPNQLAYVIYTSGTTGNPKG
Q6YK39_A2_thr                            VSILLLQHHLLEGTDYQSHTVFLDDPSSYGAETSNLNLNVMPNQLAYVIYTSGTTGNPKG
                                         ********************************:***:***********************

Q70K00_A2_thr                            TLIEHKNVVRLLFNNKNVFDFNASDTWTLFHSFCFDFSVWEMYGALLYGGKLVIIPKQIA
Q6YK39_A2_thr                            TLIEHKNVVRLLFNNKNVFDFNASDTWTLFHSFCFDFSVWEMYGALLYGGKLVIVPKQIA
                                         ******************************************************:*****

Q70K00_A2_thr                            KNPERYLQLLKSEAVTILNQTPSYFYQLMQEERADPESNLNIRKIIFGGEALNPSFLKDW
Q6YK39_A2_thr                            KNPERYLQLLKSEAVTILNQTPSYFYQLMQEERADPESNLNIRKIIFGGEALNPSFLKDW
                                         ************************************************************

Q70K00_A2_thr                            KLKYPLTQLINMYGITETTVHVTYKEITEREIDEGRSNIGQPIPTLQAYILDEYQRIQVM
Q6YK39_A2_thr                            KLKYPLTQLINMYGITETTVHVTYKEITEREIDEGRSNIGQPIPTLQAYILDEYQHIQVM
                                         *******************************************************:****

Q70K00_A2_thr                            GIPGELYVAGEGLARGYLNRPELTGEKFVEHPFAAGEKMYKTGDVARWLPDGNIEYLGRI
Q6YK39_A2_thr                            GIPGELYVAGEGLARGYLNRPELTAEKFVEHPFAAGEKMYKTGDVARWLPDGNIEYLGRI
                                         ************************.***********************************

Q70K00_A2_thr                            DHQVKIRGYRIEIGEVEAALLQLESVKEAVVIAIEEEGSKQLCAYLSGDDSLNTA
Q6YK39_A2_thr                            DHQVKIRGYRIEIGEVEAALLQSESVKEAVVIAIEEEGSKQLCAYLSGDDSLNTA
                                         ********************** ********************************
