CLUSTAL W multiple sequence alignment


Q84BQ4_A4_10__ile                        AAALLTLSREDIDYAAPRIDLDRLILSGQPTHNPNLLQSSEALAYIMYTSGSTGTPKGVM
Q84BQ4_A3_9__ile                         AVALLTLSSEAIDYAAPRIDLDRLKLSGQSTHNPNLAQSSDALAYIMYTSGSTGTPKGVM
                                         *.****** * ************* ****.****** ***:*******************

Q84BQ4_A4_10__ile                        VPHRAIGRLVLNNGYADFNAQDRVVFASNPAFDASTMDIWGPLLNGGRVVVIDHQTLLDP
Q84BQ4_A3_9__ile                         VPHRGIARLVLNNGYADFNRQDRVAFASNPAFDASTMDIWGPLLNGGRVVVIDHQTLLDP
                                         ****.*.************ ****.***********************************

Q84BQ4_A4_10__ile                        NAFGHELSASRATVLFVTTALFNQYVQLIPQALKGLRILLCGGERGDPAAFRRLLAEAPK
Q84BQ4_A3_9__ile                         NAFGRELSASGATILFVTTALFNQYVQLIPQALKGLRMVLCGGERGDPTSFRRLRAEAPQ
                                         ****:***** **:***********************::*********::**** ****:

Q84BQ4_A4_10__ile                        LRIVHCYGPTETTTYATTFEVREVAENAESVPIGGPISNTQVYVLDAHQQPVPMGVTGEL
Q84BQ4_A3_9__ile                         LRIVHCYGPTETTTYATTFEVHEVAENAESVPIGAPISNTQVYVLDAHQQPVPMGVTGEL
                                         *********************:************.*************************

Q84BQ4_A4_10__ile                        YIGGQGVALGYLNRADLTAEKFLRDPFSDQPGALLYRTGDLARWLAPGQLDCIGRNDDQV
Q84BQ4_A3_9__ile                         YIGGQGVALGYLNRADLTAEKFLPDPFSDRPGALLYRTGDLVRWLAPGQLDCIGRNDDQV
                                         *********************** *****:***********.******************

Q84BQ4_A4_10__ile                        KIRGFRIELGEIENRLLNCQGIKEAVVLARRDGQDITRLVAYYTAHAGRLDSA
Q84BQ4_A3_9__ile                         KIRGFRIELGEIENRLLSYPGINEAVVLARRDGQEPLRLVAYYTAHDGTLELA
                                         *****************.  **:***********:  ********* * *: *
