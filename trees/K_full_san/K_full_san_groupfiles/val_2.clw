CLUSTAL W multiple sequence alignment


Q09164_A4_val                            NKLILLGANVTPPKLQEAAIDFVPIRDTFTTLTDGTLQDG--PTIE---RPSAQSLAYAM
Q09164_A9_val                            NRLILLGSDTQAVKLHANSV-------RFTRISDALVESGSPPTEELSTRPTAQSLAYVM
                                         *:*****::. . **:  ::        ** ::*. ::.*  ** *   **:******.*

Q09164_A4_val                            FTSGSTGRPKGVMVQHRNIVRLVKNSNVVAKQPAAARIAHISNLAFDASSWEIYAPLLNG
Q09164_A9_val                            FTSGSTGVPKGVMVEHRGITRLVKNSNVVAKQPAAAAIAHLSNIAFDASSWEIYAPLLNG
                                         ******* ******:**.*.**************** ***:**:****************

Q09164_A4_val                            GAIVCADYFTTIDPQALQETFQEHEIRGAMLPPSLLKQCLVQAPDMISRLDILFAAGDRF
Q09164_A9_val                            GTVVCIDYYTTIDIKALEAVFKQHHIRGAMLPPALLKQCLVSAPTMISSLEILFAAGDRL
                                         *::** **:**** :**: .*::*.********:*******.** *** *:********:

Q09164_A4_val                            SSVDALQAQRLVGSGVFNAYGPTENTILSTIYNVAENDSFVNGVPIGSAVSNSGAYIMDK
Q09164_A9_val                            SSQDAILARRAVGSGVYNAYGPTENTVLSTIHNIGENEAFSNGVPIGNAVSNSGAFVMDQ
                                         ** **: *:* *****:*********:****:*:.**::* ******.*******::**:

Q09164_A4_val                            NQQLVPAGVMGELVVTGDGLARGYMDPKLDADRFIQLTVNGSEQVRAYRTGDRVRYRPKD
Q09164_A9_val                            NQQLVSAGVIGELVVTGDGLARGYTDSKLRVDRFIYITLDGN-RVRAYRTGDRVRHRPKD
                                         *****.***:************** *.** .**** :*::*. :***********:****

Q09164_A4_val                            FQIEFFGRMDQQIKIRGHRIEPAEVEQAFLNDGFVEDVAIVIRTPENQEPEMVAFVTAKG
Q09164_A9_val                            GQIEFFGRMDQQIKIRGHRIEPAEVEQALARDPAISDSAVITQLTDEEEPELVAFFSLKG
                                          ***************************: .*  :.* *:: : .:::***:***.: **

Q09164_A4_val                            DNSARE
Q09164_A9_val                            NANGTN
                                         : .. :
