CLUSTAL W multiple sequence alignment


Q8Z8L1_A1_dhb|sal                        APTLVIADRQHTLFAGEDFLNRFVAEHRSARVVLLRNDDGDHSLDAAMRQAAEGFTATPS
P10378/2506183_A1_dhb|sal                EPALLIADRQHALFSGDDFLNTFVTEHSSIRVVQLLNDSGEHNLQDAINHPAEDFTATPS
                                          *:*:******:**:*:**** **:** * *** * **.*:*.*: *:.:.**.******

Q8Z8L1_A1_dhb|sal                        PADEVAYFQLSGGTTGTPKLIPRTHNDYYYSVRRSNEICGFNEETRFLCAIPAAHNYAMS
P10378/2506183_A1_dhb|sal                PADEVAYFQLSGGTTGTPKLIPRTHNDYYYSVRRSVEICQFTQQTRYLCAIPAAHNYAMS
                                         *********************************** *** *.::**:*************

Q8Z8L1_A1_dhb|sal                        SPGALGVFLAKGTVVLATDPGATLCFPLIEKHQINATALVPPAVSLWLQAIQEWGGNAPL
P10378/2506183_A1_dhb|sal                SPGSLGVFLAGGTVVLAADPSATLCFPLIEKHQVNVTALVPPAVSLWLQALIEGESRAQL
                                         ***:****** ******:**.************:*.**************: *  ..* *

Q8Z8L1_A1_dhb|sal                        ASLRLLQVGGARLSATLAARIPAEIGCQLQQVFGMAEGLVNYTRLDDSPERIINTQGRPM
P10378/2506183_A1_dhb|sal                ASLKLLQVGGARLSATLAARIPAEIGCQLQQVFGMAEGLVNYTRLDDSAEKIIHTQGYPM
                                         ***:********************************************.*:**:*** **

Q8Z8L1_A1_dhb|sal                        CPDDEVWVADADGNPLPPGEIGRLMTRGPYTFRGYFNSPQHNVSAFDANGFYCSGDLISI
P10378/2506183_A1_dhb|sal                CPDDEVWVADAEGNPLPQGEVGRLMTRGPYTFRGYYKSPQHNASAFDANGFYCSGDLISI
                                         ***********:***** **:**************::*****.*****************

Q8Z8L1_A1_dhb|sal                        DQDGYITVHGREKDQINRGGEKIAAEEIENLLLRHPAVIHAALVSMEDELLGEKSCAYLV
P10378/2506183_A1_dhb|sal                DPEGYITVQGREKDQINRGGEKIAAEEIENLLLRHPAVIYAALVSMEDELMGEKSCAYLV
                                         * :*****:******************************:**********:*********

Q8Z8L1_A1_dhb|sal                        VKEPLRAVQ
P10378/2506183_A1_dhb|sal                VKEPLRAVQ
                                         *********
