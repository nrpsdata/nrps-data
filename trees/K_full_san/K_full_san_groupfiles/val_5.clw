CLUSTAL W multiple sequence alignment


Q2VQ15_A3_6__val                         TKLLFVQKTEMIPASYQGEVLLLAEECWMHEDSSNLELINKTQDLAYVMYTSGSTGKPKG
Q2VQ15_A2_5__val                         AQLLIVQEAAMIPEGYQGKVLLLAEECWMQEEASNLELINDAQDLAYVMYTSGSTGKPKG
                                         ::**:**:: *** .***:**********:*::*******.:******************

Q2VQ15_A3_6__val                         NLTTHQNILTTIINNGYIEIAPTDRLLQLSNYAFDGSTFDIYSALLNGATLVLVPKEVML
Q2VQ15_A2_5__val                         NLTTHQNILRTIINNGFIEIVPADRLLQLSNYAFDGSTFDIYSALLNGATLVLVPKEVML
                                         ********* ******:***.*:*************************************

Q2VQ15_A3_6__val                         NPMELAKIVREQDITVSFMTTSLFHTLVELDVTSMKSMRKVVFGGEKASYKHVEKALDYL
Q2VQ15_A2_5__val                         NPMELARIVREQDITVSFMTTSLFHTLVELDVTSMKSIRKVVFGGEKASYKHVEKALDYL
                                         ******:******************************:**********************

Q2VQ15_A3_6__val                         GEGRLVNGYGPTETTVFATTYTVDSSIKETGIVPIGRPLNNTSVYVLNENNQLQPIGVPG
Q2VQ15_A2_5__val                         GEGRLVNGYGPTETTVFATTYTVDSSIKETGIVPIGRPLNNTSVYILNENNQPQPIGVPG
                                         *********************************************:****** *******

Q2VQ15_A3_6__val                         ELCVGGAGIARGYLNRPELTAERFVENPFVSGDRMYRTGDLARWLPDGSMEYLGRMDEQV
Q2VQ15_A2_5__val                         ELCVGGAGIARGYLNRPELTAERFVDNPFLVGDRMYRTGDMARFLPDGNIEYIGRMDEQV
                                         *************************:***: *********:**:****.:**:*******

Q2VQ15_A3_6__val                         KVRGYRIELGEIETRLLEHPSISAAVLLAKQDEQGHSYLCAYIVANGVWTVAE
Q2VQ15_A2_5__val                         KIRGHRIELGEIEKSLLEYPAISEAVLVAKRDEQGHSYLCAYVVSTDQWTVAK
                                         *:**:********. ***:*:** ***:**:***********:*:.. ****:
