CLUSTAL W multiple sequence alignment


O07944_A1_pro                            PAVVLTGEDTGQDLSGYDDTDLTDADRTAPLLPAHPAYVIYTSGSTGTPKAVVMPGAALV
O05647_A1_pro                            PALVLTEDDVDEDLSGIPDGNLTDAERTAPLTPAHPAYVIYTSGSTGRPKAVVMPGAAVV
                                         **:*** :*..:****  * :****:***** *************** **********:*

O07944_A1_pro                            NLLAWHRREIPGEAGAPVAQFTTIGFDVAAQEILATWLHGKTLAVPSQEVRRSAEQLAAW
O05647_A1_pro                            NLLAWHRREIPAGAGTTVAQFASLSFDVAAQEILSTLLYGATLAVPTDAVRRDADAFAAW
                                         ***********. **:.****:::.*********:* *:* *****:: ***.*: :***

O07944_A1_pro                            LDEQHVSELYAPNLVIEALAEAAAEAGRTLPALRHIAQAGEALTLTRTVREFAAAVPGRQ
O05647_A1_pro                            LEEYRVNELYAPNLVVEALAEAAAEQGRTLPDLRHIAQAGEALTAGPRVRDFCAALPGRR
                                         *:* :*.********:********* ***** ************    **:*.**:***:

O07944_A1_pro                            LHNHYGPAETHVMTGTALPEDPAAWSEHAPLGRPVSGARVYVLDSALRPVAPGVTGELYL
O05647_A1_pro                            LHNHYGPAETHVMTGIELPVDPGGWPERVPIGGPVDNARLYVLDGFLRPVPPGVVGELYL
                                         ***************  ** **..*.*:.*:* **..**:****. ****.***.*****

O07944_A1_pro                            AGAGVSRGYLNRPVLTAERFVADPYAPSPGARMYRTGDLGRWNTRGELEFAGRADHQVKI
O05647_A1_pro                            AGAGVARGYLNRPGLTAERFVADPFG-GPGTRMYRTGDLARWAGSGVLEFAGRADHQVKV
                                         *****:******* **********:. .**:********.**   * ************:

O07944_A1_pro                            RGFRIEPGEIEAALTDLPAVARAAVVVREDRPGDKRLVAY--AVPAGEGLDAA
O05647_A1_pro                            RGFRIEPGEVESVLAAQPGVARAVVLAREDRPGERRLVAYLVAVP-GSVPDPG
                                         *********:*:.*:  *.****.*:.******::*****  *** *.  *..
