CLUSTAL W multiple sequence alignment


Q8KLL3_A2_tyr                            PVLVVCDGKARDAVPEEFADRSLVIDEVDLSAVPDAELPRVGPDDVAYVIYTSGSTGTPK
Q70AZ9_A2_tyr                            PALVVCAGKTREAVPAAFADRLLVVDEMDLTGGSAARLPRVRPGDAAYVIYTSGSTGRPK
                                         *.**** **:*:***  **** **:**:**:. . *.**** *.*.*********** **

Q8KLL3_A2_tyr                            GVVVTHAGLGNLAAAQIDRFAVSPSSRVLQFAALGFDATVSEALMALLSGATLVMAPKQD
Q70AZ9_A2_tyr                            GVVVPHAGLGNLALAQIDRFGVSPSSRVLQFAALGFDAMVSEVLMALLSGARLVMAPEHQ
                                         ****.******** ******.***************** ***.******** *****:::

Q8KLL3_A2_tyr                            LPPRVSLAEALERWDVTHVTVPPSVLATADVLPESLETVVVAGEACPPGLADRWSEGRRL
Q70AZ9_A2_tyr                            LPPRVSLAEALQRWDVTHVTVPPSVLATAEALPARLETVVVAGEACPPSLADRWSAGLRL
                                         ***********:*****************:.**  *************.****** * **

Q8KLL3_A2_tyr                            INAYGPTEATVCAAMSMPLTAGRDVVPIGEPIAGSRCHVLDAFLRPLPPGVTGELYVSGI
Q70AZ9_A2_tyr                            VNAYGPTEATVCAAMSMPLVASRPVVPIGTPIAGGRCYVLDAFLRPLPPGLTGELYVAGI
                                         :******************.*.* ***** ****.**:************:******:**

Q8KLL3_A2_tyr                            GLARGYLGRAALTAERFVADPFVPGERMYRTGDLAHLTSSGELVFAGRADDQVKLRGFRI
Q70AZ9_A2_tyr                            GLARGYLGRAALTAERFVADPFVPGERMYRTGDLAYRTGEGELVFAGRADDQVKVRGFRI
                                         ***********************************: *..**************:*****

Q8KLL3_A2_tyr                            EPGEIESVLSGHPQVAQAAVTVRDDRLLAHVSPTEVDPH
Q70AZ9_A2_tyr                            EPGEVESALSGHPGVAQAAVIVRGDRLLAYVSPAGVDPQ
                                         ****:**.***** ****** **.*****:***: ***:
