CLUSTAL W multiple sequence alignment


Q93I55_A3_gln                            ISVLLYCGKLQDDIGFSGTCIDLMEEHFYHEKDSSLALSYQSSQLAYAIYTSGTT----G
Q9R9J0_A3_gln                            INILLTHGELPENLNFSGTCINMKEEQAYHETDINLAVPCQFDQLAYCIYTSGTTGTPKG
                                         *.:**  *:* :::.******:: **: ***.* .**:. * .****.*******    *

Q93I55_A3_gln                            KPKGTLIEHRQVIHLIEGLSRQVYSAYDAELNIAMLAPYYFDASVQQMYASLLSGHTLFI
Q9R9J0_A3_gln                            TPKGTLIEHRQVIHLIEGLRNAVYSAYDGVLHVAMLAPYYFDASVQQIYASLLLGHTLFI
                                         .****************** . ******. *::**************:***** ******

Q93I55_A3_gln                            VPKEIVSDGAALCRYYRQHSIDITDGTPAHLKLLIAAGDLQGVTLQHLLIGGEALSKTTV
Q9R9J0_A3_gln                            VPKEAVSDGEALCQYYRQHRIDVTDGTPAHLKLLVAADDGEGVPLRHLLIGGEALPKTTV
                                         **** **** ***:***** **:***********:**.* :**.*:*********.****

Q93I55_A3_gln                            NKLKQLFGEHGAAPGITNVYGPTETCVDASLFNIECSSDAWARSQNYVPIGKPLGRNRMY
Q9R9J0_A3_gln                            TKFIHLFGADRAAPAITNVYGPTETCVDASLFNIEVSADAWTRSQVHIPIGKPLGNNRMY
                                         .*: :*** . ***.******************** *:***:*** ::*******.****

Q93I55_A3_gln                            ILDSKKRLQPKGVQGELYIAGDGVGRGYLNLPELTDEKFVADPFVPEDRMYRTGDLARLL
Q9R9J0_A3_gln                            ILDSQQKLQPVGVQGELYIAGDGVGRGYLNLPELTNKKFVNDPFVPSGRMYRTGDLARLL
                                         ****:::*** ************************::*** *****..************

Q93I55_A3_gln                            PDGNIEYIGRIDHQVKIQGFRIELGEIESVMLNVPDIQEAAAAALKDADDEYYLCGYFAA
Q9R9J0_A3_gln                            PDGNIEFIERVDHQVKIHGFRIELGEIESIMLNIPEIQEAVASVLEDADGEHYICGYYVA
                                         ******:* *:******:***********:***:*:****.*:.*:***.*:*:***:.*

Q93I55_A3_gln                            DKTIQISE
Q9R9J0_A3_gln                            NKPFPTSQ
                                         :*.:  *:
