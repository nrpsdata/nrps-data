CLUSTAL W multiple sequence alignment


Q01886v1_A1_pro                          ATIALVGAGKTAALFKSADTAVQTIDITKDIPHGLSDTVVQSNTKIDDPAFGLFTSGSTG
Q01886v2_A1_pro                          ATIALVGAGKTAALFKSADTAVQTIDITKDIPHGLSDTVVQSNTKIDDPAFGLFTSGSTG
                                         ************************************************************

Q01886v1_A1_pro                          VPKCIVVTHSQICTAVQAYKDRFGVTSETRVLQFSSYTFDISIADTFTALFYGGTLCIPS
Q01886v2_A1_pro                          VPKCIVVTHSQICTAVQAYKDRFGVTSETRVLQFSSYTFDISIADTFTALFYGGTLCIPS
                                         ************************************************************

Q01886v1_A1_pro                          EEDRMSNLQDYMVSVRPNWAVLTPTVSRFLDPGVVKDFISTLIFTGEASREADTVPWIEA
Q01886v2_A1_pro                          EEDRMSNLQDYMVSVRPNWAVLTPTVSRFLDPGVVKDFISTLIFTGEASREADTVPWIEA
                                         ************************************************************

Q01886v1_A1_pro                          GVNLYNVYGPAENTLITTATRIRKGKSSNIGYGVNTRTWVTDVSGACLVPVGSIGELLIE
Q01886v2_A1_pro                          GVNLYNVYGPAENTLITTATRIRKGKSSNIGYGVNTRTWVTDVSGACLVPVGSIGELLIE
                                         ************************************************************

Q01886v1_A1_pro                          SGHLADKYLNRPDRTEAAFLSDLPWIPNYEGDSVRRGRRFYRTGDLVRYCDDGSLICVGR
Q01886v2_A1_pro                          SGHLADKYLNRPDRTEAAFLSDLPWIPNYEGDSVRRGRRFYRTGDLVRYCDDGSLICVGR
                                         ************************************************************

Q01886v1_A1_pro                          SDTQIKLAGQRVELGDVEAHLQSDPTTSQAAVVFPRSGPLEARLIALLVTGNK
Q01886v2_A1_pro                          SDTQIKLAGQRVELGDVEAHLQSDPTTSQAAVVFPRSGPLEARLIALLVTGNK
                                         *****************************************************
