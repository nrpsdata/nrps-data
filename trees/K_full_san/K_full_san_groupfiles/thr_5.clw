CLUSTAL W multiple sequence alignment


Q847C7_A1_6__thr                         VPILLTQKSLLPSLPENQAIVMCLDRDWGVIAACSQENIVSHAQPQNLAYVIYTSGSTGK
Q9K5M1_A3_5__thr                         VSVLLIQQHLVEKLQQHQAHIVCLDSDGEKIAQNSNSNPLNIATPSNLAYVIYTSGSTGK
                                         *.:** *: *: .* ::** ::*** *   **  *:.* :. * *.**************

Q847C7_A1_6__thr                         PKGVLINHQNVIRLFAATQAWYHFGASDVFTLFHSIAFDFSVWELWGALLYGGSLVIVPY
Q9K5M1_A3_5__thr                         PKGVLVNHSHVVRLFAATDSWYNFNSQDVWTMFHSYAFDFSVWEVWGALLYGGRLVVVGY
                                         *****:**.:*:******::**:*.:.**:*:*** ********:******** **:* *

Q847C7_A1_6__thr                         WVSRDPSAFHTLLRQEQVTVLNQTPSAFRQLIRVEE-LAKTGESQLSLRLVIFGGEALEP
Q9K5M1_A3_5__thr                         LVTRSPKSFYELLCQEKVTILNQTPSAFRQLIPAEQSIATVGD--LNLRLVIFGGETLEL
                                          *:*.*.:*: ** **:**:************ .*: :*..*:  *.*********:** 

Q847C7_A1_6__thr                         QSLQPWFEGYKDQSPQLVNMYGITETTVHVTYRPLSIADVNNSKSLIGVPIPDLQLYILD
Q9K5M1_A3_5__thr                         NSLQPWFDRHGDQSPQLVNMYGITETTVHVTYRPLSKADLHGKASVIGRPIGDLQVYVLD
                                         :******: : ************************* **::.. *:** ** ***:*:**

Q847C7_A1_6__thr                         EQLKPLPIGIKGEMYIGGAGLARGYLNRPELTAERFIPNPFSDAPEARLYKTGDLARYLE
Q9K5M1_A3_5__thr                         EHLQPVPIGVAGEMYVGGAGVTRGYLNRAELTAQRFISNPFNGNSEQLLYKSGDLARYLP
                                         *:*:*:***: ****:****::******.****:***.***.. .*  ***:******* 

Q847C7_A1_6__thr                         NGDIEYLDRIDNQVKIRGFRIELGEIEAALLKYPEVQEAVVMARTDQPGDKRLVAYIVAK
Q9K5M1_A3_5__thr                         NGELEYLGRIDNQVKIRGFRIELGEIEAALSQLREVREVVVVARSDQPDNKRLVAYVVPQ
                                         **::***.********************** :  **:*.**:**:***.:******:*.:

Q847C7_A1_6__thr                         SSNPASEN
Q9K5M1_A3_5__thr                         QKNLESSK
                                         ..*  *.:
