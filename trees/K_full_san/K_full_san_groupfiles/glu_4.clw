CLUSTAL W multiple sequence alignment


P94459_A2_glu                            TNILLLQSAGLHVPEFTGEIVYLNQTNSGLAHRLSNPNVDVLPQSLAYVIYTSGSTGMPK
O30980_A2_glu                            SDILLIQSDNLDIPAFDGEIVHLSQLNSGLKRRLSNPNVEVYPDSLAYMIYTSGSTGRPK
                                         ::***:** .*.:* * ****:*.* **** :*******:* *:****:******** **

P94459_A2_glu                            GVEIEHRSAVNFLNSLQSRYQLKHSDMIMHKTSYSFDASIWELFWWPYAGASVYLLPQGG
O30980_A2_glu                            GVQVEHQSAVNFLNSLQFRYPLNQSDVILHKTSYSFDASIWELFWWPYGGASVYLLPQGG
                                         **::**:********** ** *::**:*:*******************.***********

P94459_A2_glu                            EKEPEVIAKAIEEQKITAMHFVPSMLHAFLEQIKYRSVPIKTNRLKRVFSGGEQLGTHLV
O30980_A2_glu                            EKEPDMILKVIEEQQITAMHFVPSMLHAFLEYLKNGPVPIKTNRLKRVFSGGEQLGAHLV
                                         ****::* *.****:**************** :*  .*******************:***

P94459_A2_glu                            SRFYELLPNVSITNSYGPTEATVEAAFFDCPPHEKLERIPIGKPVHHVRLYLLNQNQRML
O30980_A2_glu                            SRFCELLPDVTLTNSYGPTEATVEAAFFDCPLDEKLDRIPIGKPIHHVRLYILNQKQKML
                                         *** ****:*::******************* .***:*******:******:***:*:**

P94459_A2_glu                            PVGCIGELYIAGAGVARGYLNRPALTEERFLEDPFYPGERMYKTGDVARWLPDGNVEFLG
O30980_A2_glu                            PAGCIGELYIAGAGVARGYLNRPELTEERFLDDPFYPGERMYKTGDLARWLPDGQVEFLG
                                         *.********************* *******:**************:*******:*****

P94459_A2_glu                            RTDDQVKIRGYRIEPGEIEAALRSIEGVREAAVTVRTDSGEPELCAYVEGLQRNE
O30980_A2_glu                            RLDDQVKIRGYRIEPGEIEAALRSIEGVREAAVTVRTESGEAELCAYAEGLGRNE
                                         * ***********************************:***.*****.*** ***
