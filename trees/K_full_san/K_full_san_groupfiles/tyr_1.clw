CLUSTAL W multiple sequence alignment


P94459_A3_tyr                            TKLLMTINEADLGVLADFEGEILTIESVEEDDKSPLPQMSSAHHLAYIIYTSGTTGRPKG
P39846_A1_tyr                            AAQLLIEEDLISLIPPSYEGNTITIEHTESY-QTEAPNMP-PGDLAYLIYTSGTTGRPKG
O87704_A1_3__tyr                         AALLLTEEELISSIPSGYEGSIVTSGHTTHY-RTDSPDAS-IGELAYLIYTSGTTGRPKG
O30980_A3_9__tyr                         TKLLITLPAADQEALEDFEGEIFAIENAETYDGSPLPQVSRPQHLAYIIYTSGTTGQPKG
O30980_A3_tyr                            TKLLITLPAADQEALEDFEGEIFAIENAETYDGSPLPQVSRPQHLAYIIYTSGTTGQPKG
                                         :  *:           .:**. .:   .     :  *: .   .***:********:***

P94459_A3_tyr                            VMVEHKGIANTLQWRRNAYAFNETDTILQLFSFSFDGFITSMFTPLLSGAKAVLLHEEEA
P39846_A1_tyr                            VLVDHHGIANTLQWRREEYSMTEQDISLHLFSYVFDGCVTSLFTPLLSGACVLLTTDDEA
O87704_A1_3__tyr                         VLVDHQGIANTLQWRREEYGMSEGDTALHLFSYVFDGCVTSLFTPLLSGACVLLTTDNEA
O30980_A3_9__tyr                         VMVEHKGIANTLQWRRNAYTLNETDTVLQLFSFSFDGFLTSMFTPLLSGAKTVLPDENEA
O30980_A3_tyr                            VMVEHKGIANTLQWRRNAYTLNETDTVLQLFSFSFDGFLTSMFTPLLSGAKTVLPDENEA
                                         *:*:*:**********: * :.* *  *:***: *** :**:******** .:*  ::**

P94459_A3_tyr                            KDILAIKHQLSRQRITHMIIVPVLYRALLDVVQPEDVKTLRVVTLAGEAADRELIARSLA
P39846_A1_tyr                            KDVLALKRKIARYKVSHMIIVPSLYRVLLEVMTADDAKSLRIVTFAGEAVTPDLLELNQI
O87704_A1_3__tyr                         KDVLALRRKIAQYKVSHMLIVPSLYRVLLEVLTAEDAKSLRIVTFAGEAVTPDLLETSRK
O30980_A3_9__tyr                         KDILSIKHDLSHHRITHMIIVPVLYRALLDVIQPEDAKTLRVVTLAGEAADRELIDRSMA
O30980_A3_tyr                            KDILSIKHDLSHHRITHMIIVPVLYRALLDVIQPEDAKTLRVVTLAGEAADRELIDRSMA
                                         **:*::::.::: :::**:*** ***.**:*: .:*.*:**:**:****.  :*:  .  

P94459_A3_tyr                            ICPHTELANEYGPTENSVATTVMRHMEKQAYVSIGQPIDGTQVLILNSNHQLQPIGVAGE
P39846_A1_tyr                            ICPSAELANEYGPTENSVATTILRHLNKKERITIGHPIRNTKVFVLHGN-QMQPIGAAGE
O87704_A1_3__tyr                         ICPSAELANEYGPTENSVATTILRHLNEEERITIGHPIANTKVYILQGE-QLQPIGAAGE
O30980_A3_9__tyr                         ICPNTELANEYGPTENSVATTAMRHMERQKNVCIGRPIDNTEVLILNGD-QLQPIGVAGE
O30980_A3_tyr                            ICPNTELANEYGPTENSVATTAMRHMERQKNVCIGRPIDNTEVLILNGD-QLQPIGVAGE
                                         *** :**************** :**::.:  : **:** .*:* :*:.: *:****.***

P94459_A3_tyr                            LCIAGTGLARGYVNLPELTERAFTQNPFKPEARMYRTGDAARWMADGTLEYLGRIDDQVK
P39846_A1_tyr                            LCISGAGLARGYYKQQELTQKAFSDHPFLEGERLYRTGDAGRFLPDGTIEYIGRFDDQVK
O87704_A1_3__tyr                         LCISGAGLARGYYKRTELTEKAFTDHPFLKGERLYRTGDAGRFLPDGTIEYIGRFDDQVK
O30980_A3_9__tyr                         LCIAGTGLARGYVNLPELTAKTFVQHPYQPEKRMYRTGDAARWMADGTIEYLGRMDDQVK
O30980_A3_tyr                            LCIAGTGLARGYVNLPELTAKTFVQHPYQPEKRMYRTGDAARWMADGTIEYLGRMDDQVK
                                         ***:*:****** :  *** ::* ::*:    *:******.*::.***:**:**:*****

P94459_A3_tyr                            IRGYRVETKEIESVIRCIKGVKDAAVVAHVTASGQTELSAYVVTKPGLSTNA
P39846_A1_tyr                            IRGYRIELREIETVLRQAPGVKEAAVLARDVSAEEKELVAYIVPEKGNSLPD
O87704_A1_3__tyr                         IRGYRIELSEIETVLRQASGVKEAAVLARDVSDEEKELVAYIVPEKGNGLPN
O30980_A3_9__tyr                         IRGHRVETKEIESVIRRISGVKEAVVLARETAPAQTELCAYIVAEQDFNTEM
O30980_A3_tyr                            IRGHRVETKEIESVIRRISGVKEAVVLARETAPAQTELCAYIVAEQDFNTEM
                                         ***:*:*  ***:*:*   ***:*.*:*: .:  :.** **:*.: . .   
