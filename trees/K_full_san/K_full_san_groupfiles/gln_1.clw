CLUSTAL W multiple sequence alignment


Q8NJX1_A6_gln                            AEIMIVSPSSSVPCEGLTSIMVEFTIELLEQLSSRYDAFQEILPKAEPSNAAYVLFTSGS
Q8NJX1_A17_gln                           ARVLIASSDAVASCAGMAEHVVELSPSVMARLAT--SVTLKILPKVGPRNTAYILFTSGS
                                         *.::*.*..: ..* *::. :**:: .:: :*::  ..  :****. * *:**:******

Q8NJX1_A6_gln                            TGKPKGVLMEHSAFATSTLGHGGIYNLSPASRVFQFSNYIFDGSLGEIFTTLSFGGTVCV
Q8NJX1_A17_gln                           TGKPKGVVMQHGSFSSTTIGYGKVYNLSPLSRIFQFSNYIFDGSLGEIFGPLAFGGTICI
                                         *******:*:*.:*:::*:*:* :***** **:**************** .*:****:*:

Q8NJX1_A6_gln                            PSEDERLQKAPSFMREARVNTAMLTPSFVRTFAPEQVPSLRLLVLGGEPSSKDLLETWCG
Q8NJX1_A17_gln                           PSDDERLQCAPDFMHRAKVNTAMLTPSFVRTFTPDKVPHLKTLVLGGEAASKSTLEMWVD
                                         **:***** **.**:.*:**************:*::** *: ******.:**. ** * .

Q8NJX1_A6_gln                            RLRLVNGYGPAEACNYATTHDFKPT-DSPHTIGRGFNSACWIVDPTDYNKLTPIGCIGEL
Q8NJX1_A17_gln                           RVTLFNGYGPAEACNYATTHIFKSSAESPRLIGSSFNGACWVVEPSNHNKLTPIGCTGEL
                                         *: *.*************** **.: :**: ** .**.***:*:*:::******** ***

Q8NJX1_A6_gln                            IIQGNALARGYINDADRTKNSFITNVDCLPKSIISGPHRFYLTGDLVRYTPDGQLEYLGR
Q8NJX1_A17_gln                           VLQGHALARGYLNDKMKTEESFVCEIGSLPSSLLHEPKRFYLTGDLVRYNSNGELEYLGR
                                         ::**:******:**  :*::**: ::..**.*::  *:***********..:*:******

Q8NJX1_A6_gln                            KDTQVKLRGQRLELGEIEYHVKKSLANIEHVAVDVAHRETGDTLIAFVSFKEKMATT-
Q8NJX1_A17_gln                           KDSQVKLRGQRLELGEIEYNITQSLSSVRHVAVDVMHRQAGDSLVAFISFSGHANTKW
                                         **:****************::.:**:.:.****** **::**:*:**:**. :  *. 
