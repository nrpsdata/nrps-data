CLUSTAL W multiple sequence alignment


Q6VT93_A1_16__arg                        ISLLLSELEL-----CNSFLSDLGTIECVCLAIDAPGWEPEEGELPVPPVIEGRQPAYVI
Q5DIV7_A2_arg                            ARWLISQETLAERLPCPA--------EVERLPLETAAWPASADTRPLPEV-AGETLAYVI
                                            *:*:  *     * :        *   *.:::..* .. .  *:* *  *.  ****

Q6VT93_A1_16__arg                        YTSGSTGQPKGVIISHDSISHHCQVIRDYYRITAQDVILQFAPMNVDAALEQLLPGLISG
Q5DIV7_A2_arg                            YTSGSTGQPKGVAVSQAALVAHCQAAARTYGVGPGDCQLQFASISFDAAAEQLFVPLLAG
                                         ************ :*: ::  ***.    * : . *  ****.:..*** ***:  *::*

Q6VT93_A1_16__arg                        ATVVIRSEPLWSPDILCRKVVELGISVLDLPPSYLYELLLEIRDVAGWSRPPSLRLVISG
Q5DIV7_A2_arg                            ARVLLGDAGQWSAQHLADEVERHAVTILDLPPAYLQQQAEELRHA---GRRIAVRACILG
                                         * *:: .   **.: *. :* . .:::*****:** :   *:*..   .*  ::*  * *

Q6VT93_A1_16__arg                        GEALSPETLSLWCGCALSECRLVNAYGPTETTITSTVYEIESRARTFTRLPESVPIGRPL
Q5DIV7_A2_arg                            GEAWDA---SLLTQQAVQAEAWFNAYGPTEAVITPLAWHCR------TQEGGAPAIGRAL
                                         *** ..   **    *:.    .*******:.**. .:. .      *:   : .***.*

Q6VT93_A1_16__arg                        PGESAYILDTQRRPLPVGVPGELYIGGAGVAIGYLNRPELTASTFVENPFM-AGTRLYKT
Q5DIV7_A2_arg                            GARRACILDAALQPCAPGMIGELYIGGQCLARGYLGRPGQTAERFVADPFSGSGERLYRT
                                          .. * ***:  :* . *: *******  :* ***.**  **. ** :**  :* ***:*

Q6VT93_A1_16__arg                        GDAARWLADGNIALLGRLDQQVKIRGFRVECGEIEAALQALDVVKHVAVLAQPTQGSHRL
Q5DIV7_A2_arg                            GDLARYRVDGQVEYLGRADQQIKIRGFRIEIGEIESQLLAHPYVAEAAVVAQDGVGGPLL
                                         ** **: .**::  *** ***:******:* ****: * *   * ..**:**   *.  *

Q6VT93_A1_16__arg                        VAFLELVQPALPE
Q5DIV7_A2_arg                            AAYL-VGRDA---
                                         .*:* : : *   
