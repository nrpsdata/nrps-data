CLUSTAL W multiple sequence alignment


P45745_A1_gly                            PSCIITTEEIAASLPDDLAVPELVLDQAVTQEIIK---RYSPENQDVSVSLDHPAYIIYT
Q9L8H4_A2_5__gly                         PALLLTDTRTEQHLPADADTRRLALDSAEVRALLADCPDTDPAEEGVTPAPGSAAYVIYT
P45745_A1_2__gly                         PSCIITTEEIAASLPDDLAVPELVLDQAVTQEIIK---RYSPENQDVSVSLDHPAYIIYT
                                         *: ::*  .    ** *  . .*.**.* .: ::      .* ::.*: : . .**:***

P45745_A1_gly                            SGSTGRPKGVVVTQKSLSNFLLSMQEAFSLGEEDRLLAVTTVAFDISALELYLPLISGAQ
Q9L8H4_A2_5__gly                         SGSTGRPKGVVVPHSALVNFVTAMRRQAPLRPQERLLAVTTVAFDIAALELYHPLLSGAA
P45745_A1_2__gly                         SGSTGRPKGVVVTQKSLSNFLLSMQEAFSLGEEDRLLAVTTVAFDISALELYLPLISGAQ
                                         ************.:.:* **: :*:.  .*  ::************:***** **:*** 

P45745_A1_gly                            IVIAKKETIREPQALAQMIENFDINIMQATPTLWHALVTSEPEKLRGLRVLVGGEALPSG
Q9L8H4_A2_5__gly                         VVLAPKEAVPQPSAVLDLIARHGVTTVQGTPSLWQLLVGHDAEALRGLRMLVGGEALPLS
P45745_A1_2__gly                         IVIAKKETIREPQALAQMIENFDINIMQATPTLWHALVTSEPEKLRGLRVLVGGEALPSG
                                         :*:* **:: :*.*: ::* ...:. :*.**:**: **  :.* *****:******** .

P45745_A1_gly                            LLQELQDLHCSVTNLYGPTETTIWSAAAFLEEGLKGVPPIGKPIWNTQVYVLDNGLQPVP
Q9L8H4_A2_5__gly                         LAEALRALTDDLVNLYGPTETTIWSTAAELAGG-TGAAPIGRPIANTRVYVLDDGLQPVA
P45745_A1_2__gly                         LLQELQDLHCSVTNLYGPTETTIWSAAAFLEEGLKGVPPIGKPIWNTQVYVLDNGLQPVP
                                         * : *: *  .:.************:** *  * .*..***:** **:*****:*****.

P45745_A1_gly                            PGVVGELYIAGTGLARGYFHRPDLTAERFVADPYG-PPGTRMYRTGDQARWRADGSLDYI
Q9L8H4_A2_5__gly                         PGVVGELYIAGAGLARGYLDRPALTAERFPADPYGLEPGGRMYRTGDLVRWNPDGELEFV
P45745_A1_2__gly                         PGVVGELYIAGTGLARGYFHRPDLTAERFVADPYG-PPGTRMYRTGDQARWRADGSLDYI
                                         ***********:******:.** ****** *****  ** ******* .**..**.*:::

P45745_A1_gly                            GRADHQIKIRGFRIELGEIDAVLANHPHIEQAAVVVREDQPGDKRLAAYVVADAAIDTA-
Q9L8H4_A2_5__gly                         GRADHQVKVRGFRIEPGEIEKVLTDHPDIAQAAVVVREDQPGDARLVAYVVTGGSADARD
P45745_A1_2__gly                         GRADHQIKIRGFRIELGEIDAVLANHPHIEQAAVVVREDQPGDKRLAAYVVADAAIDTA-
                                         ******:*:****** ***: **::**.* ************* **.****:..: *:  
