CLUSTAL W multiple sequence alignment


Q9R9J0_A2_asn                            AGIILMPRDVR------------QQITYEGVVIL-LDEESSYHEEAFNLEPLSNANHLAY
Q70JZ9_A2_asn                            AGIVLMQRDVR------------KQLAYEGVTVL-LDDEGSYHQDGSDLAPINDASHLAY
Q93I55_A2_asn                            AGIVLMQQDVR------------KQLAYEGVTVL-LDDESSYHQDGSDLAPINDVSHLAY
Q9RAH2_A3_asn                            AKVLLTHSQLKLGAGDWGSGTGNKETFPQSPSILYLDRDNFANQSTENPSRQSQPDDLAY
                                         * ::*   :::            ::   :.  :* ** :.  ::.  :    .: ..***

Q9R9J0_A2_asn                            VIYTSGSTGKPKGVLIEHRGLSNYIWWAKEVYVKNEKTNFPLYSSISFDLTVTSIFTPLV
Q70JZ9_A2_asn                            VIYTSGSTGRPKGVLIEHRGLTNYIWWAKEVYVKGEKANFPLYSSISFDLTVTSIFTPLV
Q93I55_A2_asn                            VIYTSGSTGRPKGVLIEHGGLTNYIWWAKEVYVKGEKANFPLYSSISFDLTVTSIFTPLV
Q9RAH2_A3_asn                            VIYTSGSTGKPKGVQLPHRGLSNYLHWAKDYYAVAQGQGTPVQSSLSFDATITSLYLPLI
                                         *********:**** : * **:**: ***: *.  :  . *: **:*** *:**:: **:

Q9R9J0_A2_asn                            TGNTIIVYDGEDKTALLSSIV-QDQRVDIIKLTPAHLHVLKAMNIANKIA--IRKMIVGG
Q70JZ9_A2_asn                            TGNAIIVYDGEDKTALLESIV-RDPRVDIIKLTPAHLQVLKEMNIADQTA--VRRMIVGG
Q93I55_A2_asn                            TGNAIIVYDGEDKTALLESIV-RDPRVDIIKLTPAHLQVLKEMNIADQMA--VRRMIVGG
Q9RAH2_A3_asn                            CGRTTILVREKQELQLLADIVKQNNHLSLVKITPSHLEILNQQIEPDTMPNRVNAFVLGG
                                          *.: *:   :::  ** .** :: ::.::*:**:**.:*:    .:  .  :. :::**

Q9R9J0_A2_asn                            ENL-STQLAQSIHEQFDGQIEICNEYGPTETVVGCMLYRYDAVKDRRESVPIGTAAANTS
Q70JZ9_A2_asn                            ENL-STRLARSIHEQFEGRIEICNEYGPTETVVGCMIYRYDAAKDRRESVPIGTAAANTS
Q93I55_A2_asn                            ENL-STRLARSIHEQFEGRIEICNEYGPTETVVGCMIYRYDPAKDRRESVPIGTAAANTS
Q9RAH2_A3_asn                            EALHANQIIPWLTHAPDTR--LINEYGPTEAVVGCCVYEATGKRDLAGDLLIGQPIANVR
                                         * * :.::   : .  : :  : *******:**** :*.    :*   .: ** . **. 

Q9R9J0_A2_asn                            IYVLDEDMKPVPIGVPGEMYISGAGVARGYLNRPELTAEKFVE-NPFVTGERMYKTGDLA
Q70JZ9_A2_asn                            IYVLDENMKPAPIGVPGEIYISGAGVARGYLNRPELTAEKFVD-DPFEPGAKMYKTGDLA
Q93I55_A2_asn                            IYVLDENMKPAPIGVPGEIYISGAGVARGYLNRPELTAEKFVD-DLFEPGAKMYKTGDLA
Q9RAH2_A3_asn                            IYILDNQNQLLPVGIPGELCIAGAGLARGYLNRPELTAEKFIELDLFGKKERIYKTGDLA
                                         **:**:: :  *:*:***: *:***:***************:: : *    ::*******

Q9R9J0_A2_asn                            KWLPDGNIEYLGRMDEQVKIRGFRIELGEIETAMLQAEEIKEAVVTAREDVHGLKQLCGY
Q70JZ9_A2_asn                            KWLADGNIEYAGRIDEQVKIRGYRIELGEIEAALLQEEAIKEAVVTAREDVHGFKQLCAY
Q93I55_A2_asn                            KWLADGNIEYAGRIDEQVKIRGYRIELGEIEAALLQEEAIKEAVVTAREDVHGFKQLCAY
Q9RAH2_A3_asn                            RWLPDGNLEYLGRIDNQIKIRGFRIELGEIEALLNQHDDVQASVVTAREDTPGDKRLIAY
                                         :**.***:** **:*:*:****:********: : * : :: :*******. * *:* .*

Q9R9J0_A2_asn                            YVSSQPITVSQ
Q70JZ9_A2_asn                            YVSGGQTTAAR
Q93I55_A2_asn                            YVSGGQTTAAR
Q9RAH2_A3_asn                            VVPHQHCTPTI
                                          *.    * : 
