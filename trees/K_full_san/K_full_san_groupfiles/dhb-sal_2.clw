CLUSTAL W multiple sequence alignment


Q56950a_A1_dhb|sal                       PVAYVIHGENHAELARQMAHKHACLRHVLVAGETVSDDFTPLFSLHGERQAWPQPDVSAT
Q56950b_A1_dhb|sal                       PVAYVIHGENHAELARQMAHKHACLRHVLVAGETVSDDFTPLFSLHGERQAWPQPDVSAT
                                         ************************************************************

Q56950a_A1_dhb|sal                       ALLLLSGGTTGTPKLIPRRHADYSYNFSASAELCGISQQSVYLAVLPVAHNFPLACPGIL
Q56950b_A1_dhb|sal                       ALLLLSGGTTGTPKLIPRRHADYSYNFSASAELCGISQQSVYLAVLPVAHNFPLACPGIL
                                         ************************************************************

Q56950a_A1_dhb|sal                       GTLACGGKVVLTDSASCDEVMPLIAQERVTHVALVPALAQLWVQAREWEDSDLSSLRVIQ
Q56950b_A1_dhb|sal                       GTLACGGKVVLTDSASCDEVMPLIAQERVTHVALVPALAQLWVQAREWEDSDLSSLRVIQ
                                         ************************************************************

Q56950a_A1_dhb|sal                       AGGARLDPTLAEQVIATFDCTLQQVFGMAEGLLCFTRLDDPHATILHSQGRPLSPLDEIR
Q56950b_A1_dhb|sal                       AGGARLDPTLAEQVIATFDCTLQQVFGMAEGLLCFTRLDDPHATILHSQGRPLSPLDEIR
                                         ************************************************************

Q56950a_A1_dhb|sal                       IVDQDENDVAPGETGQLLTRGPYTISGYYRAPAHNAQAFTAQGFYRTGDNVRLDEVGNLH
Q56950b_A1_dhb|sal                       IVDQDENDVAPGETGQLLTRGPYTISGYYRAPAHNAQAFTAQGFYRTGDNVRLDEVGNLH
                                         ************************************************************

Q56950a_A1_dhb|sal                       VEGRIKEQINRAGEKIAAAEVESALLRLAEVQDCAVVAAPDTLLGERICAFIIAQQVPTD
Q56950b_A1_dhb|sal                       VEGRIKEQINRAGEKIAAAEVESALLRLAEVQDCAVVAAPDTLLGERICAFIIAQQVPTD
                                         ************************************************************

Q56950a_A1_dhb|sal                       YQQ
Q56950b_A1_dhb|sal                       YQQ
                                         ***
