CLUSTAL W multiple sequence alignment


Q9RLP6v2_A2_thr                          PVAAVSSADLCTRLIASGVPVIEVDDPAIGAEASTSLPVPAVDDIAYIIYTSGTTGTPKG
Q9RLP6v1_A2_thr                          PVAAVSSADLCTRLIASGVPVIEVDDPAIGAEASTSLPVPAVDDIAYIIYTSGTTGTPKG
                                         ************************************************************

Q9RLP6v2_A2_thr                          VAVTHRNVAQLLDTLGAQLELGQTWTQCHSLAFDYSVWEIWGPLLNGGRLLMVPDAVVRS
Q9RLP6v1_A2_thr                          VAVTHRNVAQLLDTLGAQLELGQTWTQCHSLAFDYSVWEIWGPLLNGGRLLMVPDAVVRS
                                         ************************************************************

Q9RLP6v2_A2_thr                          PEDLHAMLVAEQVSMLSQTPSAFYALQTADALYPERGEQLKLQTVVFGGEALEPHRLSGW
Q9RLP6v1_A2_thr                          PEDLHAMLVAEQVSMLSQTPSAFYALQTADALYPERGEQLKLQTVVFGGEALEPHRLSGW
                                         ************************************************************

Q9RLP6v2_A2_thr                          MHAHPGMPRMINMYGITETTVHASFREIGEADLANSTSPIGVPLEHLSFFVLDGWLRQVP
Q9RLP6v1_A2_thr                          MHAHPGMPRMINMYGITETTVHASFREIGEADLANSTSPIGVPLEHLSFFVLDGWLRQVP
                                         ************************************************************

Q9RLP6v2_A2_thr                          VGVVGELYVAGEGLACGYISRSDLTSTRFVACPFGAPGARMYRTGDLVRWGADGQLQYVG
Q9RLP6v1_A2_thr                          VGVVGELYVAGEGLACGYISRSDLTSTRFVACPFGAPGARMYRTGDLVRWGADGQLQYVG
                                         ************************************************************

Q9RLP6v2_A2_thr                          RADEQVKIRGYRIELGEVHAALVGLDGVEQAAVIAREDRPGDKRLVGYVTGAVDPV
Q9RLP6v1_A2_thr                          RADEQVKIRGYRIELGEVHAALVGLDGVEQAAVIAREDRPGDKRLVGYVTGAVDPV
                                         ********************************************************
