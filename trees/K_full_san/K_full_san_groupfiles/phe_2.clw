CLUSTAL W multiple sequence alignment


O30408_A3_4__phe                         AKLVVTH---AHLLHKVSSQSEVVDVDDPGSYATQTDNLPCANTPSDLAYIIYTSGTTGK
P09095_A1_phe                            TKIVLTQKSVSQLVHDVGYSGEVVVLDEEQLDARETANLHQPSKPTDLAYVIYTSGTTGK
P0C062_A1_1__phe                         ARMLLTQKHLVHLIHNIQFNGQVEIFEEDTIKIREGTNLHVPSKSTDLAYVIYTSGTTGN
                                         :::::*:    :*:*.:  ..:*  .::      :  **  ....:****:********:

O30408_A3_4__phe                         PKGVMLEHKGVANLQAVFAHHLGVTPQDRAGHFASISFDASVWDMFGPLLSGATLYVLSR
P09095_A1_phe                            PKGTMLEHKGIANLQSFFQNSFGVTEQDRIGLFASMSFDASVWEMFMALLSGASLYILSK
P0C062_A1_1__phe                         PKGTMLEHKGISNLKVFFENSLNVTEKDRIGQFASISFDASVWEMFMALLTGASLYIILK
                                         ***.******::**: .* : :.** :** * ***:*******:** .**:**:**:: :

O30408_A3_4__phe                         DVINDFQRFAEYVRDNAITFLTLPPTYAIYLEPEQVPSLRTLITAGSASSVALVDKWKEK
P09095_A1_phe                            QTIHDFAAFEHYLSENELTIITLPPTYLTHLTPERITSLRIMITAGSASSAPLVNKWKDK
P0C062_A1_1__phe                         DTINDFVKFEQYINQKEITVITLPPTYVVHLDPERILSIQTLITAGSATSPSLVNKWKEK
                                         :.*:**  * .*: :: :*.:******  :* **:: *:: :******:* .**:***:*

O30408_A3_4__phe                         VTYVNGYGPTESTVCATLWKA-KPDEPVETITIGKPIQNTKLYIVDDQLQLKAPGQMGEL
P09095_A1_phe                            LRYINAYGPTETSICATIWEAPSNQLSVQSVPIGKPIQNTHIYIVNEDLQLLPTGSEGEL
P0C062_A1_1__phe                         VTYINAYGPTETTICATTWVA-TKETTGHSVPIGAPIQNTQIYIVDENLQLKSVGEAGEL
                                         : *:*.*****:::*** * * . : . .::.** *****::***:::*** . *. ***

O30408_A3_4__phe                         CISGLSLARGYWNRPELTAEKFVDNPFVPGTKMYRTGDLARWLPDGTIEYLGRIDHQVKI
P09095_A1_phe                            CIGGVGLARGYWNRPDLTAEKFVDNPFVPGEKMYRTGDLAKWLTDGTIEFLGRIDHQVKI
P0C062_A1_1__phe                         CIGGEGLARGYWKRPELTSQKFVDNPFVPGEKLYKTGDQARWLPDGNIEYLGRIDNQVKI
                                         **.* .******:**:**::********** *:*:*** *:**.**.**:*****:****

O30408_A3_4__phe                         RGHRVELGEVESVLLRYDTVKEAAAITHEDDRGQAYLCAYYVAEGEATPAQ
P09095_A1_phe                            RGHRIELGEIESVLLAHEHITEAVVIAREDQHAGQYLCAYYISQQEATPAQ
P0C062_A1_1__phe                         RGHRVELEEVESILLKHMYISETAVSVHKDHQEQPYLCAIFVSEKHIPLEQ
                                         ****:** *:**:** :  :.*:.. .::*.:   **** :::: . .  *
