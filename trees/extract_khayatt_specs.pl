#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my $kfaa = 'K_full_san.faa';
my $qfaa = 'khayatt_goset_mibig.faa';
my %seenspec = ();
my %seenseq = ();
my $k = new Bio::SeqIO(-file=>$kfaa, -format=>'fasta');
while(my $seq = $k->next_seq){
	$seenseq{$seq->id} = 1;
	my @hn = split(/_+/, $seq->id);
	$seenspec{$hn[-1]} = 1;
}
my $q = new Bio::SeqIO(-file=>$qfaa, -format=>'fasta');
while(my $seq = $q->next_seq){
	unless(exists $seenseq{$seq->id}){
		my @hn = split(/_+/, $seq->id);
		print '>' . $seq->id . '_TEST' . "\n" . $seq->seq . "\n" if(exists $seenspec{$hn[-1]});
	}
}
