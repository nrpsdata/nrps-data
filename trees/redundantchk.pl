#!/bin/env perl

use strict;
use warnings;

my %seen = ();
while(<>){
	if($_ =~ m/^>/){
		chomp;
		$seen{$_} += 1;
	}
}
foreach my $s (keys %seen){
	print "At least one duplicate for header:\t$s\n" if($seen{$s} > 1);
}
