#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

## usage:
##	perl queryfaa.pl *.faa > file.tsv

my $wildcard = 'UNK';
foreach my $faa (@ARGV){
	my $fa = new Bio::SeqIO(-file=> $faa, -format=> 'fasta');
	while(my $seq = $fa->next_seq){
		open my $tf, '>', "tmpt.fa" or die $!;
		print $tf '>' . $seq->id . '_' . $wildcard . "\n" . $seq->seq . "\n";
		close $tf;
		print join("\t", $seq->id, hmmscanner('tmpt.fa')) . "\n";
		system("rm tmp*");
	}
}

sub hmmscanner{
	my $q = $_[0];
	my $db = './trees/Adomains/Khayatt_nrpsA.hmmdb';
	system("hmmscan -o tmp.hmmscan.out --tblout tmp.hmmtbl.out --noali $db $q");
	open my $sfh, '<', "tmp.hmmtbl.out" or die "Died in hmmscanner: $!";
	while(<$sfh>){
		chomp;
		if($_ =~ m/^(\w+\S*)\s/){
			close $sfh;
			return $1;
		}
	}
	close $sfh;
	return "no_call";
}
