#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my %list = (
	'simA.bp'	=> 'simA.faa',
	'chaP.tbn'	=> 'chaP.fna'
);
foreach my $b (keys %list){
	my %ps = goget($b, $list{$b});
	foreach my $p (sort keys %ps){
		print '>', $p . "\n" . $ps{$p} . "\n";
	}
}

sub goget{
	my %toget = ();
	my ($bout, $fasta) = @_;
	open my $b, '<', $bout or die $!;
	while(<$b>){
		chomp;
		my ($query, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
		$toget{$hit}{$sstart} = $send;
	}
	my $fa = new Bio::SeqIO(-file=>$fasta, -format=>'fasta');
	my %ps = ();
	while(my $seq = $fa->next_seq){
		my $id = $seq->id;
		if(exists $toget{$id}){
			my $mod = 1;
			foreach my $st (sort keys %{$toget{$id}}){
				my $sub = $seq->trunc($st, $toget{$id}{$st});
				$sub = $sub->translate unless($fasta =~ m/\.faa/);
				$ps{join('_', $id, 'mod' . $mod)} = $sub->seq;
				$mod+=1;
			}
		}
	}
	return %ps;
}
