#!/bin/env perl

use strict;
use warnings;

my ($fulllist, $set) = ('list2pull.tsv', 'mibig_set.tsv');
my %inset = ();
open my $sfh, '<', $set or die $!;
while(<$sfh>){
	if($_ =~ m/^(BGC\d+)/){
		$inset{$1} = 1;
	}
}
close $sfh;
open my $lfh, '<', $fulllist or die $!;
while(<$lfh>){
	chomp;
	print "$_\n" unless(exists $inset{$_});
}
close $lfh;
