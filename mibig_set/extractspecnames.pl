#!/bin/env perl

use strict;
use warnings;

my %seen = ();
open my $i, '<', 'mibig_set.tsv' or die $!;
while(<$i>){
	next if($_ =~ m/^Cluster/);
	chomp;
	my ($bgc, $mod, $evi, $spec, $gene) = split(/\t/, $_);
	$seen{$spec} += 1;
}
close $i;
foreach my $s (keys %seen){
	print "$s\n";
}
