#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

unless(-e "nismc2.nrpsA.bp"){
	## Get A_Domains
	my $a_query = 'nnvaivceneqltyhelnvkanqlarifiekgigkdtlvgimmeksidlfigilavlkaggayvpidieypkeriqyilddsqarmlltqkhlvhlihniqfngqveifeedtikiregtnlhvpskstdlayviytsgttgnpkgtmlehkgisnlkvffenslnvtekdrigqfasisfdasvwemfmalltgaslyiilkdtindfvkfeqyinqkeitvitlpptyvvhldperilsiqtlitagsatspslvnkwkekvtyinaygptetticattcvatketighsvpigapiqntqiyivdenlqlksvgeagelciggeglargywkrpeltsqkfvdnpfvpgeklyktgdqarwlsdgnieylgridnqvkirghrveleevesillkhmyisetavsvhkdhqeqpylcayfvsekhipleqlrqfsseelptymipsyfiqldkmpltsngkidrkqlpepdltfgmrvdyeaprn';
	open my $q, '>', 'q.tmp' or die $!;
	print $q '>nrpsA' . "\n$a_query\n";
	close $q;
	my $evalue = '1e-20';
	system("makeblastdb -in notinset.mc_nrps2.faa -out nismc_nrps2.db -dbtype prot 1> /dev/null");
	system("blastp -query q.tmp -db nismc_nrps2.db -outfmt 6 -out nismc2.nrpsA.bp -max_target_seqs 10000000 -num_threads 16 -evalue $evalue");
	system("rm q.tmp");
}

open my $bp, '<', 'nismc2.nrpsA.bp' or die $!;
my %s2e = ();
while(<$bp>){
	next if ($_ =~ m/^#/);
	chomp;
	my ($query, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
	$s2e{$hit}{$sstart}{'end'} = $send;
}
## Get mod numbers
foreach my $h ( keys %s2e){
	my $m = 1;
	foreach my $s (sort { $a <=> $b } keys %{$s2e{$h}} ){
		$s2e{$h}{$s}{'mod'} = $m;
		$m+=1;
	}
}
close $bp;
my %g2s = ();
open my $lfh, '<', 'notinset.mc_subset2.tsv' or die $!;
while(<$lfh>){
	next if ($_ =~ m/^#/);
	chomp;
	my ($bgc, $pub, $gname, $gene, $mod, $spec, $evid) = split(/\s/, $_);
	my $head = join('_', $bgc, $gene);
	$g2s{$head}{$mod} = $spec;
}
open my $aout, '>', 'nismc2_adom.faa' or die $!;
my $nrpsfa = new Bio::SeqIO(-file=>'notinset.mc_nrps2.faa', -format=>'fasta');
while(my $seq = $nrpsfa->next_seq){
	foreach my $s (sort { $a <=> $b } keys %{$s2e{$seq->id}} ){
		my $a_dom = $seq->trunc($s, $s2e{$seq->id}{$s}{'end'});
		if(exists $g2s{$seq->id}{ $s2e{$seq->id}{$s}{'mod'} } ){
			print $aout '>' . join('_', $seq->id, $s, 'mod' . $s2e{$seq->id}{$s}{'mod'}, $g2s{$seq->id}{ $s2e{$seq->id}{$s}{'mod'} } ) . "\n" . $a_dom->seq . "\n";
		}else{
			print STDERR "Issue with mod $s2e{$seq->id}{$s}{'mod'} of " . $seq->id . "\n";
		}
	}
}
close $aout;
