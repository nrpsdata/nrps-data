#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my %ams = ();
open my $mcs, '<', './notinset.mc_subset.tsv' or die $!;
while(<$mcs>){
	unless($_ =~ m/^#/){
		chomp;
		my ($bgc, $pub, $gname, $gacc, $mod, $spec, $evid) = split(/\t/, $_);
		$ams{$gacc} = $bgc;
	}
}
close $mcs;
open my $bo, '>', 'notinset.mc_nrps.faa' or die $!;
foreach my $k (keys %ams){
	system("esearch -db protein -query \"$k\" | efetch -format fasta |grep . > tmp.faa");
	my $t = new Bio::SeqIO(-file=>'tmp.faa', -format=>'fasta');
	my $found = 0;
	while(my $seq = $t->next_seq){
		die "ERROR:\t$found sequences found for $k\n" if($found > 1);
		$found+=1;
		print $bo '>' . join('_', $ams{$k}, $k) . "\n" . $seq->seq . "\n";
	}
}
close $bo;
system("rm tmp.faa");
