#!/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;

my ($start, $end) = (15150, 22349);
my $fa = new Bio::SeqIO(-file=>'tnb.fna', -format=>'fasta');
my $seq=$fa->next_seq; 
$seq->trunc($start, $end);
open my $tnbf, '>', 'tnbF.fna' or die $!;
print $tnbf '>tnbF' . "\n" . $seq->seq . "\n";
close $tnbf; 
my $a_query = 'nnvaivceneqltyhelnvkanqlarifiekgigkdtlvgimmeksidlfigilavlkaggayvpidieypkeriqyilddsqarmlltqkhlvhlihniqfngqveifeedtikiregtnlhvpskstdlayviytsgttgnpkgtmlehkgisnlkvffenslnvtekdrigqfasisfdasvwemfmalltgaslyiilkdtindfvkfeqyinqkeitvitlpptyvvhldperilsiqtlitagsatspslvnkwkekvtyinaygptetticattcvatketighsvpigapiqntqiyivdenlqlksvgeagelciggeglargywkrpeltsqkfvdnpfvpgeklyktgdqarwlsdgnieylgridnqvkirghrveleevesillkhmyisetavsvhkdhqeqpylcayfvsekhipleqlrqfsseelptymipsyfiqldkmpltsngkidrkqlpepdltfgmrvdyeaprn'; 
open my $q, '>', 'q.tmp' or die $!;
print $q '>nrpsA' . "\n$a_query\n";
close $q;
my $evalue = '1e-20';
system("makeblastdb -in tnb.fna -out tnb.db -dbtype nucl 1> /dev/null");
system("tblastn -query q.tmp -db tnb.db -outfmt 6 -out tnbF.nrpsA.tbn -max_target_seqs 10000000 -num_threads 16 -evalue $evalue");
open my $tbn, '<', 'tnbF.nrpsA.tbn' or die $!;
my $mod = 2;
while(<$tbn>){
	chomp;
	my ($query, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
	my $rc = 0;
	if($sstart > $send){
		my $swap = $sstart;
		$sstart = $send;
		$send = $swap;
		$rc = 1;
	}
	my $adom = undef;
	if($rc == 1){
		$adom = $seq->trunc($sstart, $send)->revcom->translate;
	}else{
		$adom = $seq->trunc($sstart, $send)->translate;
	}
	print '>BGC0000451_CP001614.2_15150-22349_mod' . $mod . '_';
	if($mod == 2){
		print 'ser';
	}else{
		print 'orn';
	}
	print "\n" . $adom->seq . "\n";
	$mod -= 1;
}
close $tbn;
