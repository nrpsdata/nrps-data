#!/bin/env perl

use strict;
use warnings;

while(<>){
	chomp;
	if($_ =~ m/^(>\S+)/){
		$_ = $1;
	}
	print "$_\n";
}
