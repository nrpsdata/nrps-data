#!/bin/env python

import json
import glob

bgclist = [line.rstrip('\n') for line in open('notinset.tsv')]
print '\t'.join(['Cluster', 'Publications'])
for bgc in bgclist:
	with open('../mibig_json/all_json_170815/' + bgc + '.json') as data_file:
		data = json.load(data_file)
		pub = ''
		for p in data['general_params']['publications']:
			if(len(pub) < 8):
				pub += p
			if(len(pub) == 8):
				print '\t'.join([bgc, pub])
