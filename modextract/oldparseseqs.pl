#!/bin/env perl

use strict;
use warnings;
use LWP::Simple;
use Bio::SeqIO;

## Parse CSV
my %nrps = ();
while(<>){
	unless($_ =~ m/^#/){
		chomp;
		my ($inc, $tord, $aord, $gi, $uprot, $lid, $nr, $ann1, $cop, $set, $c, $ann2, $uprot2, $e, $domain, $reduced) = split(/,/, $_);
		my $mod = 0;
		next if($set ne 'K');
		if($nr =~ m/A(\d+)/){
			$mod = $1;
		}elsif($nr eq 'ALod' or $nr eq 'A'){
			$mod = 'Load';
		}elsif($nr eq 'ADiscrete'){
			$mod = 'Disc';
		}else{
			die "Issue with module: $nr\n";
		}
		$nrps{$uprot} = {
			$mod	=> {
				'full'	=> $domain,
				'red'	=> $reduced,
				'ann_s'	=> $ann1,
				'ann_v'	=> $ann2,
				'uprn'	=> 'NA'
			}
		};
	}
}

## Dump seqs from CSV
open my $kr, '>', 'K_reduced.faa' or die $!;
open my $kf, '>', 'K_full.faa' or die $!;
foreach my $u (sort keys %nrps){
	foreach my $m (sort { $nrps{$u}{$a} <=> $nrps{$u}{$b} } keys %{$nrps{$u}} ){
		print $kf '>' . join('_', $u, 'module' . $m, $nrps{$u}{$m}{'ann_s'}, '_full') . "\n" . $nrps{$u}{$m}{'full'} . "\n";
		print $kr '>' . join('_', $u, 'module' . $m, $nrps{$u}{$m}{'ann_s'}, '_reduced') . "\n" . $nrps{$u}{$m}{'red'} . "\n";
	}
}
close $kr;
close $kf;

## Dump list of UniProt IDs
open my $u, '>', 'uniprot.txt' or die $!;
print $u join("\n", keys %nrps) . "\n";
close $u;

## Grab Proteins
my $base = 'http://www.uniprot.org/uniprot/';
open my $upfa, '>', 'uniprot.faa' or die $!;
foreach(keys %nrps){
	my $o = $_;
	$_ =~ s/(\S+)\/.+/$1/;
	next if($_ =~ m/removed/i);
	my $fa = '';
	my $url = $base . $_ . '.fasta';
	if(my $page = get($url)){
		$fa = $page;
	}else{
		$url = 'http://www.uniprot.org/uniparc/?query=' . $_;
		my $parc = get($url) or die "$url is unavailable\n";
		if($parc =~ m/\/uniparc\/(\w\S+)\"/){
			$parc = $1;
		}else{
			die "Died on $_\n";
		}
		$url = 'http://www.uniprot.org/uniparc/' . $parc . '.fasta';
		$fa = get($url) or die "$url is unavailable\n";
	};
	my ($head, @seq) = split(/\n/, $fa);
	my ($fs, @junk) = split(/\s+/, $head);
	my $faseq = '';
	foreach my $l (@seq){
		$faseq .= $l;
	}
	$fs =~ s/>//;
	foreach my $m (keys %{$nrps{$_}}){
		$nrps{$o}{$m}{'uprn'} = $fs;
        }
	print $upfa '>' . $fs . "\n$faseq\n";
}
close $upfa;
open my $lfh, '>', 'uprotlist.tsv' or die $!;
my %blook = ();
foreach my $u (keys %nrps){
	foreach my $m (keys %{$nrps{$u}}){
		print $lfh join("\t", $u, $m, $nrps{$u}{$m}{'uprn'}) . "\n";
		$blook{ $nrps{$u}{$m}{'uprn'} } = $u;
	}
}
close $lfh;

## Get A_Domains
my $a_query = 'nnvaivceneqltyhelnvkanqlarifiekgigkdtlvgimmeksidlfigilavlkaggayvpidieypkeriqyilddsqarmlltqkhlvhlihniqfngqveifeedtikiregtnlhvpskstdlayviytsgttgnpkgtmlehkgisnlkvffenslnvtekdrigqfasisfdasvwemfmalltgaslyiilkdtindfvkfeqyinqkeitvitlpptyvvhldperilsiqtlitagsatspslvnkwkekvtyinaygptetticattcvatketighsvpigapiqntqiyivdenlqlksvgeagelciggeglargywkrpeltsqkfvdnpfvpgeklyktgdqarwlsdgnieylgridnqvkirghrveleevesillkhmyisetavsvhkdhqeqpylcayfvsekhipleqlrqfsseelptymipsyfiqldkmpltsngkidrkqlpepdltfgmrvdyeaprn';
open my $q, '>', 'q.tmp' or die $!;
print $q '>nrpsA' . "\n$a_query\n";
close $q;
my $evalue = '1e-20';
system("makeblastdb -in uniprot.faa -out uniprot.db &> /dev/null");
system("blastp -query q.tmp -db uniprot.db -outfmt 6 -out adom.bp -max_target_seqs 10000000 -num_threads 16 -evalue $evalue");
system("rm q.tmp");

## Parse blast
open my $bp, '<', 'adom.bp' or die $!;
my %mod = ();
my $i = 0;
while(<$bp>){
	chomp;
	my ($query, $hit, $pctid, $alen, $mismatch, $gapopen, $qstart, $qend, $sstart, $send, $evalue, $bitscore) = split(/\t/, $_);
	$i += 1;
	$mod{$hit}{$i}{'start'} = $sstart;
	$mod{$hit}{$i}{'end'} = $send;
}
close $bp;

## Extract modules
open my $ad, '>', 'adom.faa' or die $!;
my $faa = new Bio::SeqIO(-file=>'uniprot.faa', -format=>'fasta');
while (my $seq = $faa->next_seq){
	if(exists $mod{$seq->id} ){
		my $mod = 0;
		foreach my $m ( sort { $mod{$seq->id}{$a}{'start'} <=> $mod{$seq->id}{$b}{'start'} } keys %{$mod{$seq->id}} ){
			$mod += 1;
			my $tr = $seq->trunc( $mod{$seq->id}{$m}{'start'}, $mod{$seq->id}{$m}{'end'} );
			#print "Mod=$mod\tSeqID=" . $seq->id . "\tAnnLookup=" . $nrps{ $blook{$seq->id} }{$mod}{'ann_s'} . "\n" if(exists $blook{$seq->id} && exists $nrps{ $blook{$seq->id} }{$mod}{'ann_s'});
			print $ad '>' . join('_', $seq->id, "module$mod", $mod{$seq->id}{$m}{'start'}, $mod{$seq->id}{$m}{'end'}, $nrps{ $blook{$seq->id} }{$mod}{'ann_s'}) . "\n" . $tr->seq . "\n" if(exists $blook{$seq->id} && exists $nrps{ $blook{$seq->id} }{$mod}{'ann_s'});;
		}
	}
}
close $ad;
