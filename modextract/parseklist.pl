#!/bin/env perl

use strict;
use warnings;

## Parse CSV
open my $kr, '>', 'K_reduced.faa' or die $!;
open my $kf, '>', 'K_full.faa' or die $!;
while(<>){
	unless($_ =~ m/^#/){
		chomp;
		my ($dom, $red, $head) = split(/,/, $_);
		print $kr "$head\n$red\n";
		print $kf "$head\n$dom\n";
	}
}
close $kr;
close $kf;
